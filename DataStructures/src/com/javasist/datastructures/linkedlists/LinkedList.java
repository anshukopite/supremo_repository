package com.javasist.datastructures.linkedlists;

public class LinkedList<T extends Comparable<T>> implements List<T> {

	private Node<T> root;
	private int sizeOfList;
	
	@Override
	public void insert(T data) {
		
		++this.sizeOfList;
		
		if(root==null) {
			this.root = new Node(data);
		}else {
			insertDataAtBegginning(data);
		}
	}

	// root --> Null ; 
	// newNode --> root;
	private void insertDataAtBegginning(T data) {
		Node<T> newNode = new Node<>(data);
		newNode.setNextNode(root);
		this.root = newNode;
	}

	@Override
	public void remove(T data) {
		// TODO Auto-generated method stub
		
	}

	
	
	
}

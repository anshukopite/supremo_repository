package com.javasist.datastructures.linkedlists;

public interface List<T extends Comparable<T>> {

	void insert(T data);
	void remove(T data);
}

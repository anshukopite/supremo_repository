package com.javasist.thepracticalimpl.execute;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class IterateMap {

	public void usingEntrySet(Map<String,Integer> map) {

		Iterator<Entry<String,Integer>> itr = map.entrySet().iterator();
		Map.Entry<String, Integer> entry = null;
		while(itr.hasNext()) {
			entry = itr.next();
			System.out.println("[Key: "+entry.getKey()+" Value: "+entry.getValue()+"]");
		}
	}

	public void usingKeySet(Map<String,Integer> map) {

		Set<String> itr= map.keySet();
		for(String key : itr) {

			System.out.println("[Key: "+key+" Value: "+map.get(key)+"]");

		}
	} 
	
	
	public void usingStream(Map<String,Integer> map) {
		
		map.entrySet().stream().forEach(
				(entry)->System.out.println("[Key: "+entry.getKey()+" Value: "+entry.getValue()+"]"));
	}

	public void usingLambda(Map<String,Integer> map) {

		map.forEach((k,v)-> System.out.println("[Key: "+k+" Value: "+v+"]")); 
		
	}
}

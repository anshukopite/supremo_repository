package com.javasist.thepracticalimpl.execute;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class ExecuteIterateMap {

	public static void main(String[] args) {

		IterateMap iterateMap = new IterateMap();
		String keyName = "#KEY";
		Map<String,Integer> map = new HashMap<String, Integer>();
		for(int i = 0;i<1000;i++) {
			map.put(keyName+i, (i*i));
		}
		System.out.println("----Iteration Starting Now----");
		long startTime = new Date().getTime();
		//iterateMap.usingKeySet(map); //Using keySet --> 60 ms
		//iterateMap.usingEntrySet(map); //Using entrySet --> 53 ms
		//iterateMap.usingLambda(map); //Using LambdaExp --> 68 ms
		iterateMap.usingStream(map); //using Stream --> 76 ms
		long endTime = new Date().getTime();
		System.out.println("----Iteration Ended----"+ (startTime-endTime)+" ms");
		
	}

}

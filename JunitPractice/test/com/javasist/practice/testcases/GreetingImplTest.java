package com.javasist.practice.testcases;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;

import com.javasist.junit.practice.Greeting;
import com.javasist.junit.practice.GreetingImpl;

public class GreetingImplTest {

	private static int count = 0;
	
	@Before
	public void beforeMethod() {
		
		System.out.println("Before Method Called "+ ++count);
	}
	
	
	@Test
	public void testingGreeting() {
		
		Greeting greet = new GreetingImpl();
		String result = greet.greeting("Junit");
		System.out.println("Hello Testing");
		assertNotNull(result);
		assertEquals("Hello Junit", result);
	}

	@Test
	public void myTestCase() {
		
		Greeting greet = new GreetingImpl();
		String result = greet.greeting("Junit");
		System.out.println("Hello myTestCase");
		assertNotNull(result);
		assertEquals("Hello Junit", result);
		
	}
	
	
	
}

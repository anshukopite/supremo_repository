package com.javasist.junit.practice;

public class GreetingImpl implements Greeting {

	@Override
	public String greeting(String name) {
		return "Hello "+name;
	}

}

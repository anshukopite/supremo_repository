package com.javasist.coreImpl.lambdaexp;

public class LambdaExp {

	public static void main (String [] args) {
		
		FunctionalInter f1 = () -> System.out.println("Functional...");
		f1.printStuff();
		f1.printStuff2();
		FunctionalInter.printStuff3();
	}
}

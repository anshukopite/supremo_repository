package com.javasist.coreImpl.lambdaexp;

import java.util.List;

import com.javasist.coreImpl.impls.Employee;

@FunctionalInterface
public interface FunctionalInter {

	void printStuff();
	default void printStuff2() {
		
		System.out.println("Interface");
	}

	static void  printStuff3() {
		
		System.out.println("Static method!!");
	}
}

package com.javasist.coreImpl.methodref;

import java.util.function.Supplier;

import com.javasist.coreImpl.impls.Employee;

public class MethodReferences {

	
	public static void main(String[] args) {
		
		Employee emp = new Employee("Anshuman", 292, "gaonsindhe");
		Supplier<Employee> lambdaExp = () -> new Employee("Anshuman",21 , "Gaonisndhe");
		System.out.println("Using Lambda Exp --> "+lambdaExp.get());
		
		Supplier<Employee> reference = Employee::new;
		
		System.out.println("Using Method Ref --> "+reference.get());
		
		
	}
	
	
}

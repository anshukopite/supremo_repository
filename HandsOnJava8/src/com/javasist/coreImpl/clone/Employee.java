package com.javasist.coreImpl.clone;

import java.util.HashMap;
import java.util.Map;

public class Employee implements Cloneable{
	
	private int id;
	private String name;
	private Map<String,Integer> props = new HashMap<String, Integer>();
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Map<String, Integer> getProps() {
		return props;
	}
	public void setProps(Map<String, Integer> props) {
		this.props = props;
	}
	
	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + ", props=" + props + "]";
	}
	@Override
	public Object clone() throws CloneNotSupportedException{
		return super.clone();
	}
	
}

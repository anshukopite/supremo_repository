package com.javasist.coreImpl.clone;

import java.util.List;

public class ListClone implements Cloneable{

	private int age;
	private String name;
	private List<Integer> incentives;
	
	public int getAge() {
		return age;
	}
	public ListClone(int age, String name, List<Integer> incentives) {
		super();
		this.age = age;
		this.name = name;
		this.incentives = incentives;
	}
	
	public List<Integer> getIncentives() {
		return incentives;
	}
	public void setIncentives(List<Integer> incentives) {
		this.incentives = incentives;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException{
		return super.clone();
	}
	
	
}

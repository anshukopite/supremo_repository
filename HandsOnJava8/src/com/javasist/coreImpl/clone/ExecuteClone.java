package com.javasist.coreImpl.clone;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ExecuteClone {

	public static void main(String[] args) throws CloneNotSupportedException {

		/*
		 * Employee emp = new Employee(); emp.setId(1); emp.setName("Anshuman");
		 * Map<String, Integer> props = new HashMap<String, Integer>();
		 * props.put("salary", 10000); props.put("incentive",2000); emp.setProps(props);
		 * Employee clonedEmp = (Employee) emp.clone();
		 * System.out.println("comparing Memory Address "+(emp==clonedEmp));
		 * System.out.println("Comparing Memory Address of Non-primitive variables "+(
		 * emp.getProps()==clonedEmp.getProps())); emp.setName("Kanika");
		 * emp.getProps().put("salary", 20000);
		 * System.out.println("Get Props "+clonedEmp.getProps());
		 * System.out.println("Emp ->  "+emp+"\nCloned Obj -> "+clonedEmp);
		 */	
		
		ListClone list = new ListClone(15,"Anshuman",Arrays.asList(10000,2000,6000));
		
		ListClone clonedList = (ListClone) list.clone();
		
		
		System.out.println("Comparing Objects "+(list==clonedList));
		
		System.out.println("Commparing List Objects "+(list.getIncentives()==clonedList.getIncentives()));
		
		list.setIncentives(Arrays.asList(10,20,15));
		
		System.out.println("Fecthing Cloned Incentives"+ clonedList.getIncentives());
	}
}
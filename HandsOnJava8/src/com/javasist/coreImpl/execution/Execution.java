package com.javasist.coreImpl.execution;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import com.javasist.coreImpl.impls.Employee;
import com.javasist.coreImpl.lambdaexp.FunctionalInter;

public class Execution {

	public static void main(String[] args) {

		Employee emp = new Employee("Anshuman", 28, "Gaonsindhe");
		Employee emp2 = new Employee("Kanika", 29, "Tyagi");
		Employee emp3 = new Employee("Amyrah", 9, "Tyagi");
		Employee emp4 = new Employee("Jerry", 19, "Tyagi");
		Employee emp5 = new Employee("Jerry", 15, "Gaonsindhe");
		
		List<Employee> empList = new ArrayList<Employee>();
		empList.add(emp);	
		empList.add(emp2);
		empList.add(emp3);
		empList.add(emp4);
		empList.add(emp5);
		
		empList.forEach(p -> System.out.println(p.getFirstName()));
		
		empList.sort((Employee empw, Employee emp2w) -> empw.getFirstName().compareTo(emp2w.getFirstName())) ;
		System.out.println("Sorted List ");
		empList.forEach(p -> System.out.println(p.getFirstName()));
		
		
	}

}

import java.util.Arrays;
import java.util.List;

public class ExecuteEight {

	public static void main(String[] args) {

		List<People> people = Arrays.asList(
				new People("Kanika", "Tyagi",32),
				new People("Anshuman","Gaonsindhe",30),
				new People("Amyrah","Tyagi",10),
				new People("Amyrah","Gaonsindhe",10));
		
		long filtered = people.stream()
		.filter(p -> p.getLastName().startsWith("G"))
		.count();
		
		people.stream()
		.filter(p-> p.getAge() >15)
		.forEach(p -> System.out.println(p.getFirstName()));
		
		System.out.println("Count is "+filtered);
	}

}

package com.javasist.multithreading.oldthreadapi;

import java.util.concurrent.TimeUnit;

public class ReturningValuesFromTaskA implements Runnable {

	private int a;
	private int b;
	private int sum;
	private long sleepTime;
	
	private static int count=0;
	private int instanceId ;
	private String taskId;
	private boolean done = false;
	
	
	public ReturningValuesFromTaskA(int a,int b,long sleepTime) {
	
		this.a = a;
		this.b= b;
		this.sleepTime = sleepTime;
		
		this.instanceId = ++count;
		this.taskId = "Task-" + instanceId;
	}
	
	
	
	@Override
	public void run() {

		String currentThreadName = Thread.currentThread().getName();
		
		System.out.println("#### Starting ["+currentThreadName+"] for "+taskId+" ####");
		System.out.println("**** Sleeping ["+currentThreadName+"] for "+taskId+" for "+sleepTime+" milliseconds ****");
		
		try {
			TimeUnit.MILLISECONDS.sleep(sleepTime);
		}catch(InterruptedException e){
			e.printStackTrace();
		}

		sum= a+b;
		System.out.println("#### Ending ["+currentThreadName+"] for "+taskId+" ####");
		done= true;
		synchronized (this) {
			System.out.println("["+currentThreadName+"] "+taskId+" Notifying...");
			this.notifyAll();
		}
	}

	public int getSum() {
		if(!done) {
			synchronized (this) {
				try {
					System.out.println("["+Thread.currentThread().getName()+"] waiting for result from "+taskId);
					this.wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			System.out.println("["+Thread.currentThread().getName()+"] Woken Up for "+taskId);
		}
		return sum;
	}
}

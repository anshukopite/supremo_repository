package com.javasist.multithreading.oldthreadapi;

public class ExecuteThread {

	public static void main(String[] args) {

		String currentthreadName = Thread.currentThread().getName();
		System.out.println("["+currentthreadName+"] thread starts here!!");
		
		ReturningValuesFromTaskA task1 = new ReturningValuesFromTaskA(5, 3, 2000);
		Thread t1 = new Thread(task1,"My Thread-1");

		ReturningValuesFromTaskA task2 = new ReturningValuesFromTaskA(5, 9, 5000);
		Thread t2 = new Thread(task2, "My Thread-2");

		ReturningValuesFromTaskA task3 = new ReturningValuesFromTaskA(5, 3, 1000);
		Thread t3 = new Thread(task3, "My Thread-3");
		
		t1.start();
		t2.start();
		t3.start();
		
		//Retrieving Results using Getter where Main thread is blocked if worker thread has not produced results.
		System.out.println("Result-1 = "+task1.getSum());
		System.out.println("Result-2 = "+task2.getSum());
		System.out.println("Result-3 = "+task3.getSum());
		
		
		System.out.println("["+currentthreadName+"] thread ends here!!");
		
		
	}

}

package com.javasist.multithreading.oldthreadapi;

public class ExecuteDeamonThread {

	public static void main(String[] args) {
		String currentthreadName = Thread.currentThread().getName();
		System.out.println("["+currentthreadName+"] thread starts here!!");
		
		Thread task1 = new Thread(new LoopTaskA(2000), "Thread - 1");
		Thread task2 = new Thread(new LoopTaskA(500), "Thread - 2");

		task1.setDaemon(true);
		
		task1.start();
		task2.start();
		
		System.out.println("["+currentthreadName+"] thread ends here!!");
		
	}

}

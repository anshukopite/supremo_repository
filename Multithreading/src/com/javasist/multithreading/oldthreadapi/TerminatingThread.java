package com.javasist.multithreading.oldthreadapi;

import java.util.concurrent.TimeUnit;


public class TerminatingThread {

	public static void main(String[] args) throws InterruptedException {

		String currentthreadName = Thread.currentThread().getName();
		System.out.println("["+currentthreadName+"] thread starts here!!");
		
		LoopTaskB task1 = new LoopTaskB();
		LoopTaskB task2 = new LoopTaskB();
		LoopTaskB task3 = new LoopTaskB();
		
		new Thread(task1, "Thread-1").start();
		new Thread(task2, "Thread-2").start();
		new Thread(task3, "Thread-3").start();
		
		TimeUnit.MILLISECONDS.sleep(5000);
	
		task1.cancel();
		task2.cancel();
		task3.cancel();
		
		System.out.println("["+currentthreadName+"] thread ends here!!");

	
	}

}

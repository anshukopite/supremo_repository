package com.javasist.multithreading.oldthreadapi;

public class ResultObserver implements ResultListener<Integer> {

	
	private String taskId;
	
	public ResultObserver(String taskId) {
		this.taskId = taskId;
	}
	
	
	@Override
	public void notifyResult(Integer result) {

		System.out.println("Result for "+taskId + "- "+result);
		
	}

}

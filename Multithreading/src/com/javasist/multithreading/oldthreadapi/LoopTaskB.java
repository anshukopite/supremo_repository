package com.javasist.multithreading.oldthreadapi;

import java.util.concurrent.TimeUnit;

public class LoopTaskB implements Runnable {

	private static int count=0;
	private int taskId ;
	private String name = "LoopTaskB";
	private boolean shutdown =false;
	
	public LoopTaskB() {
		this.taskId = ++count;
		this.name = name +taskId;
	}
	
	
	@Override
	public void run() {
		String currentThreadName = Thread.currentThread().getName();
		System.out.println("#### Starting ["+currentThreadName+"] for "+name+" ####");
		
		for(int i=1;;i++) {
			System.out.println("#### ["+currentThreadName+"] for "+name+" TICK TOK "+i);
			try {
				TimeUnit.MILLISECONDS.sleep((long)(Math.random()*3000));
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
			synchronized (this) {
				if(shutdown)
					break;
			}
			
		}
		System.out.println("#### Ending ["+currentThreadName+"] for "+name+" ####");
	}

	public void cancel() {
		
		System.out.println("**** Shutting down ["+Thread.currentThread().getName()+"] for "+name+" ****");
		synchronized (this) {
			shutdown= true;
		}
	}

}






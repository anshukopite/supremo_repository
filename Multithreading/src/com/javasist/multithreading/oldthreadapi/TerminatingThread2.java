package com.javasist.multithreading.oldthreadapi;

import java.util.concurrent.TimeUnit;


public class TerminatingThread2 {

	public static void main(String[] args) throws InterruptedException {

		String currentthreadName = Thread.currentThread().getName();
		System.out.println("["+currentthreadName+"] thread starts here!!");
		
		LoopTaskC task1 = new LoopTaskC();
		LoopTaskC task2 = new LoopTaskC();
		LoopTaskC task3 = new LoopTaskC();
		
		Thread t1 = new Thread(task1, "Thread-1");
		t1.start();
		
		Thread t2 = new Thread(task2, "Thread-2");
		t2.start();
		
		Thread t3 = new Thread(task3, "Thread-3");
		t3.start();
		
		TimeUnit.MILLISECONDS.sleep(5000);
	
		System.out.println(currentthreadName+" Interrupting..."+t1.getName()+" ...");
		t1.interrupt();
		
		System.out.println(currentthreadName+" Interrupting..."+t2.getName()+" ...");
		t2.interrupt();
		
		System.out.println(currentthreadName+" Interrupting..."+t3.getName()+" ...");
		t3.interrupt();
		
		System.out.println("["+currentthreadName+"] thread ends here!!");

	
	}

}

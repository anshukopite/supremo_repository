package com.javasist.multithreading.oldthreadapi;

public class ExecuteThreadReturningValue {

	public static void main(String[] args) {
		String currentthreadName = Thread.currentThread().getName();
		System.out.println("["+currentthreadName+"] thread starts here!!");
		
		ReturningValuesFromTaskB task1 = new ReturningValuesFromTaskB(5, 3, 2000, new ResultObserver("task - 1"));
		Thread t1 = new Thread(task1,"My Thread-1");

		ReturningValuesFromTaskB task2 = new ReturningValuesFromTaskB(5, 9, 5000, new ResultObserver("task - 2"));
		Thread t2 = new Thread(task2, "My Thread-2");

		ReturningValuesFromTaskB task3 = new ReturningValuesFromTaskB(5, 3, 1000, new ResultObserver("task - 3"));
		Thread t3 = new Thread(task3, "My Thread-3");
		
		t1.start();
		t2.start();
		t3.start();
		
		System.out.println("["+currentthreadName+"] thread ends here!!");
		
	}

}

package com.javasist.multithreading.oldthreadapi;

public interface ResultListener<T> {

	void notifyResult(T result);
	
}

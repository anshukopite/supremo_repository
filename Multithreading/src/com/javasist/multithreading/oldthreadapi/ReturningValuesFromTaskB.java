package com.javasist.multithreading.oldthreadapi;

import java.util.concurrent.TimeUnit;

public class ReturningValuesFromTaskB implements Runnable {

	private int a;
	private int b;
	private int sum;
	private long sleepTime;
	
	private static int count=0;
	private int instanceId ;
	private String taskId;
	private ResultListener<Integer> listener;
	
	
	public ReturningValuesFromTaskB(int a,int b,long sleepTime, ResultListener<Integer> listener) {
	
		this.a = a;
		this.b= b;
		this.sleepTime = sleepTime;
		this.listener = listener;
		this.instanceId = ++count;
		this.taskId = "Task-" + instanceId;
	}
	
	
	
	@Override
	public void run() {

		String currentThreadName = Thread.currentThread().getName();
		
		System.out.println("#### Starting ["+currentThreadName+"] for "+taskId+" ####");
		System.out.println("**** Sleeping ["+currentThreadName+"] for "+taskId+" for "+sleepTime+" milliseconds ****");
		
		try {
			TimeUnit.MILLISECONDS.sleep(sleepTime);
		}catch(InterruptedException e){
			e.printStackTrace();
		}

		sum= a+b;
		System.out.println("#### Ending ["+currentThreadName+"] for "+taskId+" ####");
		listener.notifyResult(sum);
	}

}

package com.javasist.multithreading.oldthreadapi;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class LoopTaskC implements Runnable {

	private static final int DATA_SIZE = 100;
	private static int count=0;
	private int taskId ;
	private String name = "LoopTaskC";
	
	public LoopTaskC() {
		this.taskId = ++count;
		this.name = name +taskId;
	}
	
	
	@Override
	public void run() {
		String currentThreadName = Thread.currentThread().getName();
		System.out.println("#### Starting ["+currentThreadName+"] for "+name+" ####");
		
		for(int i=1;;i++) {
			System.out.println("#### ["+currentThreadName+"] for "+name+" TICK TOK "+i);

			doSomeWork();
			
			
			if(Thread.interrupted()) {
				System.out.println("#### Interrupting thread ["+currentThreadName+"] for "+name);
				 break;
			}
		}
		System.out.println("#### Retrieving interruppted status ["+currentThreadName+"] for "+name+" :"+Thread.interrupted());

		System.out.println("#### Ending ["+currentThreadName+"] for "+name+" DONE ####");
	}

	
	public void doSomeWork() {
		
		for(int i=1; i <3 ;i++) {
			Collections.sort(generateDataset());
		}
	}

	private List<Integer> generateDataset() {

		List<Integer> list = new ArrayList<Integer>();
		Random randomGenerator = new Random();
		for(int i=1;i<DATA_SIZE;i++) {
			list.add(randomGenerator.nextInt(DATA_SIZE));
		}
		
		return list;
	}
	

}






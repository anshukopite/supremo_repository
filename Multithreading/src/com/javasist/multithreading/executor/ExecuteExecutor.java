package com.javasist.multithreading.executor;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecuteExecutor {

	public static void main(String[] args) {

		System.out.println("^^ Main Thread Starts Here ^^");
		//Initialize ExecutorService 
		ExecutorService executorservice = Executors.newFixedThreadPool(3); //Three threads initialized to work concurrently
		//ExecutorService executorservice = Executors.newCachedThreadPool(); //Three threads initialized to work concurrently
		
		//Submitting task to the service
		executorservice.execute(new FixedThreadPool());
		executorservice.execute(new FixedThreadPool());	
		executorservice.execute(new FixedThreadPool());
		
		
		//Destruction Phase
		executorservice.shutdown();
		
		
		System.out.println("^^ Main Thread Starts Here ^^");
	}

}

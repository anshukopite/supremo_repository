package com.javasist.multithreading.executor;

import java.util.concurrent.TimeUnit;

public class LoopTaskA implements Runnable {

	private static int count=0;
	private int taskId ;
	private String name = "LoopTaskA";
	
	private long sleepTime;
	
	public LoopTaskA(long sleepTime) {
		this.taskId = ++count;
		this.name = name +taskId;
		this.sleepTime = sleepTime;
	}
	
	
	@Override
	public void run() {
		boolean isDeamonThreadrunning = Thread.currentThread().isDaemon();
		String threadType = isDeamonThreadrunning ? "DEAMON" : "USER";
		
		String currentThreadName = Thread.currentThread().getName();
		System.out.println("#### Starting ["+currentThreadName+", "+threadType+"] for "+name+" ####");
		System.out.println("**** Sleeping ["+currentThreadName+", "+threadType+"] for "+name+" for "+sleepTime+" milliseconds ****");
		
		for(int i=10;i>0;i--) {
			System.out.println("#### ["+currentThreadName+", "+threadType+"] for "+name+" TICK TOK "+i);
			try {
				TimeUnit.MILLISECONDS.sleep(sleepTime);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("#### Ending ["+currentThreadName+", "+threadType+"] for "+name+" ####");
		
	}

}

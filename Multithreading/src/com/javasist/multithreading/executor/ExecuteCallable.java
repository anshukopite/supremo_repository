package com.javasist.multithreading.executor;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ExecuteCallable {

	public static void main(String[] args) {
		String currentthreadName = Thread.currentThread().getName();
		System.out.println("["+currentthreadName+"] thread starts here!!");
		
		ExecutorService executorService = Executors.newCachedThreadPool(new NamedThread());
		
		Future<Integer> result1 = executorService.submit(new CalculateTaskB(5, 3, 3000));
		Future<Integer> result2 = executorService.submit(new CalculateTaskB(4, 3, 300));
		Future<Integer> result3 = executorService.submit(new CalculateTaskB(15, 3, 1000));
		
		executorService.shutdown();
		
		
		try {
			System.out.println("Result 1 - "+result1.get());
			System.out.println("Result 2 - "+result2.get());
			System.out.println("Result 3 - "+result3.get());
			
		} catch (InterruptedException | ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		System.out.println("["+currentthreadName+"] thread ends here!!");
	}

}

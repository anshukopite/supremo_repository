package com.javasist.multithreading.executor;

import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import com.javasist.multithreading.oldthreadapi.ResultListener;

public class CalculateTaskB implements Callable<Integer> {

	private int a;
	private int b;
	private int sum;
	private long sleepTime;
	
	private static int count=0;
	private int instanceId ;
	private String taskId;
	
	
	public CalculateTaskB(int a,int b,long sleepTime) {
	
		this.a = a;
		this.b= b;
		this.sleepTime = sleepTime;
		this.instanceId = ++count;
		this.taskId = "Task-" + instanceId;
	}

	@Override
	public Integer call() throws Exception {
		
		String currentThreadName = Thread.currentThread().getName();
		System.out.println("#### Starting ["+currentThreadName+"] for "+taskId+" ####");
		System.out.println("**** Sleeping ["+currentThreadName+"] for "+taskId+" for "+sleepTime+" milliseconds ****");
		TimeUnit.MILLISECONDS.sleep(sleepTime);
		System.out.println("**** Ending ["+currentThreadName+"] for "+taskId+" ****");

		return a+b;
	}

}

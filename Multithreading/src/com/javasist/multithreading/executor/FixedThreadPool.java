package com.javasist.multithreading.executor;

import java.util.concurrent.TimeUnit;

public class FixedThreadPool implements Runnable {

	private static int count=0;
	private int id;
	
	@Override
	public void run() {

		System.out.println("##### <TASK ID "+id+"> STARTING ######");
			for(int i=10;i>0;i--) {
				System.out.println("<TASK ID "+id+ "> TICK TOK "+i);
			
				try {
					TimeUnit.MILLISECONDS.sleep((long) (Math.random() *1000));
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		System.out.println("***** <TASK ID "+id+"> DONE *****");
	}
	
	public FixedThreadPool() {
		this.id = ++count;
	}
}

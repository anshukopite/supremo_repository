package com.javasist.multithreading.executor;

import java.util.concurrent.ThreadFactory;

public class NamedThread implements ThreadFactory {
	private static int count=0;
	private String NAME = "Task-";
	@Override
	public Thread newThread(Runnable r) {
		Thread t = new Thread(r, NAME + ++count);
		return t;
	}
}

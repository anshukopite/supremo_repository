package com.javasist.multithreading.executor;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecuteDeamonExecutor {

	public static void main(String[] args) {

		String currentThreadName = Thread.currentThread().getName();
		System.out.println("["+currentThreadName+"] thread starts here!!");
		//Initialize ExecutorService 
		ExecutorService executorservice = Executors.newCachedThreadPool(new DeamonThreadsNamedFactory()); //Three threads initialized to work concurrently
		
		//Submitting task to the service
		executorservice.execute(new LoopTaskA(100));
		executorservice.execute(new LoopTaskA(200));	
		executorservice.execute(new LoopTaskA(100));
		executorservice.execute(new LoopTaskA(200));
		executorservice.execute(new LoopTaskA(200));
		
		
		//Destruction Phase
		executorservice.shutdown();
		
		System.out.println("["+currentThreadName+"] thread ends here!!");
	}

}

package com.javasist.multithreading.rehashing;

public class ExecuteRehashingProblem {

	public static void main(String[] args) {

		RehashingProblem r1 = new RehashingProblem();
		RehashingProblem r2 = new RehashingProblem();
		RehashingProblem r3 = new RehashingProblem();
		
		
		Thread t1 = new Thread(r1,"Thread-1");
		Thread t2 = new Thread(r2,"Thread-2");
		Thread t3 = new Thread(r3,"Thread-3");
		
		t1.start();
		t2.start();
		t3.start();
		
	}

}

package com.javasist.multithreading.rehashing;

import java.util.HashMap;
import java.util.Map;

public class RehashingProblem implements Runnable{

	private static int count = 0;
	private int taskId;
	private String taskName = "Task-";
	private static Map<String,Integer> map = new HashMap<String, Integer>();;
	
	private String key = "Key-";

	public RehashingProblem() {
		this.taskId = ++count;
		this.taskName = taskName + taskId;
		
	}
	
	
	@Override
	public void run() {
		String currentThreadName = Thread.currentThread().getName();
		for(int i=1;i<30;i++) {
			map.put(key+i,i);
			System.out.println(currentThreadName+" Adding Value to Map on Task ["+taskName+"]  Map("+(key+i)+","+i+")");
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}

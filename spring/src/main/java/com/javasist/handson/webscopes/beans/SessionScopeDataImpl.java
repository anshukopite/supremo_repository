package com.javasist.handson.webscopes.beans;

import java.util.Date;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

@Component
@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class SessionScopeDataImpl implements SessionScopeData {

	private  Date date = new Date();
	
	public SessionScopeDataImpl() {
		System.out.println("In SessionScopeDataImpl!!");
	}
	
	public String getDate() {
		return date.toString();
	}

}

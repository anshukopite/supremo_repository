package com.javasist.handson.webscopes.beans;

public interface SessionScopeData {

	public String getDate();
}

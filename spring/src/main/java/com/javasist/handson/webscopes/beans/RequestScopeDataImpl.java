package com.javasist.handson.webscopes.beans;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

@Component
@Scope(value = WebApplicationContext.SCOPE_REQUEST,proxyMode = ScopedProxyMode.TARGET_CLASS)
public class RequestScopeDataImpl implements RequestScopeData {
	
	private Date date  = new Date();

	
	public RequestScopeDataImpl() {
		
		System.out.println("In RequestSCopeDataImpl!!");
	}

	public String getDate() {
		System.out.println("Date ->"+date.toString());
		return date.toString();
	}

}

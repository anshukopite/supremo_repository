package com.javasist.handson.webscopes.beans;

public interface RequestScopeData {

	public String getDate();
}

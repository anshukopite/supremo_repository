package com.javasist.handson.webscopes.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.javasist.handson.webscopes.beans.RequestScopeDataImpl;
import com.javasist.handson.webscopes.beans.SessionScopeDataImpl;

@Controller
public class ScopeController {

	@Autowired
	private RequestScopeDataImpl req;
	
	@Autowired
	private SessionScopeDataImpl ses;
	
	
	@RequestMapping("/scopes")
	public String showData(Model model) {
		
		System.out.println(req.getDate());
		model.addAttribute("requestScope", req.getDate());
		
		model.addAttribute("sessionScope", ses.getDate());
		
		
		return "scope";
	}
	
	
}

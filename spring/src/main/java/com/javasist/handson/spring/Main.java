package com.javasist.handson.spring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.javasist.handson.spring.bean.Employee;

public class Main {

	
	public static void main(String [] args) {
		
		ApplicationContext context = new ClassPathXmlApplicationContext("application-context.xml");
		
		//ApplicationContext context2 = new ClassPathXmlApplicationContext("bean.xml");
		Employee employee = context.getBean(Employee.class);
		Employee emp = context.getBean(Employee.class);
		
		//Employee employee2 = context2.getBean(Employee.class);
		System.out.println("Employee One "+employee.hashCode());
		System.out.println("Employee Two "+emp.hashCode());
		System.out.println("Address One "+employee.getAddress().hashCode());
		System.out.println("Address Two "+emp.getAddress().hashCode());
		//System.out.println("Bean Two "+employee2.hashCode());
		
	}
	
	
}

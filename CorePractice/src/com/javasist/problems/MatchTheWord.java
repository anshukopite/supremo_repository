package com.javasist.problems;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public class MatchTheWord {

	
	public int matchWord() {
		
		int count = 0;
		boolean wordComplete = true;
		String word = "BALLOON";
		String input = "ABCDLOLONFZ";
		Map<Character,Integer> wordOccurence = new TreeMap<Character, Integer>();
		Map<Character,Integer> inputOccurence = new TreeMap<Character, Integer>();
		
		for(int i=0;i<word.length();i++) {
			char c = word.charAt(i);
			Integer val = wordOccurence.get(c);
			if(val!=null)
				wordOccurence.put(c, val+1);
			else
				wordOccurence.put(c, 1);
		}
		
		for(int i=0;i<input.length();i++) {
			char c = input.charAt(i);
			Integer val = inputOccurence.get(c);
			if(val!=null)
				inputOccurence.put(c, val+1);
			else
				inputOccurence.put(c, 1);
		}
		
		System.out.println("Input Occurence");
		
		inputOccurence.forEach(
				(letter,occurence)
				-> System.out.println("Key "+letter+" , Frequency "+occurence));
		
		System.out.println("Word Occrence");
		wordOccurence.forEach(
				(letter,occurence)
				-> System.out.println("Key "+letter+" , Frequency "+occurence));
		
		return count;
		
	}
}

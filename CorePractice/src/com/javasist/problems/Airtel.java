package com.javasist.problems;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class Airtel {

	private static int MAX_CHARS=256;



	public static void main(String[] args) {
		int[] A = {1,2,3,4,5};
		int res = closestToZero(5, A);
		
		System.out.println(res);
		//substring(s);
	}
	
	static String shortestPalindrome(String str){
		
		int x=0;  
		int y=str.length()-1;
		     
		  while(y>=0){
		     if(str.charAt(x)==str.charAt(y)){
		          x++;
		         }
		            y--;
		  }
		 
		if(x==str.length())
			return str;
		 
		String suffix = str.substring(x);
		String prefix = new StringBuilder(suffix).reverse().toString();
		String mid = shortestPalindrome(str.substring(0, x));
		 
		return prefix+mid+suffix;
		
		
	}
	
	public static String subString(String str) {
		
		return "";
	}
	
	
	
	public static void substring(String s) {
		
		char[] c = s.toCharArray();
		Set<Character> list = new LinkedHashSet<>(s.length());
		
		for(Character ch : c) {
			if(Character.isLowerCase(ch))
				list.add(ch);
		}
		System.out.println(list.size());
	
	}
	
	
	public static int closestToZero(int N,int[] A) {
		
		int minVal=A[0];
			
		for(int i=0;i<N;i++) {
			
			if(A[i]==0) {
				return A[i];
			}
			if(A[i]<minVal) {
				minVal = A[i];
			}
		}
		
		return minVal;
	}

}

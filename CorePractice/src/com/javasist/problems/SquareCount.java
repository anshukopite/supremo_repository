package com.javasist.problems;

public class SquareCount {

	
	public int squareCount(int[] a) {
		
		int count = 0;
		//{1,2,2,7,4,2,4,3,9};
		int square = 0;
		for(int i=0;i<a.length;i++) {
			square = a[i] * a[i];
			for(int j=0;j<a.length;j++) {
				if(i!=j && square==a[j])
					count++;
			}			
		}
		System.out.println("Count "+count);
		return count;
	}
	
	@SuppressWarnings("null")
	public int squareCount(int[] a,int count) {
		
		int[] squareArr = new int[a.length] ;
		int square = 0;
		for(int i=0;i<a.length;i++) {
			squareArr[i] = a[i]*a[i]; 
			System.out.print(" "+squareArr[i]);
		}
		
		for(int i=0,j=0;j<a.length;i++) {
			if(i!=j && squareArr[j]==a[i]) {
				count++;
			}
			if(i==(a.length-1)) {
				i=0;
				j=j+1;
			}
		}
		System.out.println("\nCount "+count);
		return count;
	}
	
	public int squareCount(int[] a,boolean c) {
		int count = 0;
		// INPUT ---> {1,2,2,7,4,2,4,3,9}
		for(int i=0;i<a.length;i++) {
			
			System.out.print(" "+a[i]);
		}
		for(int i=0,j=0;j<a.length;) {
			if(i!=j && ((a[j]*a[j])==a[i])) {
				count++;
			}
			if(i==(a.length-1)) {
				i=0;
				j=j+1;
			}
			else
				i++;
		}
		System.out.println("\nCount "+count);
		return count;
	}
	
	
}

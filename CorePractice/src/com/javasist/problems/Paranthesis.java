package com.javasist.problems;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class Paranthesis {

	
	public static void main (String [] args) {
		
		Paranthesis p = new Paranthesis();
		System.out.println("Please provide input to generate series of Parenthesis! ");
		Scanner sc = new Scanner(System.in);
		List<String> result = p.generateParenthesis(sc.nextInt());
		System.out.println(result);
		
	}
	
	public int getInput() {
		
		System.out.println("Please provide input to generate series of Parenthesis! ");
		Scanner sc = new Scanner(System.in);
		try{
			return sc.nextInt();
		}catch(Exception e) {
			System.out.println("Bad Input! Please enter positive Integer");
			getInput();
		}
		return sc.nextInt();
	}
	
	public List<String> generateParenthesis(int n) {
		Set<String> set = new HashSet<String>();
		if(n==0) {
			System.out.println("Bad Input! Please enter positive Integer");
		}

			if(n==1)
				return new ArrayList<String>(Arrays.asList("()"));
			for(String str : generateParenthesis(n-1)) {
				
				for(int i=0;i<str.length();i++) {
					set.add(str.substring(0,i+1)+"()"+str.substring(i+1,str.length()));
				}
			}
			
			List<String> list = new ArrayList<String>(set);
			return list;
	}
}

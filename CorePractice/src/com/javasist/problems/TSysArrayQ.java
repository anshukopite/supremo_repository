package com.javasist.problems;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class TSysArrayQ {

	public int sumArray(List<Integer> list, int k) {
		
		
	ArrayList<Integer> num = new ArrayList<Integer>(list);
		int sum=0,temp;
		Integer roundUp;
		int result = 0;
		Collections.sort(num);
		for(int i=(num.size()-1),j=0;(i>=0 && j<k);) {
			temp = num.get(i);
			result = (int) Math.ceil(temp/2d);
			System.out.println(num.get(i)+"->"+result);
			if((temp%2)==0) {
				num.set(i,result);
				j++;
				i--;
			}else {
				num.set(i, (int) Math.ceil(temp/2d));
				j++;
				i--;
			}
			if(i<0) {
				i=num.size()-1;
			}
		}
		
		//sum = num.stream().collect(Collectors.summingInt(Integer::intValue));
		sum = num.stream()
				.mapToInt(Integer::valueOf)
				.sum();
		
		return sum;
	}
}

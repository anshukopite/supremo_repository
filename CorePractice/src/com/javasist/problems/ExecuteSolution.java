package com.javasist.problems;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ExecuteSolution {

	public static void main(String[] args) {

		//SquareCount sqcnt = new SquareCount();
		//int[] input = {1};
		/*
		 * List<Integer> num = Arrays.asList(11,21); int sum =0;
		 * 
		 * 
		 * sum = new TSysArrayQ().sumArray(num,2);
		 * System.out.println("Minimum Sum is "+sum);
		 */
		
		//sqcnt.squareCount(input,true);

		int[] a = new int[7];
		int temp=0,count=0;
		int j = 0;
		List<Integer> countList= new ArrayList<Integer>();
		a[0]=5;
		a[1]=4;
		a[2]=0;
		a[3]=3;
		a[4]=1;
		a[5]=6;
		a[6]=2;
		int loop=0;
		boolean start = true;
		Set<Integer> tempSet = new HashSet<Integer>();
		for(int i=0,k=0;i<7;) {
			if(start) {
				j = a[i];
				if(tempSet.contains(a[i]))
					start=false;
				tempSet.add(a[i]);
				i = a[j];
				if(tempSet.contains(a[j]))
					start=false;
				tempSet.add(a[j]);
			}
			if(!start) {
				countList.add(tempSet.size());
				tempSet = new HashSet<Integer>();
				k++;
				i=k;
				start=true;
			}
		}
		System.out.println(countList.stream()
				.mapToInt(v -> v).max());
	}
	

}

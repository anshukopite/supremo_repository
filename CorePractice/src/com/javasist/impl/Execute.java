package com.javasist.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class Execute {

	public static void main(String[] args) {

		Employee emp = new Employee("Anshuman", "Gaonsindhe", 29, "Conduent");
		Employee emp2 = new Employee("Kanika", "Tyagi", 27, "HCL");
		Employee emp3 = new Employee("Jerry", "Gaonsindhe", 29, "Infosys");
		Employee emp4 = new Employee("Jerry", "Tyagi", 31, "Iris");
		
		List<Employee> list = new ArrayList<Employee>();
		list.add(emp);
		list.add(emp2);
		list.add(emp3);
		list.add(emp4);
	
		System.out.println("Comparing by Age!!");
		Collections.sort(list);
		
		list.forEach((e)-> System.out.println(e.getFirstName()));
		
		Collections.sort(list, new CustomComparator());
		
		list.forEach(e -> System.out.println(e.getFirstName()));
		
	}

}

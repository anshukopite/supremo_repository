package com.javasist.impl.serialization;

import java.io.Serializable;

public class SerializingObject implements Serializable{

	private static int a;
	private int b;
	private transient String str;

	public SerializingObject(int a, int b, String str) {
		this.a = a;
		this.b = b;
		this.str = str;
	}
	
	public void setA(int a) {
		this.a = a;
	}

	public void setB(int b) {
		this.b = b;
	}

	public void setStr(String str) {
		this.str = str;
	}
	public static void printData(SerializingObject so) {
		
		System.out.println("Values --> a: "+SerializingObject.a+" b:"+so.getB()+" str:"+so.getStr());
		
	}

	public int getA() {
		return a;
	}

	public int getB() {
		return b;
	}

	public String getStr() {
		return str;
	}
	
	
	
}

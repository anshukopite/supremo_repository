package com.javasist.impl.serialization;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;

public class ExecuteSerialization {

	public static void main(String[] args) {

		SerializingObject so = new SerializingObject(5, 8, "Addition");
		
		System.out.println("Serializing Object!!");
		
		try {
		
			ObjectOutput out = new ObjectOutputStream(new FileOutputStream("savingState.txt"));
			out.writeObject(so);
			out.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Printing Data before Deseiralization!!");
		so.setA(15);
		so.setStr("str");
		
		SerializingObject.printData(so);
		
		System.out.println("\nDe-Serialising Object!!");
		
		try {
			ObjectInput in = new ObjectInputStream(new FileInputStream("savingState.txt"));
			SerializingObject so1 = (SerializingObject) in.readObject();
			SerializingObject.printData(so1);
			in.close();
		} catch (IOException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}

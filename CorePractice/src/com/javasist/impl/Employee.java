package com.javasist.impl;

public class Employee implements Comparable<Employee>{

	private String firstName;
	private String lastName;
	private int age;
	private String company;
	
	public Employee(String firstName, String lastName, int age, String company) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
		this.company = company;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	@Override
	public int compareTo(Employee o) {
		return this.firstName.compareTo(o.firstName);
	}

}

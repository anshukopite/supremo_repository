package com.javasist.bookstore;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import com.javasist.bookstore.domain.User;
import com.javasist.bookstore.domain.security.Role;
import com.javasist.bookstore.domain.security.UserRole;
import com.javasist.bookstore.service.UserService;
import com.javasist.bookstore.utility.SecurityUtility;

@SpringBootApplication
@ComponentScan("com.javasist.bookstore")
public class BookstoreApplication implements CommandLineRunner{

	@Autowired
	private UserService userService;
	
	public static void main(String[] args) {
		SpringApplication.run(BookstoreApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		User user1 = new User();
		user1.setFirstName("Kanika");
		user1.setLastName("Tyagi");
		user1.setUsername("k");
		user1.setPassword(SecurityUtility.passwordEncoder().encode("k"));
		user1.setEmail("kanikatyagi@gmail.com");
		Set<UserRole> userRoles = new HashSet<UserRole>();
		Role role1= new Role();
		role1.setRoleId(1);
		role1.setName("ROLE_USER");
		userRoles.add(new UserRole(user1, role1));
		
		userService.createUser(user1, userRoles);		
	}

}

package com.javasist.bookstore.repository;

import org.springframework.data.repository.CrudRepository;

import com.javasist.bookstore.domain.User;

public interface UserRepository extends CrudRepository<User, Long> {
	User findByUsername(String username);

	User findByEmail(String email);
}

package com.javasist.bookstore.repository;

import java.util.Date;
import java.util.stream.Stream;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.javasist.bookstore.domain.User;
import com.javasist.bookstore.domain.security.PasswordResetToken;

public interface PasswordResetTokenRepository extends JpaRepository<PasswordResetToken, Long> {

	PasswordResetToken findByToken(final String token);
	
	PasswordResetToken findByUser(User user);
	
	Stream<PasswordResetToken> findAllByExpiryLessThan(Date now);
	
	@Modifying
	@Query("delete from PasswordResetToken t where t.expiry <=?1")
	void deleteAllExpiredSince(Date now);

}

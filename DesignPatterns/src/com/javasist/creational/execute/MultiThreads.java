package com.javasist.creational.execute;

import com.javasist.creational.singleton.EnumSingleton;
import com.javasist.creational.singleton.InnerClassSingleton;
import com.javasist.creational.singleton.LazySingleton;
import com.javasist.creational.singleton.Multiton;
import com.javasist.creational.singleton.StaticBlockSingleton;

public class MultiThreads implements Runnable{

	@Override
	public void run() {
		
		Multiton object = Multiton.getInstance("One");
		Multiton objectTwo = Multiton.getInstance("Two"); 
		 
		//for(int i=1;i<100;i++) {
			System.out.println(Thread.currentThread().getName()+" Object One:"+object.hashCode()+" Object Two:"+objectTwo.hashCode());
			
		
	}
}

package com.javasist.creational.execute;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import com.javasist.creational.singleton.EagerSingleton;

public class ExecuteSingleton {

	/*
	 * public static void main(String [] args) {
	 * 
	 * 
	 * //EagerSingleton EagerSingleton inst = EagerSingleton.getInstance();
	 * EagerSingleton inst2 = EagerSingleton.getInstance();
	 * System.out.println("Object One "+inst.hashCode());
	 * System.out.println("Object Two "+inst2.hashCode());
	 * 
	 * 
	 * 
	 * 
	 * //LazySingleton LazySingleton obj = LazySingleton.getInstance();
	 * LazySingleton obj2 = LazySingleton.getInstance();
	 * System.out.println("Object One "+obj.hashCode());
	 * System.out.println("Object Two "+obj2.hashCode());
	 * 
	 * 
	 * 
	 * 
	 * MultiThreads t1 = new MultiThreads(); MultiThreads t2 = new MultiThreads();
	 * MultiThreads t3 = new MultiThreads();
	 * 
	 * Thread tt1 = new Thread(t1); Thread tt2 = new Thread(t2); Thread tt3 = new
	 * Thread(t3); tt1.start(); tt2.start(); tt3.start();
	 * 
	 * 
	 * 
	 * Multiton object = Multiton.getInstance("Object One"); Multiton objectTwo =
	 * Multiton.getInstance("Object Two"); Multiton objectThree =
	 * Multiton.getInstance("Object One");
	 * 
	 * System.out.println("Object One "+object.hashCode());
	 * System.out.println("Object Two "+objectTwo.hashCode());
	 * System.out.println("Object Two "+objectThree.hashCode());
	 * 
	 * }
	 */
	
	public static void main(String[] args) throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, FileNotFoundException, ClassNotFoundException, IOException {
		EagerSingleton instance = EagerSingleton.getInstance();
		EagerSingleton instanceTwo = null;
		System.out.println("Instance One -> "+instance.hashCode());
		breakSingletonWithReflection(instanceTwo);
		
		EagerSingleton instanceThree = EagerSingleton.getInstance();
		breakSingletonWithSerialization(instanceThree);
		
		EagerSingleton instanceOne = EagerSingleton.getInstance();
		
	}


	private static void breakSingletonWithSerialization(Object instanceThree) throws FileNotFoundException, IOException, ClassNotFoundException {
		System.out.println("!! Breaking Singleton with Serialization !!");

		ObjectOutput out = new ObjectOutputStream(new FileOutputStream("EagerFile.txt"));
		out.writeObject(instanceThree);
		out.close();
		
		ObjectInput in = new ObjectInputStream(new FileInputStream("EagerFile.txt"));
		instanceThree = (EagerSingleton) in.readObject();
		in.close();
		
		System.out.println("Instance Via Serialization -> "+instanceThree.hashCode());
	}

	private static void breakSingletonWithReflection(Object instance) throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		System.out.println("!! Breaking Singleton with Reflection !!");
		Constructor<EagerSingleton> constructor = EagerSingleton.class.getDeclaredConstructor();
		constructor.setAccessible(true);
		instance = (EagerSingleton) constructor.newInstance();
		System.out.println("Instance Via Reflection -> "+instance.hashCode());
	}
}
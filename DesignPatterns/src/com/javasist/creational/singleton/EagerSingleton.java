package com.javasist.creational.singleton;

import java.io.Serializable;

import com.javasist.creational.execute.CloneSuper;

public class EagerSingleton extends CloneSuper implements Serializable{

	private static EagerSingleton INSTANCE = new EagerSingleton();
	
	
	private EagerSingleton() {
		//System.out.println("Initializing EagerSingleton!!");
	}
	
	public static EagerSingleton getInstance() {
		return INSTANCE;
	}
	
	protected Object readResolve() {
		return INSTANCE;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		 throw new CloneNotSupportedException();
	}
	
}

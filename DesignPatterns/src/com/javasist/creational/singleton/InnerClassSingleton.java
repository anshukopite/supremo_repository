package com.javasist.creational.singleton;

public class InnerClassSingleton {

	private InnerClassSingleton() {
		
		System.out.println("Initializing InnerClassSingleton!!");
	}
	
	static class InnerSingleton{
		private static InnerClassSingleton INSTANCE = new InnerClassSingleton();
	}
	
	
	public static InnerClassSingleton getInstance() {
		
		return InnerSingleton.INSTANCE;
	}
	
}

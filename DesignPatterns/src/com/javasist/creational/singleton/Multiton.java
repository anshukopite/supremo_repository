package com.javasist.creational.singleton;

import java.util.HashMap;
import java.util.Map;

public class Multiton {

	private static final Map<Object, Multiton> instance =  new HashMap<Object, Multiton>(); 
	
	private Multiton() {
		System.out.println("Multiton Object Initialised!!");
	}
	
	
	public static Multiton getInstance(Object key) {
		Multiton object = instance.get(key);
		if(object==null) {
			synchronized (Multiton.class) {
				if(object==null) {
					object = new Multiton();
					instance.put(key, object);
				}
			}
		}
		return object;
	}
}

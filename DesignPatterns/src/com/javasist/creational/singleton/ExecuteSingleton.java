package com.javasist.creational.singleton;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import com.javasist.creational.singleton.EagerSingleton;

public class ExecuteSingleton {
	
	public static void main(String[] args) throws NoSuchMethodException, SecurityException, InstantiationException, 
	IllegalAccessException, IllegalArgumentException, InvocationTargetException, FileNotFoundException, ClassNotFoundException, 
	IOException, CloneNotSupportedException {
		
		EagerSingleton instance = EagerSingleton.getInstance();
		EagerSingleton instanceTwo = null;
		System.out.println("Instance One -> "+instance.hashCode());
		breakSingletonWithReflection(instanceTwo);
		
		EagerSingleton instanceThree = EagerSingleton.getInstance();
		breakSingletonWithSerialization(instanceThree);
		
		EagerSingleton instanceOne = EagerSingleton.getInstance();
		breakingSingletonWithCloning(instanceOne,instanceTwo);
	}

	private static void breakingSingletonWithCloning(EagerSingleton instanceOne,EagerSingleton instanceTwo) throws CloneNotSupportedException {
		System.out.println("\n!! Breaking Singleton with Cloning !!");
		
		instanceTwo = (EagerSingleton) instanceOne.clone();
		
		System.out.println("Instance Via Serialization -> "+instanceTwo.hashCode());
	}

	private static void breakSingletonWithSerialization(Object instanceThree) throws FileNotFoundException, IOException, ClassNotFoundException {
		System.out.println("\n!! Breaking Singleton with Serialization !!");

		ObjectOutput out = new ObjectOutputStream(new FileOutputStream("EagerFile.txt"));
		out.writeObject(instanceThree);
		out.close();
		
		ObjectInput in = new ObjectInputStream(new FileInputStream("EagerFile.txt"));
		instanceThree = (EagerSingleton) in.readObject();
		in.close();
		
		System.out.println("Instance Via Serialization -> "+instanceThree.hashCode());
		System.out.println("Cannot Break Since readResolve() is overridden.");
	}

	private static void breakSingletonWithReflection(Object instance) throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		System.out.println("\n!! Breaking Singleton with Reflection !!");
		
		Constructor<EagerSingleton> constructor = EagerSingleton.class.getDeclaredConstructor();
		constructor.setAccessible(true);
		instance = (EagerSingleton) constructor.newInstance();
		
		System.out.println("Instance Via Reflection -> "+instance.hashCode());
	}
}
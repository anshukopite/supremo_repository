package com.javasist.creational.singleton;

public class LazySingleton {

	private static LazySingleton INSTANCE ;
	private LazySingleton() {
		System.out.println("Initializing LazySingleton!!");
	}
	
	public static LazySingleton getInstance() {
	
		if(INSTANCE==null) {
			
			synchronized (LazySingleton.class) {
				if(INSTANCE==null)
					INSTANCE = new LazySingleton();
			}	
		}
		return INSTANCE;
	}
}

package com.javasist.creational.singleton;

public class StaticBlockSingleton {

	private static StaticBlockSingleton INSTANCE;
	
	private StaticBlockSingleton() {
		
		System.out.println("Initializing StaticBlockSingelton!!");
	}
	
	public static StaticBlockSingleton getInstance() {
		
		if(INSTANCE==null) {
			synchronized (StaticBlockSingleton.class) {
				if(INSTANCE==null)
					INSTANCE = new StaticBlockSingleton();
			}
		}
		return INSTANCE;
	}
}

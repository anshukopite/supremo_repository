package com.javasist.creational.singleton;

public enum EnumSingleton {

	INSTANCE;
	EnumSingleton() {
		System.out.println("Initializing EnumSingleton!!");
	}
	
	
}

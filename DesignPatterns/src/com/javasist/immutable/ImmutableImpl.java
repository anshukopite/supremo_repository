package com.javasist.immutable;

import java.util.Date;

public final class ImmutableImpl {

	private final String name;
	private final String lastName;
	private final Age age;
	
	public ImmutableImpl(String name,String lastName,Age age) {
		this.name = name;
		this.lastName = lastName;
		Age tempAge  =new Age(age.getDay(), age.getMonth(), age.getYear());
		this.age = tempAge;
		//this.date = getDate();
	}

	public String getName() {
		return name;
	}
	/*
	 * public Date getDate() { return new Date(); }
	 */	
	public String getLastName() {
		return lastName;
	}

	public Age getAge() {
		Age tempAge = new Age();
		tempAge.setDay(this.age.getDay());
		tempAge.setMonth(this.age.getMonth());
		tempAge.setYear(this.age.getYear());
		return tempAge;
	}
	
	@Override
	public String toString() {
		return "ImmutableImpl [name=" + name + ", lastName=" + lastName + ", age=" + age + "]";
	}
}

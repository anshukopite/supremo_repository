package com.javasist.immutable;

import java.util.Date;

public class ImmutableExecute {

	public static void main(String[] args) {

		Age age = new Age(15,02,1990);
		
		ImmutableImpl imm = new ImmutableImpl("Anshuman","Gaonsindhe", age);
		System.out.println("<Object -> "+imm.getName()+" "+imm.getLastName()+" | "+imm.getAge().getDay());

		age.setDay(20);
		imm.getAge().setDay(30);
		
		System.out.println("<Object -> "+imm.getName()+" "+imm.getLastName()+" | "+imm.getAge().getDay());
		 
	}
}

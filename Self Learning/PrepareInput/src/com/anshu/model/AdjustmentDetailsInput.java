package com.anshu.model;

public class AdjustmentDetailsInput {

	private String discomCode;
	private String officeCode;
	public String getDiscomCode() {
		return discomCode;
	}
	public void setDiscomCode(String discomCode) {
		this.discomCode = discomCode;
	}
	public String getOfficeCode() {
		return officeCode;
	}
	public void setOfficeCode(String officeCode) {
		this.officeCode = officeCode;
	}
	
	
}

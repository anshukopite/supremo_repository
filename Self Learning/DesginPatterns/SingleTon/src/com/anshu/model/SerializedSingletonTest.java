package com.anshu.model;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;

import com.anshu.pattern.LazyInstialization;
import com.anshu.pattern.SerializableSingleton;

public class SerializedSingletonTest {

	public static void main (String [] args) throws FileNotFoundException, IOException, ClassNotFoundException{
		SerializableSingleton instanceOne = SerializableSingleton.getInstance();
		ObjectOutput out = new ObjectOutputStream(new FileOutputStream("fileName.ser"));
		out.writeObject(instanceOne);
		out.close();
		
		//Deserializationfrom file to object
		
		ObjectInput in = new ObjectInputStream(new FileInputStream("fileName.ser"));
		SerializableSingleton instanceTwo = (SerializableSingleton) in.readObject();
		
		in.close();
		
		System.out.println("Instace One "+instanceOne.hashCode());
		System.out.println("Instace Two "+instanceTwo.hashCode());
	}
	
	
	
}

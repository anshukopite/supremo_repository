package com.anshu.main;

import com.anshu.pattern.EagerIntialization;
import com.anshu.pattern.EnumImplementation;
import com.anshu.pattern.LazyInstialization;

public class Main {

	static LazyInstialization instanceOne,instanceTwo,instanceThree;
	
	
	public static void main(String[] args){
		
		System.out.println("Calling Eager Instance");
		instanceOne = LazyInstialization.getInstance();
		instanceTwo = LazyInstialization.getInstance();
 		
		System.out.println("Instance One "+instanceOne.hashCode());
		System.out.println("Instance Two "+instanceTwo.hashCode());
		System.out.println("Instance Three "+instanceThree.hashCode());
		
	}
}

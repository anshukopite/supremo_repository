package com.anshu.pattern;

import java.lang.reflect.Constructor;

public class ReflectionDestroysSingleton {

	public static void main (String[] args){
	
		//EagerIntialization
		EagerIntialization instanceOne = EagerIntialization.getInstance();
		EagerIntialization instanceTwo = null;
		
		//StackBlockIntialization
		StackBlockIntialization stackInstanceOne = StackBlockIntialization.getInstance();
		StackBlockIntialization stackInstanceTwo = null;
		
		try{
			Constructor[] constructors = StackBlockIntialization.class.getDeclaredConstructors();
			for(Constructor constructor : constructors){
				//Below code destroy Singleton Pattern
				constructor.setAccessible(true);
				stackInstanceTwo = (StackBlockIntialization)constructor.newInstance();
				break;
			}
			System.out.println("Instance One "+stackInstanceOne.hashCode());
			System.out.println("Instance Two "+stackInstanceTwo.hashCode());
		}catch(Exception e){
			
			e.printStackTrace();
		}
	}
}

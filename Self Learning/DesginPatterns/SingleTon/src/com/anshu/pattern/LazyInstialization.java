package com.anshu.pattern;

public class LazyInstialization {
	
	public static LazyInstialization instance ;
	
	//Private Constructor
	private LazyInstialization(){
	}
	
	public static LazyInstialization getInstance(){
		
		if(instance==null){
			instance = new LazyInstialization();
		}
		return instance;
	}
}

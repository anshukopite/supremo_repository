package com.anshu.pattern;
public class EagerIntialization {
	
	public static EagerIntialization instance = new EagerIntialization();
	
	//Private Constructor
	private EagerIntialization(){
	}
	
	public static EagerIntialization getInstance(){
		System.out.println("Instance Created");
		return instance;
	}
	
	public void printLines(){
		
		System.out.println("Called by Instance");
		
	}
	
}

package com.anshu.pattern;

public class ThreadSafeIntialization {

	public static ThreadSafeIntialization instance ;
	
	//Private Cosntructor
	private ThreadSafeIntialization(){
	}

	public static ThreadSafeIntialization getInstance(){
		
		if(instance==null){
			synchronized (ThreadSafeIntialization.class){
				if(instance==null){
					instance = new ThreadSafeIntialization();
				}
			}
		}
	   return instance;
	}
	
}

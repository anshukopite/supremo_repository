package com.anshu.pattern;

public class StackBlockIntialization {

	public static StackBlockIntialization instance ; 
	
	//Private Constructor
	private StackBlockIntialization(){
		
	}
	
	static {
		try{
			
			instance = new StackBlockIntialization();
		}catch(Exception e){
			e.printStackTrace();
		}	
	
	}
		public static StackBlockIntialization getInstance(){
					return instance;
		}
}

package com.anshu.pattern;

public class BillPughImplementation {

	//Private Constructor
	private BillPughImplementation(){
	}

	private static class BillPughHelper{
		private static final BillPughImplementation instance = new BillPughImplementation();
	}
	
	public static BillPughImplementation getInstance(){
		
		return  BillPughHelper.instance;
		
	}
	
	
}

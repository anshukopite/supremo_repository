package com.anshu.pattern;

import java.io.Serializable;

public class SerializableSingleton implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private SerializableSingleton(){
	}
	
	public static SerializableSingleton instance ;
	
	//To avoid descripancy in instances while deserializing object
	protected Object readResolve(){
		return getInstance();
	}
	
	private static class SerializableHelper{
		private static final SerializableSingleton instance = new SerializableSingleton();
	}
	
	public static SerializableSingleton getInstance(){
		return SerializableHelper.instance;
	}
}

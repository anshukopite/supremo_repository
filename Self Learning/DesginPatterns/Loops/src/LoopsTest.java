import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class LoopsTest {

	
	public static void main (String [] args){
		
		List<String> flavours = new ArrayList<>();
		
		flavours.add("Choclate");
		flavours.add("Mango");
		flavours.add("Strawberry");
		flavours.add("Black Current");
		
		Iterator<String> itr = flavours.iterator();
		while(itr.hasNext()){
			System.out.println(""+itr.next());
			
		}
		
		System.out.println("Outside For Loop "+flavours.size());
		for(String value : flavours){
			
			System.out.println(value);
			flavours.add(3, "Strawberry");
		}

		
	}
	
	
	
	
	
	
	
}

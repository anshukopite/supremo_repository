/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Administrator
 */
public class DBConnection {

    public static Connection getConnection() throws ClassNotFoundException, SQLException{
        Connection con=null;
        Class.forName("com.mysql.jdbc.Driver");
        System.out.println("After class.forname");
        con = DriverManager.getConnection("jdbc:mysql://localhost:3306/backupbank","root","root");
        System.out.println("after con...");
        return con;
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author employee
 */
public class AdminAns extends HttpServlet {
    Connection con = null;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AdminAns</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AdminAns at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
            */
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        //processRequest(request, response);
        try{
            String ans = request.getParameter("ans");
            int userid = Integer.parseInt(request.getParameter("userid"));
            System.out.println("Servlet "+userid);
            Date date = new Date();
            Timestamp sqlDate  = new Timestamp(date.getTime());
            int msgid = Integer.parseInt(request.getParameter("msgid"));
            System.out.println("Servlet "+msgid);
            con = connection.DBConnection.getConnection();
            PreparedStatement st = con.prepareStatement("insert into user_inbox_tbl(user_inbox_user_id,user_inbox_answer,user_inbox_rcv_date,user_inbox_admin_inbox_id) values (?,?,?,?)");
            st.setInt(1, userid);
            st.setString(2, ans);
            st.setTimestamp(3, sqlDate);
            st.setInt(4, msgid);
            int insert = st.executeUpdate();
            if(insert>0){

                   response.sendRedirect("admininbox.jsp?msg=Reply Sent successfully");

            }else{
                    response.sendRedirect("adminreply.jsp?msgid="+msgid+"&userid="+userid+"&msg=Reply not sent");
            }

        }catch(Exception c){
            response.sendRedirect("adminreply.jsp?msg=something went wrong in database");
            c.printStackTrace();
         }

    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

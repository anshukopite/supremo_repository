/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package servlets;

import connection.DBConnection;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.PasswordEncoder;

/**
 *
 * @author employee
 */
public class DoLogin extends HttpServlet {
    Connection con = null;
    

         
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException, NoSuchAlgorithmException, NoSuchPaddingException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession(true);
        try {
            con = DBConnection.getConnection();
            String uname = request.getParameter("loginUserName");
            String pass = request.getParameter("loginPassword");
            
            
            //key = KeyGenerator.getInstance(algorithm).generateKey();
            //cipher = Cipher.getInstance(algorithm);
            PasswordEncoder pw = PasswordEncoder.getInstance();
            String passw = pw.encode(pass, uname);
            System.out.println("Passw "+passw);
            //System.out.println("Entered: " + input);
           // byte[] encryptionBytes = encrypt(input);
            //String passw = new String(encryptionBytes);
            Statement st = con.createStatement();
            Statement stmt = con.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM USER_CRED_TBL u,USER_TBL us WHERE USER_CRED_UNAME='"+uname+"' AND USER_CRED_PASSWORD='"+passw+"' and us.user_id = u.user_cred_user_id");
            //ResultSet rss = stmt.executeQuery("SELECT user_cred_status FROM USER_TBL");
            
            if(rs.next()){
                System.out.println(rs.getString(2));
                if(rs.getString("USER_CRED_STATUS").equals("deactive")){
                    response.sendRedirect("index.jsp?damsg=Your account is temporarily deactivated. Please contact Back up Bank Admin for further details..");
                }
                else{
                if(uname.equals(rs.getString("USER_CRED_UNAME")) && passw.equals(rs.getString("USER_CRED_PASSWORD"))){
                    int uid = Integer.parseInt(rs.getString("USER_CRED_USER_ID"));
                        String user_pref="C";
                        String set = rs.getString("USER_FNAME");
                        System.out.println("SET "+set);
                        session.setAttribute("uid",uid);
                        session.setAttribute("name",set);
                        Statement st4=con.createStatement();
                        ResultSet rs1=st4.executeQuery("select USER_PREFERENCES from user_tbl where user_id="+uid);
                        if(rs1.next()){
                            user_pref=rs1.getString(1);
                            System.out.println("User_preference="+rs1.getString(1));
                        }
                        session.setAttribute("user_pref",user_pref);
                        java.util.Date date = new Date();
                        java.sql.Timestamp sqlDate = new Timestamp(date.getTime());
                                
                        String ip = request.getRemoteAddr();
                        
                        try{
                            PreparedStatement st1 = con.prepareStatement("insert into user_log_tbl(USER_LOG_USER_ID,USER_LOG_STATUS,USER_LOG_IN,USER_IP_ADDRESS ) values (?,?,?,?)");
                            st1.setInt(1,uid);
                            st1.setString(2,"active");
                            st1.setTimestamp(3, sqlDate);
                            st1.setString(4, ip);
                            int ins = st1.executeUpdate();
                            response.sendRedirect("home.jsp");
                        }catch(Exception c){
                            c.printStackTrace();
                           response.sendRedirect("home.jsp");
                        }

                         
                }
                else{
                    response.sendRedirect("index.jsp?msg=Login failed");
                }}
            }

            else{
                response.sendRedirect("index.jsp?msg=Username or Password do not match");
            }
        }catch(ClassNotFoundException c){
            c.printStackTrace();
         }catch(SQLException c){
             c.printStackTrace();
            response.sendRedirect("index.jsp?msg=Something went wrong with database");
          }finally {
            out.close();
            con.close();
           }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(DoLogin.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchPaddingException ex) {
            Logger.getLogger(DoLogin.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(DoLogin.class.getName()).log(Level.SEVERE, null, ex);
        }
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(DoLogin.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchPaddingException ex) {
            Logger.getLogger(DoLogin.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(DoLogin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

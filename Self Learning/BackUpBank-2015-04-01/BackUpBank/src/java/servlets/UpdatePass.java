/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import connection.DBConnection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import model.PasswordEncoder;

/**
 *
 * @author employee
 */
public class UpdatePass extends HttpServlet {
    Connection con = null;
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet UpdatePass</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet UpdatePass at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
            */
        } finally { 
            out.close();
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        //processRequest(request, response);
        try{
            String opass = request.getParameter("oldpass");
            opass=opass.replace(" ", "+");
            System.out.println(opass);


             String pass = request.getParameter("newpass");
            int userid =Integer.parseInt(request.getParameter("id"));
            System.out.println(opass+" and  "+userid+" ");
            String username = "";
            con = DBConnection.getConnection();
            Statement st = con.createStatement();

            ResultSet rs = st.executeQuery("select user_cred_uname from user_cred_tbl where  user_cred_id ="+userid+" and user_cred_password ='"+opass+"'");

            if(rs.next()){
                System.out.println("Ankit");
                username = rs.getString(1);
                System.out.println("Username "+username);

             PasswordEncoder pw = PasswordEncoder.getInstance();
             String passw = pw.encode(pass, username);
                System.out.println("Enc "+passw);
            int update = st.executeUpdate("update user_cred_tbl set user_cred_password ='"+passw+"' where user_cred_id ="+userid);

            if(update>0){
                response.sendRedirect("index.jsp?smsg=Password updated successfully");
            }else{
                response.sendRedirect("index.jsp?fmsg=Updation Failed. Try Again by cliking the link");
             }
            }
            else{
                response.sendRedirect("accinfo.jsp?msg=Use new link to update");
            }
        }catch(ClassNotFoundException c){
            
            c.printStackTrace();
         }catch(SQLException c){
             
            c.printStackTrace();
         }catch(Exception c){
             
            c.printStackTrace();
          }



    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package servlets;

import connection.DBConnection;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.Statement;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author employee
 */
public class EditDocRecord extends HttpServlet {
    Connection con=null;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
                HttpSession session=request.getSession(true);
                String id=(String)session.getAttribute("id");
                int lockerId=Integer.parseInt(request.getParameter("lockerId"));
                System.out.println("locker:"+lockerId);
             System.out.println(id);
           int contentId=Integer.parseInt(id);
             System.out.println(contentId);
           String fileName=request.getParameter("editdocfilename");
           String fileDesc=request.getParameter("editdocfiledesc");
             System.out.println("File Name "+fileName+" File Dc "+fileDesc);
            if (fileDesc == null) {
                fileDesc = "N/A";
            }
            if (fileDesc.equals("")) {
                fileDesc = "N/A";
            }
            con=DBConnection.getConnection();
            Statement st=con.createStatement();
            int stat=st.executeUpdate("update locker_content_tbl set locker_content_name='"+fileName+"',locker_content_desc='"+fileDesc+"' where locker_content_id="+contentId);
             if(stat>0){
                if(lockerId==1)
                response.sendRedirect("userhome.jspusmsg=Record edited successfully!&nextpage=1&label_id=0");
                else if(lockerId==2)
                    response.sendRedirect("usermusic.jsp?usmsg=Record edited successfully!&nextpage=1&label_id=0");
                 else if(lockerId==3)
                    response.sendRedirect("userphoto.jsp?usmsg=Record edited successfully!&nextpage=1&label_id=0");
                 else if(lockerId==4)
                    response.sendRedirect("uservideo.jsp?usmsg=Record edited successfully!&nextpage=1&label_id=0");
            }
            else{
                 if(lockerId==1)
                     response.sendRedirect("userhome.jsp?amsg=Record update failed!&nextpage=1&label_id=0");
                 if(lockerId==2)
                     response.sendRedirect("usermusic.jsp?amsg=Record update failed!&nextpage=1&label_id=0");
                 if(lockerId==3)
                     response.sendRedirect("userphoto.jsp?amsg=Record update failed!&nextpage=1&label_id=0");
                  if(lockerId==4)
                     response.sendRedirect("uservideo.jsp?amsg=Record update failed!&nextpage=1&label_id=0");
            }
        }catch(Exception e){
            e.printStackTrace();
        }



    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
       // processRequest(request, response);



    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

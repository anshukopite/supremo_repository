/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package servlets;

import connection.DBConnection;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.Statement;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author employee
 */
public class adminInboxDelete extends HttpServlet {
    Connection con = null;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet adminInboxDelete</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet adminInboxDelete at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
            */
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
       // processRequest(request, response);
        try{
                String sort = request.getParameter("sort");

                    String msgid = request.getParameter("msgid");
                    String userid = request.getParameter("userid");
                    con = DBConnection.getConnection();
                    Statement st = con.createStatement();
                    String sql = "delete from admin_inbox_tbl where admin_inbox_user_id ="+userid+" and admin_inbox_id="+msgid;
                    int del = st.executeUpdate(sql);
                    if(del>0){
                        response.sendRedirect("admininbox.jsp?msg=Message deleted successfully");
                    }
                    else{
                        response.sendRedirect("admininbox.jsp?msg=Message deletion failed");
                    }



        }catch(ClassNotFoundException c){
            response.sendRedirect("admininbox.jsp?msg=Something went wrong");
            c.printStackTrace();
         }catch(Exception c){
             response.sendRedirect("admininbox.jsp?msg=Something went wrong");
            c.printStackTrace();
          }

    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

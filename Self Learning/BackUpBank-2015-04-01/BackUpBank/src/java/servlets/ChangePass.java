/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package servlets;

import connection.DBConnection;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.PasswordEncoder;

/**
 *
 * @author gopal
 */
public class ChangePass extends HttpServlet {
   Connection con = null;

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ChangePass</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ChangePass at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
            */
        } finally { 
            out.close();
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        //processRequest(request, response);
        HttpSession session = request.getSession();
        try{
            String opass = request.getParameter("oldpass");
            String pass = request.getParameter("newpass");
            System.out.println(opass);
            int userid = (Integer) session.getAttribute("uid");
            System.out.println(opass+" and  "+userid+" ");
            String username = "";
            con = DBConnection.getConnection();
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("select user_cred_id,user_cred_uname,user_cred_password from user_cred_tbl where user_cred_user_id ="+ userid);
            if(rs.next()){
                System.out.println("Ankit");
                int userCredId = rs.getInt(1);
                username = rs.getString(2);
                PasswordEncoder opw = PasswordEncoder.getInstance();
            String epassw = opw.encode(opass, username);
                if(rs.getString("user_cred_password").equals(epassw)){

                PasswordEncoder pw = PasswordEncoder.getInstance();
             String passw = pw.encode(pass, username);
                System.out.println("Enc "+passw);
            int update = st.executeUpdate("update user_cred_tbl set user_cred_password ='"+passw+"' where user_cred_id ="+userCredId);

            if(update>0){
                response.sendRedirect("changepassword.jsp?smsg=Password updated successfully");
            }else{
                response.sendRedirect("changepassword.jsp?msg=Password updation failed. ");
             }
            }
                else{

                         response.sendRedirect("changepassword.jsp?msg=Old password not matched");
                }
            
            
            }
            else{
                response.sendRedirect("logout.jsp");
            }
        }catch(ClassNotFoundException c){
            c.printStackTrace();
         }catch(SQLException c){
            c.printStackTrace();
         }catch(Exception c){
            c.printStackTrace();
          }




    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package servlets;

import connection.DBConnection;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.SetImportantNumber;

/**
 *
 * @author employee
 */
public class ImportantNumber extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ImportantNumber</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ImportantNumber at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
            */
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        //processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        //processRequest(request, response);

        HttpSession session=request.getSession(true);
      int  userId=(Integer)(session.getAttribute("uid"));
        String lables=request.getParameter("country");
       int lcker_label_id=0;



        try{
       Connection con=DBConnection.getConnection();
       String query="select * from locker_label_tbl where LOCKER_LABEL_USER_ID="+userId+" AND LOCKER_LABEL_NAME='"+lables+"'";
       Statement smt=con.createStatement();
       ResultSet rs=smt.executeQuery(query);
       while(rs.next()){
       lcker_label_id=rs.getInt("LOCKER_LABEL_ID");
       }

        }catch(SQLException e){
       e.printStackTrace();
       }catch(ClassNotFoundException e){
       e.printStackTrace();
       }



        PrintWriter out=response.getWriter();


        String PAN=request.getParameter("PAN");
        String password=request.getParameter("password");
        String desc=request.getParameter("desc");
       if(desc==null){
        desc="N/A";
        }
         if(desc.equals("")){
        desc="N/A";
        }
        out.println("emailid"+PAN+"\n password="+password+"\n desc="+desc+"\n locker_id"+lcker_label_id);
      SetImportantNumber obj=new SetImportantNumber();

        boolean status=obj.insertImportantNumberInfo(PAN, password, desc, userId, lcker_label_id);
        if(status==true){
        response.sendRedirect("userimportantnumber.jsp?addimpnomsg=add sucessfully&label_id=0&nextpage=1");
        }
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

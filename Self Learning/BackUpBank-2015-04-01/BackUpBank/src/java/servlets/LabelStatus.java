/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package servlets;

import connection.DBConnection;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.Statement;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author employee
 */
public class LabelStatus extends HttpServlet {
   Connection con = null;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet LabelStatus</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet LabelStatus at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
            */
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
       // processRequest(request, response);
        try{
               HttpSession session = request.getSession();
               String stat = request.getParameter("stat");
               System.out.println("Stat "+stat);
               String locklabelid = request.getParameter("locklabelid");
               System.out.println("lock "+locklabelid);
               int uid = (Integer)session.getAttribute("uid");
                if(stat.equals("docdel")){
                        String updatedoc = "update locker_label_tbl set locker_label_status = 'deactive' where locker_label_id ="+locklabelid;
                        con = DBConnection.getConnection();
                        Statement stdoc = con.createStatement();
                        int insdoc = stdoc.executeUpdate(updatedoc);
                        if(insdoc>0){
                            Statement stinsdoc = con.createStatement();
                            int up = stinsdoc.executeUpdate("update locker_content_tbl set locker_content_locker_label_id = (select locker_label_id from locker_label_tbl where locker_label_user_id="+uid+" and locker_label_name = 'Docs'  ) where LOCKER_CONTENT_LOCKER_LABEL_ID  ="+locklabelid);
                            if(up>0)
                                response.sendRedirect("managelabel.jsp?smsg=Label deleted and data moved to default label.");
                            else{
                                   updatedoc = "update locker_label_tbl set locker_label_status = 'active' where locker_label_id ="+locklabelid;
                                con = DBConnection.getConnection();
                                stdoc = con.createStatement();
                                    insdoc = stdoc.executeUpdate(updatedoc);
                                    response.sendRedirect("managelabel.jsp?amsg=Label cannot be deleted.");
                            }

                        }else{
                              updatedoc = "update locker_label_tbl set locker_label_status = 'active' where locker_label_id ="+locklabelid;
                            con = DBConnection.getConnection();
                            stdoc = con.createStatement();
                            insdoc = stdoc.executeUpdate(updatedoc);
                            if(insdoc>0){
                                response.sendRedirect("managelabel.jsp?amsg=Label cannot be deleted.");
                            }else{
                            response.sendRedirect("managelabel.jsp?emsg=Something went wrong.");
                         }

                        }
               }
               if(stat.equals("musdel")){
                        String updatemus = "update locker_label_tbl set locker_label_status = 'deactive' where locker_label_id ="+locklabelid;
                        con = DBConnection.getConnection();
                        Statement stmus = con.createStatement();
                        int insmus = stmus.executeUpdate(updatemus);
                        if(insmus>0){
                            Statement stinsmus = con.createStatement();
                        try{
                            int up = stinsmus.executeUpdate("update locker_content_tbl set locker_content_locker_label_id = (select locker_label_id from locker_label_tbl where locker_label_user_id="+uid+" and locker_label_name = 'Music'  ) where LOCKER_CONTENT_LOCKER_LABEL_ID  ="+locklabelid);
                            if(up>0)
                                response.sendRedirect("managelabel.jsp?smsg=Label deleted and data moved to default label.");
                            else{
                                  updatemus = "update locker_label_tbl set locker_label_status = 'active' where locker_label_id ="+locklabelid;
                                   con = DBConnection.getConnection();
                                 stmus = con.createStatement();
                                 insmus = stmus.executeUpdate(updatemus);
                                 if(insmus>0)
                                        response.sendRedirect("managelabel.jsp?amsg=Label Not deleted");
                            }
                        }catch(Exception c ){
                            c.printStackTrace();
                        }
                        }else{
                              updatemus = "update locker_label_tbl set locker_label_status = 'active' where locker_label_id ="+locklabelid;
                            con = DBConnection.getConnection();
                            stmus = con.createStatement();
                            insmus = stmus.executeUpdate(updatemus);
                            if(insmus>0){
                                response.sendRedirect("managelabel.jsp?amsg=Label cannot be deleted.");
                            }else{
                            response.sendRedirect("managelabel.jsp?emsg=Something went wrong.");
                         }

                        }
               }

               if(stat.equals("phodel")){
                        String updatemus = "update locker_label_tbl set locker_label_status = 'deactive' where locker_label_id ="+locklabelid;
                        con = DBConnection.getConnection();
                        Statement stmus = con.createStatement();
                        int insmus = stmus.executeUpdate(updatemus);
                        if(insmus>0){
                            Statement stinsmus = con.createStatement();
                        try{
                            int up = stinsmus.executeUpdate("update locker_content_tbl set locker_content_locker_label_id = (select locker_label_id from locker_label_tbl where locker_label_user_id="+uid+" and locker_label_name = 'Photos'  ) where LOCKER_CONTENT_LOCKER_LABEL_ID  ="+locklabelid);
                            if(up>0)
                                response.sendRedirect("managelabel.jsp?smsg=Label deleted and data moved to default label.");
                            /*else{
                                  updatemus = "update locker_label_tbl set locker_label_status = 1 where locker_label_id ="+locklabelid;
                                   con = DBConnection.getConnection();
                                 stmus = con.createStatement();
                                 insmus = stmus.executeUpdate(updatemus);
                                 if(insmus>0)
                                        response.sendRedirect("managelabel.jsp?amsg=Label Not deleted");
                            }*/
                        }catch(Exception c ){
                            c.printStackTrace();
                        }
                        }else{
                              updatemus = "update locker_label_tbl set locker_label_status = 'active' where locker_label_id ="+locklabelid;
                            con = DBConnection.getConnection();
                            stmus = con.createStatement();
                            insmus = stmus.executeUpdate(updatemus);
                            if(insmus>0){
                                response.sendRedirect("managelabel.jsp?amsg=Label cannot be deleted.");
                            }else{
                            response.sendRedirect("managelabel.jsp?emsg=Something went wrong.");
                         }

                        }
               }


               if(stat.equals("viddel")){
                        String updatemus = "update locker_label_tbl set locker_label_status = 'deactive' where locker_label_id ="+locklabelid;
                        con = DBConnection.getConnection();
                        Statement stmus = con.createStatement();
                        int insmus = stmus.executeUpdate(updatemus);
                        if(insmus>0){
                            Statement stinsmus = con.createStatement();
                        try{
                            int up = stinsmus.executeUpdate("update locker_content_tbl set locker_content_locker_label_id = (select locker_label_id from locker_label_tbl where locker_label_user_id="+uid+" and locker_label_name = 'Videos'  ) where LOCKER_CONTENT_LOCKER_LABEL_ID  ="+locklabelid);
                            if(up>0)
                                response.sendRedirect("managelabel.jsp?smsg=Label deleted and data moved to default label.");
                            else{
                                  updatemus = "update locker_label_tbl set locker_label_status = 'active' where locker_label_id ="+locklabelid;
                                   con = DBConnection.getConnection();
                                 stmus = con.createStatement();
                                 insmus = stmus.executeUpdate(updatemus);
                                 if(insmus>0)
                                        response.sendRedirect("managelabel.jsp?amsg=Label Not deleted");
                            }
                        }catch(Exception c ){
                            c.printStackTrace();
                        }
                        }else{
                              updatemus = "update locker_label_tbl set locker_label_status = 'active' where locker_label_id ="+locklabelid;
                            con = DBConnection.getConnection();
                            stmus = con.createStatement();
                            insmus = stmus.executeUpdate(updatemus);
                            if(insmus>0){
                                response.sendRedirect("managelabel.jsp?amsg=Label cannot be deleted.");
                            }else{
                            response.sendRedirect("managelabel.jsp?emsg=Something went wrong.");
                         }

                        }
               }

                if(stat.equals("creddel")){
                        String updatemus = "update locker_label_tbl set locker_label_status = 'deactive' where locker_label_id ="+locklabelid;
                        con = DBConnection.getConnection();
                        Statement stmus = con.createStatement();
                        int insmus = stmus.executeUpdate(updatemus);
                        if(insmus>0){
                            Statement stinsmus = con.createStatement();
                        try{
                            int up = stinsmus.executeUpdate("update locker_content_tbl set locker_content_locker_label_id = (select locker_label_id from locker_label_tbl where locker_label_user_id="+uid+" and locker_label_name = 'Other Credentials'  ) where LOCKER_CONTENT_LOCKER_LABEL_ID  ="+locklabelid);
                            if(up>0)
                                response.sendRedirect("managelabel.jsp?smsg=Label deleted and data moved to default label.");
                            else{
                                  updatemus = "update locker_label_tbl set locker_label_status = 'active' where locker_label_id ="+locklabelid;
                                   con = DBConnection.getConnection();
                                 stmus = con.createStatement();
                                 insmus = stmus.executeUpdate(updatemus);
                                 if(insmus>0)
                                        response.sendRedirect("managelabel.jsp?amsg=Label Not deleted");
                            }
                        }catch(Exception c ){
                            c.printStackTrace();
                        }
                        }else{
                              updatemus = "update locker_label_tbl set locker_label_status = 'active' where locker_label_id ="+locklabelid;
                            con = DBConnection.getConnection();
                            stmus = con.createStatement();
                            insmus = stmus.executeUpdate(updatemus);
                            if(insmus>0){
                                response.sendRedirect("managelabel.jsp?amsg=Label cannot be deleted.");
                            }else{
                            response.sendRedirect("managelabel.jsp?emsg=Something went wrong.");
                         }

                        }
               }


                if(stat.equals("impnodel")){
                        String updatemus = "update locker_label_tbl set locker_label_status = 'deactive' where locker_label_id ="+locklabelid;
                        con = DBConnection.getConnection();
                        Statement stmus = con.createStatement();
                        int insmus = stmus.executeUpdate(updatemus);
                        if(insmus>0){
                            Statement stinsmus = con.createStatement();
                        try{
                            int up = stinsmus.executeUpdate("update locker_content_tbl set locker_content_locker_label_id = (select locker_label_id from locker_label_tbl where locker_label_user_id="+uid+" and locker_label_name = 'Other Important Numbers'  ) where LOCKER_CONTENT_LOCKER_LABEL_ID  ="+locklabelid);
                            if(up>0)
                                response.sendRedirect("managelabel.jsp?smsg=Label deleted and data moved to default label.");
                            else{
                                  updatemus = "update locker_label_tbl set locker_label_status = 'active' where locker_label_id ="+locklabelid;
                                   con = DBConnection.getConnection();
                                 stmus = con.createStatement();
                                 insmus = stmus.executeUpdate(updatemus);
                                 if(insmus>0)
                                        response.sendRedirect("managelabel.jsp?amsg=Label Not deleted");
                            }
                        }catch(Exception c ){
                            c.printStackTrace();
                        }
                        }else{
                              updatemus = "update locker_label_tbl set locker_label_status = 'active' where locker_label_id ="+locklabelid;
                            con = DBConnection.getConnection();
                            stmus = con.createStatement();
                            insmus = stmus.executeUpdate(updatemus);
                            if(insmus>0){
                                response.sendRedirect("managelabel.jsp?amsg=Label cannot be deleted.");
                            }else{
                            response.sendRedirect("managelabel.jsp?emsg=Something went wrong.");
                         }

                        }
               }


              if(stat.equals("docre")){

                        String rename = request.getParameter("rename");
                        System.out.println(rename);
                        String updatemus = "update locker_label_tbl set LOCKER_LABEL_NAME ='"+rename+"' where locker_label_id ="+locklabelid;
                        con = DBConnection.getConnection();
                        Statement stmus = con.createStatement();
                        int insmus = stmus.executeUpdate(updatemus);
                        if(insmus>0){
                                response.sendRedirect("managelabel.jsp?rmsg=Label Renamed");
                            }                        
                        else{
                                response.sendRedirect("managelabel.jsp?nrmsg=Label cannot be renamed.");
                            
                         }                        
               }


               if(stat.equals("musre")){

                        String rename = request.getParameter("rename");
                        System.out.println(rename);
                        String updatemus = "update locker_label_tbl set LOCKER_LABEL_NAME ='"+rename+"' where locker_label_id ="+locklabelid;
                        con = DBConnection.getConnection();
                        Statement stmus = con.createStatement();
                        int insmus = stmus.executeUpdate(updatemus);
                        if(insmus>0){
                                response.sendRedirect("managelabel.jsp?rmsg=Label Renamed");
                            }
                        else{
                                response.sendRedirect("managelabel.jsp?nrmsg=Label cannot be renamed.");

                         }
               }

                if(stat.equals("phore")){

                        String rename = request.getParameter("rename");
                        System.out.println(rename);
                        String updatemus = "update locker_label_tbl set LOCKER_LABEL_NAME ='"+rename+"' where locker_label_id ="+locklabelid;
                        con = DBConnection.getConnection();
                        Statement stmus = con.createStatement();
                        int insmus = stmus.executeUpdate(updatemus);
                        if(insmus>0){
                                response.sendRedirect("managelabel.jsp?rmsg=Label Renamed");
                            }
                        else{
                                response.sendRedirect("managelabel.jsp?nrmsg=Label cannot be renamed.");

                         }
               }

                if(stat.equals("vidre")){

                        String rename = request.getParameter("rename");
                        System.out.println(rename);
                        String updatemus = "update locker_label_tbl set LOCKER_LABEL_NAME ='"+rename+"' where locker_label_id ="+locklabelid;
                        con = DBConnection.getConnection();
                        Statement stmus = con.createStatement();
                        int insmus = stmus.executeUpdate(updatemus);
                        if(insmus>0){
                                response.sendRedirect("managelabel.jsp?rmsg=Label Renamed");
                            }
                        else{
                                response.sendRedirect("managelabel.jsp?nrmsg=Label cannot be renamed.");

                         }
               }

                if(stat.equals("credre")){

                        String rename = request.getParameter("rename");
                        System.out.println(rename);
                        String updatemus = "update locker_label_tbl set LOCKER_LABEL_NAME ='"+rename+"' where locker_label_id ="+locklabelid;
                        con = DBConnection.getConnection();
                        Statement stmus = con.createStatement();
                        int insmus = stmus.executeUpdate(updatemus);
                        if(insmus>0){
                                response.sendRedirect("managelabel.jsp?rmsg=Label Renamed");
                            }
                        else{
                                response.sendRedirect("managelabel.jsp?nrmsg=Label cannot be renamed.");

                         }
               }
                if(stat.equals("impnore")){

                        String rename = request.getParameter("rename");
                        System.out.println(rename);
                        String updatemus = "update locker_label_tbl set LOCKER_LABEL_NAME ='"+rename+"' where locker_label_id ="+locklabelid;
                        con = DBConnection.getConnection();
                        Statement stmus = con.createStatement();
                        int insmus = stmus.executeUpdate(updatemus);
                        if(insmus>0){
                                response.sendRedirect("managelabel.jsp?rmsg=Label Renamed");
                            }
                        else{
                                response.sendRedirect("managelabel.jsp?anrmsg=Label cannot be renamed.");

                         }
               }

        }catch(Exception c){
             response.sendRedirect("managelabel.jsp?emsg=Something went wrong in database.");
            c.printStackTrace();
         }


    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package servlets;

import connection.DBConnection;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.Statement;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author employee
 */
public class Status extends HttpServlet {
    Connection con = null;
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
                String status = request.getParameter("status");
                System.out.println("Status "+status);
                int stat = Integer.parseInt(status);
                System.out.println(request.getParameter("userid"));
                int uid = Integer.parseInt(request.getParameter("userid"));
                String by=request.getParameter("by");

                System.out.println("Status "+uid);
                 con = DBConnection.getConnection();
                 
                 if(stat==1&&by.equals("admin")){
                    Statement st = con.createStatement();
                    Statement stmt = con.createStatement();
                    int updateUser = st.executeUpdate("Update User_tbl set user_status = 0 where user_id="+uid);
                    int updateCred = stmt.executeUpdate("Update User_cred_tbl set user_cred_status = 0 where user_cred_id="+uid);
                    if(updateUser >0 && updateCred>0){
                        response.sendRedirect("adminuserlist.jsp?dsmsg=Deactivation Successful");
                    }
                    else{
                        response.sendRedirect("adminuserlist.jsp?dfmsg=Deactivation Failed");
                    }
                 }  else if(stat==1&&by.equals("user")){
                    Statement st = con.createStatement();
                    Statement stmt = con.createStatement();
                    int updateUser = st.executeUpdate("Update User_tbl set user_status = 0 where user_id="+uid);
                    int updateCred = stmt.executeUpdate("Update User_cred_tbl set user_cred_status = 0 where user_cred_id="+uid);
                    if(updateUser >0 && updateCred>0){

                        response.sendRedirect("deactivationmail.jsp");
                    }
                    else{
                        response.sendRedirect("myprofile.jsp?stat=view");
                    }
                 }
                 else if(stat==0&&by.equals("admin")){
                    Statement st = con.createStatement();
                    Statement stmt = con.createStatement();
                    int updateUser = st.executeUpdate("Update User_tbl set user_status = 1 where user_id="+uid);
                    int updateCred = stmt.executeUpdate("Update User_cred_tbl set user_cred_status = 1 where user_cred_id="+uid);
                    if(updateUser >0 && updateCred>0){
                        response.sendRedirect("adminuserlist.jsp?asmsg=Activation Successful");
                    }
                    else{
                        response.sendRedirect("adminuserlist.jsp?afmsg=Activation Failed");
                    }

                 }
           
        }catch(ClassNotFoundException c){
            
            c.printStackTrace();
         }catch(Exception c){
             response.sendRedirect("adminuserlist.jsp?msg=Something went wrong in database");
            c.printStackTrace();
          } finally {
            out.close();
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

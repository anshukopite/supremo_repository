/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import connection.DBConnection;
import java.io.File;
import java.sql.Statement;
import model.GetFileSize;


/**
 *
 * @author employee
 */
public class SetDocRecord extends HttpServlet {

    Connection con=null;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        try {
            HttpSession session = request.getSession();
            int labelId = Integer.parseInt((String) session.getAttribute("fileLabelId"));
            int lockerId = Integer.parseInt((String) session.getAttribute("fileLockerId"));
            int uid;
            uid = (Integer)session.getAttribute("uid");
            String getDocFile = (String) session.getAttribute("filePath");
            String getDocFileName = (String) session.getAttribute("fileName");
            String getDocFileDesc = (String) session.getAttribute("fileDesc");
            java.util.Date date = new java.util.Date();
            Timestamp sqlDate = new Timestamp(date.getTime());
            
            if (getDocFileDesc == null) {
                getDocFileDesc = "N/A";
            }
            if (getDocFileDesc.equals("")) {
                getDocFileDesc = "N/A";
            }
            String getDocFileExt = (String) session.getAttribute("fileExt");
            System.out.println(getDocFile + getDocFileDesc + getDocFileName);
            String sql = "insert into locker_content_tbl (locker_content_path,locker_content_desc,locker_content_user_id,locker_content_locker_id,locker_content_status,locker_content_name,locker_content_extension,locker_content_locker_label_id,record_created_on) values (?,?,?,?,?,?,?,?,?)";
            con = DBConnection.getConnection();
            PreparedStatement ps = con.prepareStatement(sql);
            /* COMMAND FOR INSERTION OF CONTENT IN DATABASE  */
            ps.setString(1, getDocFile);
            ps.setString(2, getDocFileDesc);
            ps.setInt(3, uid);
            ps.setInt(4, lockerId);
            ps.setString(5, "active");
            ps.setString(6, getDocFileName);
            ps.setString(7, getDocFileExt);
            ps.setInt(8, labelId);
            ps.setTimestamp(9, sqlDate);
            
            int stat = ps.executeUpdate();
            if (stat > 0) {  // IF INSERTION SUCCESSFUL
                        /*  FILE SIZE LIMIT CHECK CODE BELOW */
                double size = 0;
                int fileCount = 0;
                String sizeLabel = "KB";

                String sql1 = "select locker_content_path from locker_content_tbl where locker_content_user_id=" + (Integer) session.getAttribute("uid");
                GetFileSize file = new GetFileSize();

                Statement st = con.createStatement();
                Statement st1 = con.createStatement();
                ResultSet rs1 = st.executeQuery(sql1); // QUERY FOR PATH OF EACH FILE
                       /* GETTING TOTAL SIZE OF THE CONTENTS OF USER  */
                while (rs1.next()) {
                    size = size + file.GetFileSize(getServletContext().getRealPath("/") + rs1.getString(1));
                    fileCount = file.GetFileCount(getServletContext().getRealPath("/") + rs1.getString(1));
                }
                size = size / 1024.00; // File Size Converted to MB

                if (size > 20) {    //If size limit is reached.
                    int contentId = 0;
                    /* Query for fetching the above content Id  */
                    Statement st3 = con.createStatement();
                    ResultSet rs2 = st.executeQuery("select max(locker_content_id) from locker_content_tbl");
                    if (rs2.next()) {
                        contentId = rs2.getInt(1);
                    }

                    /* Query for DELETING/updating the above contenT */
                     Statement st4 = con.createStatement();
                    int stat1 = st.executeUpdate("update locker_content_tbl set locker_content_status='deactive' where locker_content_id=" + contentId);
                    if(stat1>0){
                        Statement st5=con.createStatement();
                        ResultSet rs4=st5.executeQuery("select locker_content_path from locker_content_tbl where locker_content_id="+contentId);
                        if(rs4.next()){
                            String Path=getServletContext().getRealPath("/")+rs4.getString(1);
                            File deleteFile=new File(Path);
                            if(deleteFile.exists()){
                                deleteFile.delete();
                        }
                        }
                    session.removeAttribute("filePath");
                    session.removeAttribute("fileName");
                    session.removeAttribute("fileDesc");
                    session.removeAttribute("fileExt");
                    session.removeAttribute("fileLabelId");
                    session.removeAttribute("fileLockerId");

                    if (lockerId == 1) {
                        response.sendRedirect("userhomeadddoc.jsp?emsg=Error:Uploading size limit reached");
                    } else if (lockerId == 2) {
                        response.sendRedirect("userhomeaddmusic.jsp?emsg=Error:Uploading size limit reached");
                    } else if (lockerId == 3) {
                        response.sendRedirect("useraddphoto.jsp?emsg=Error:Uploading size limit reached");
                    } else if (lockerId == 4) {
                        response.sendRedirect("useraddvideos.jsp?emsg=Error:Uploading size limit reached");
                    }
                    }
                    else{
                        session.removeAttribute("filePath");
                        session.removeAttribute("fileName");
                        session.removeAttribute("fileDesc");
                        session.removeAttribute("fileExt");
                        session.removeAttribute("fileLabelId");
                        session.removeAttribute("fileLockerId");
                        response.sendRedirect("userhomeadddoc.jsp?emsg=Error:Somthing went wrong with the database");
                    }

                }
                else { // if limit is NOT reached
                    session.removeAttribute("filePath");
                    session.removeAttribute("fileName");
                    session.removeAttribute("fileDesc");
                    session.removeAttribute("fileExt");
                    session.removeAttribute("fileLabelId");
                    session.removeAttribute("fileLockerId");
                    if (lockerId == 1) {
                        response.sendRedirect("userhome.jsp?smsg=Record Upload Successful!&nextpage=1&label_id=0");
                    } else if (lockerId == 2) {
                        response.sendRedirect("usermusic.jsp?smsg=Record Upload Successful!&nextpage=1&label_id=0");
                    } else if (lockerId == 3) {
                        response.sendRedirect("userphoto.jsp?smsg=Record Upload Successful!&nextpage=1&label_id=0");
                    } else if (lockerId == 4) {
                        response.sendRedirect("uservideo.jsp?smsg=Record Upload Successful!&nextpage=1&label_id=0");
                    }
                }
            } else { //IF INSERTION FAILS
                session.removeAttribute("filePath");
                session.removeAttribute("fileName");
                session.removeAttribute("fileDesc");
                session.removeAttribute("fileExt");
                session.removeAttribute("fileLabelId");
                session.removeAttribute("fileLockerId");
                if (lockerId == 1) {
                    response.sendRedirect("userhomeadddoc.jsp?fmsg=Record Upload failed!");
                }
                if (lockerId == 2) {
                    response.sendRedirect("userhomeaddmusic.jsp?fmsg=Record Upload failed!");
                }
                if (lockerId == 3) {
                    response.sendRedirect("useraddphoto.jsp?fmsg=Record Upload failed!");
                }
                if (lockerId == 4) {
                    response.sendRedirect("useraddvideos.jsp?fmsg=Record Upload failed!");
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        //processRequest(request, response);


    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

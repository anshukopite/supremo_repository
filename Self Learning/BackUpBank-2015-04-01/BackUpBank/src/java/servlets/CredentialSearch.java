/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author employee
 */
public class CredentialSearch extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CredentialSearch</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CredentialSearch at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
            */
        } finally { 
            out.close();
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        //processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
       // processRequest(request, response);
        int a=0;
         int locker_id=0;
         int label_id=0;
        HttpSession session=request.getSession(true);
        int user_id=(Integer)session.getAttribute("uid");
        PrintWriter out=response.getWriter();
        String searchvalue=request.getParameter("searchvalue");
         //String label_id1 = request.getParameter("label_id");
                          String pageno = request.getParameter("nextpage");
         String search=request.getParameter("search");

                    locker_id=Integer.parseInt(request.getParameter("locker_id"));
                    label_id=Integer.parseInt(request.getParameter("lables"));
         if(search.equals("NameStartwith")){
             a=0;}
         else {if(search.equals("NameContain"))
             a=1;
         }
         out.print("searchvalue="+searchvalue+"\n"+search);
         model.GetCredentialsInfo obj=new model.GetCredentialsInfo();
         ArrayList list=obj.getSearchCredentialInfo(searchvalue, a, user_id,locker_id,label_id);
         System.out.println("here");
         session.setAttribute("credsearchlist", list);
         if(locker_id==5)
         response.sendRedirect("usercredential.jsp?seacrmsg=serach&label_id="+label_id+"&nextpage="+pageno);
         else if(locker_id==6)
         response.sendRedirect("userimportantnumber.jsp?seacrmsg=serach&label_id="+label_id+"&nextpage="+pageno);
          else if(locker_id==4)
         response.sendRedirect("uservideo.jsp?seacrmsg=serach&label_id="+label_id+"&nextpage="+pageno);
          else if(locker_id==3)
         response.sendRedirect("userphoto.jsp?seacrmsg=serach&label_id="+label_id+"&nextpage="+pageno);
          else if(locker_id==2)
         response.sendRedirect("usermusic.jsp?seacrmsg=serach&label_id="+label_id+"&nextpage="+pageno);
          else if(locker_id==1)
         response.sendRedirect("userhome.jsp?seacrmsg=serach&label_id="+label_id+"&nextpage="+pageno);


    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package servlets;

import connection.DBConnection;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.PasswordEncoder;

/**
 *
 * @author employee
 */
public class RecheckPassword extends HttpServlet {
    Connection con=null;
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet RecheckPassword</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet RecheckPassword at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
            */
        } finally { 
            out.close();
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
       // processRequest(request, response);

        try{
                HttpSession session=request.getSession(true);
                int uid=(Integer)session.getAttribute("uid");
                String userName=(String)session.getAttribute("name");
                String password=request.getParameter("repassword");
                String lockerId=request.getParameter("locker_id");
                if(lockerId==null){
                    response.sendRedirect("home.jsp?msg=No locker to enter!!");
                }
                int lockerIdInt=Integer.parseInt(lockerId);
                con=DBConnection.getConnection();
                Statement st=con.createStatement();
                System.out.println("User Id check #re "+uid);
                ResultSet rs=st.executeQuery("select u.user_cred_password,u.user_cred_uname from user_cred_tbl u where u.user_cred_user_id="+uid);
                if(rs.next()){

                        PasswordEncoder pw = PasswordEncoder.getInstance();
                        String passw = pw.encode(password, rs.getString(2));

                        if(passw.equals(rs.getString(1))){
                            session.setAttribute("repass", lockerIdInt);
                            if(lockerIdInt==1){

                        response.sendRedirect("userhome.jsp?nextpage=1&label_id=0");
                            }
                            else if(lockerIdInt==2){
                        response.sendRedirect("usermusic.jsp?nextpage=1&label_id=0");
                            }
                            else if(lockerIdInt==3){
                        response.sendRedirect("userphoto.jsp?nextpage=1&label_id=0");
                            }
                            else if(lockerIdInt==4){
                        response.sendRedirect("uservideo.jsp?nextpage=1&label_id=0");
                            }
                            else if(lockerIdInt==5){
                        response.sendRedirect("usercredential.jsp?nextpage=1&label_id=0");
                            }
                            else if(lockerIdInt==6){
                        response.sendRedirect("userimportantnumber.jsp?nextpage=1&label_id=0");
                            }
                             else if(lockerIdInt==0){
                        response.sendRedirect("userpreferences.jsp?");
                            }
                            else{
                                session.removeAttribute("repass");
                                response.sendRedirect("home.jsp?msg=No locker to enter");
                            }

                        }
                        else{
                            if(lockerIdInt==1){
                        response.sendRedirect("repassword.jsp?locker_id=1&msg=Password not matched");
                            }
                            else if(lockerIdInt==2){
                        response.sendRedirect("repassword.jsp?locker_id=2&msg=Password not matched");
                            }
                            else if(lockerIdInt==3){
                        response.sendRedirect("repassword.jsp?locker_id=3&msg=Password not matched");
                            }
                            else if(lockerIdInt==4){
                        response.sendRedirect("repassword.jsp?locker_id=4&msg=Password not matched");
                            }
                            else if(lockerIdInt==5){
                        response.sendRedirect("repassword.jsp?locker_id=5&msg=Password not matched");
                            }
                            else if(lockerIdInt==6){
                        response.sendRedirect("repassword.jsp?locker_id=6&msg=Password not matched");
                            }
                            else{
                                response.sendRedirect("home.jsp?msg=No locker to enter");
                            }
                        }
                }
                else{
                        
                        response.sendRedirect("repassword.jsp?msg=Session Expired");
                }
        }catch(SQLException e){
                e.printStackTrace();
                response.sendRedirect("repassword.jsp?msg=Something went wrong with the database");
        }
        catch(Exception e){
                e.printStackTrace();
                response.sendRedirect("repassword.jsp?msg=Something went wrong!! Sorry for the inconvinience");
        }

    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

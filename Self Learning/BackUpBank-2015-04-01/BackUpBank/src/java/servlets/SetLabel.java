/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package servlets;

import connection.DBConnection;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author gopal
 */
public class SetLabel extends HttpServlet {
   Connection con=null;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {

                HttpSession session=request.getSession();
                int UserId=(Integer) session.getAttribute("uid");
                int id=0;
                String labelName=request.getParameter("newlabel");
                int locker_id=Integer.parseInt(request.getParameter("lock_id"));

                con=DBConnection.getConnection();
                Statement st1=con.createStatement();
                ResultSet rs=st1.executeQuery("select max(locker_label_id)+1 from locker_label_tbl");
                if(rs.next()){

                            id=rs.getInt(1);
                }

                Statement st=con.createStatement();

                int stat=st.executeUpdate("insert into locker_label_tbl values("+id+",'"+labelName+"',"+locker_id+","+UserId+",1)");
                if(stat>0){
                    if(locker_id==1){
                        response.sendRedirect("userhomeadddoc.jsp?msg=label created successfully");
                    }
                    if(locker_id==2){
                        response.sendRedirect("userhomeaddmusic.jsp?msg=label created successfully");
                    }
                    if(locker_id==3){
                        response.sendRedirect("useraddphoto.jsp?msg=label created successfully");
                    }
                     if(locker_id==4){
                        response.sendRedirect("useraddvideos.jsp?msg=label created successfully");
                    }
                    if(locker_id==5){
                     response.sendRedirect("addcred.jsp?msg=label created successfully");
                    }
                    if(locker_id==6){
                     response.sendRedirect("addimpno.jsp?msg=label created successfully");
                    }

                }
                else{
                   if(locker_id==1){
                        response.sendRedirect("userhomeadddoc.jsp?msg=label creation failed");
                    }
                    if(locker_id==2){
                        response.sendRedirect("userhomeaddmusic.jsp?msg=label creation failed");
                    }
                    if(locker_id==3){
                        response.sendRedirect("useraddphoto.jsp?msg=label creation failed");
                    }
                     if(locker_id==4){
                        response.sendRedirect("useraddvideos.jsp?msg=label creation failed");
                    }
                    if(locker_id==5){
                     response.sendRedirect("addcred.jsp?msg=label creation failed");
                    }
                    if(locker_id==6){
                     response.sendRedirect("addimpno.jsp?msg=label creation failed");
                    }
                }



        }catch(Exception e){
            e.printStackTrace();
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

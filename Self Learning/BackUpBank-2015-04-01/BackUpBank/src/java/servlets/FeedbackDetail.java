/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package servlets;

import connection.DBConnection;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author employee
 */
public class FeedbackDetail extends HttpServlet {
   Connection con = null;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet FeedbackDetail</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet FeedbackDetail at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
            */
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        //processRequest(request, response);
        HttpSession session = request.getSession();
        try{
              String sub = request.getParameter("subject");
              String msg = request.getParameter("msg");
              int uid = (Integer) session.getAttribute("uid");
              int fId = 0;
              Date date = new Date();
              Timestamp sqlDate = new Timestamp(date.getTime());
               con = DBConnection.getConnection();
               PreparedStatement st = con.prepareStatement("INSERT INTO FEEDBACK_TBL (FEEDBACK_USER_ID,FEEDBACK_MSG,FEEDBACK_RCV_DATE,FEEDBACK_SUBJECT) VALUES (?,?,?,?)");
               //ResultSet rs = st.executeQuery("Select max(feedback_id) from feedback_tbl");
               st.setInt(1, uid);
               st.setString(2, msg);
               st.setTimestamp(3, sqlDate);
               st.setString(4, sub);
               
               int insert = st.executeUpdate();

               if(insert>0){

                    response.sendRedirect("feedback.jsp?msg=Message send successfully");
               }
               else{
                    response.sendRedirect("feedback.jsp?msg=Message sending failed");
               }


        }catch(ClassNotFoundException c){
            response.sendRedirect("feedback.jsp?msg=Something Went Wrong");
            c.printStackTrace();
         }catch(SQLException c){
             response.sendRedirect("feedback.jsp?msg=Something Went Wrong");
            c.printStackTrace();
          }



    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package servlets;

import connection.DBConnection;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author employee
 */
public class DeleteMultiple extends HttpServlet {

    Connection con=null;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        if(session.getAttribute("name")==null){
            response.sendRedirect("index.jsp?status=session expired");
        }
        try {

            String [] contentId=new String[500];
            contentId=request.getParameterValues("content_id");
           
            int lockerId=Integer.parseInt(request.getParameter("lockerId"));
            con=DBConnection.getConnection();
            for(int i=0;i<contentId.length;i++){
                System.out.println("ContentId["+i+"]="+contentId[i]);
            Statement st=con.createStatement();
            int stat=st.executeUpdate("update locker_content_tbl set locker_content_status=0 where locker_content_id="+contentId[i]);
            Statement st1=con.createStatement();
            ResultSet rs=st1.executeQuery("select locker_content_path from locker_content_tbl where locker_content_id="+contentId[i]);
            if(stat>0){
                if(rs.next()){
                            String Path=getServletContext().getRealPath("/")+rs.getString(1);
                            File deleteFile=new File(Path);
                            if(deleteFile.exists()){
                                deleteFile.delete();
                            }


                    }
            }
            else{
                 if(lockerId==1)
                     response.sendRedirect("userhome.jsp?msg=Record deletion failed!&label_id=0&nextpage=1&pageno=1");
                 if(lockerId==2)
                     response.sendRedirect("usermusic.jsp?msg=Record deletion failed!&label_id=0&nextpage=1&pageno=1");
                 if(lockerId==3)
                     response.sendRedirect("userphoto.jsp?msg=Record deletion failed!&label_id=0&nextpage=1&pageno=1");
                  if(lockerId==4)
                     response.sendRedirect("uservideo.jsp?msg=Record deletion failed!&label_id=0&nextpage=1&pageno=1");
            }
            }
                if(lockerId==1){
                        response.sendRedirect("userhome.jsp?msg=Record deletion success!&label_id=0&nextpage=1&pageno=1");
                }
                else if(lockerId==2)
                    response.sendRedirect("usermusic.jsp?msg=Record deletion success!&label_id=0&nextpage=1&pageno=1");
                else if(lockerId==3)
                    response.sendRedirect("userphoto.jsp?msg=Record deletion success!&label_id=0&nextpage=1&pageno=1");
                 else if(lockerId==4)
                    response.sendRedirect("uservideo.jsp?msg=Record deletion success!&label_id=0&nextpage=1&pageno=1");
                 else if(lockerId==5)
                     response.sendRedirect("usercredential.jsp?msg=Record deletion success!&label_id=0&nextpage=1&pageno=1");
                 else if(lockerId==6)
                     response.sendRedirect("userimportantnumber.jsp?msg=Record deletion success!&label_id=0&nextpage=1&pageno=1");
            
            
            
        }catch(Exception e){
            e.printStackTrace();
        }
        finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

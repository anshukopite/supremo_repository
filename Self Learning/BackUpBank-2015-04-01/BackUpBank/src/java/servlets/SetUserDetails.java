/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package servlets;

import connection.DBConnection;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.Statement;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 *
 * @author gopal
 */
public class SetUserDetails extends HttpServlet {
    Connection con= null;
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet SetUserDetails</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet SetUserDetails at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
            */
        } finally { 
            out.close();
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
       // processRequest(request, response);
        try{
            HttpSession session = request.getSession();
            String fname = request.getParameter("firstname");
            String lname = request.getParameter("lastname");
            String emailid = request.getParameter("emailid");
            String contactno = request.getParameter("contactno");
            int country = Integer.parseInt(request.getParameter("country"));
            int state = Integer.parseInt(request.getParameter("state"));
            int city = Integer.parseInt(request.getParameter("city"));
            int userid = (Integer) session.getAttribute("uid");
            con = DBConnection.getConnection();
            Statement st = con.createStatement();
            String sql = "update user_tbl set user_fname='"+fname+"',user_lname ='"+lname+"',user_email='"+emailid+"',user_contact_number="+contactno+",user_country_id="+country+",user_state_id="+state+",user_city_id="+city+" where user_id ="+userid;
            int update = st.executeUpdate(sql);

            if(update>0){

                session.setAttribute("name", fname);
                response.sendRedirect("myprofile.jsp?stat=view&msg=Profile updated successfully");
            }
            else{
                response.sendRedirect("myprofile.jsp?stat=view&msg=Profile updation unsuccessful");
            }
            
        }catch(Exception c){
            c.printStackTrace();
        }
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

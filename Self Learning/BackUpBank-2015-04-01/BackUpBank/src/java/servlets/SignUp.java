/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package servlets;

import connection.DBConnection;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.NoSuchAlgorithmException;

import java.sql.*;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Registration;
import nl.captcha.Captcha;

/**
 *
 * @author employee
 */
public class SignUp extends HttpServlet {
   Connection con = null;
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            
        } finally { 
            out.close();
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        //processRequest(request, response);

        try{
        HttpSession session = request.getSession(true);
        int userId=0;
        int user_status=1;
        Captcha captcha = (Captcha)
        session.getAttribute(Captcha.NAME);
        request.setCharacterEncoding("UTF-8");
        String answer = request.getParameter("answer");
        boolean status3;
        boolean status1 = false;
        boolean status4 = false;
        if (captcha.isCorrect(answer)) {
                status3 = true;
        } else {
                status3 = false;
        }
        if(status3==true){
        String userName=request.getParameter("username");
        String emailId=request.getParameter("emailid");
        String fname=request.getParameter("firstname");
        String lname=request.getParameter("lastname");
        String repass = request.getParameter("userPref");
            System.out.println("repass "+repass);
        long contactNumber=Long.parseLong(request.getParameter("contactnumber"));
        int country_id=Integer.parseInt(request.getParameter("country"));
        int  state_id=Integer.parseInt(request.getParameter("state"));
        int city_id=Integer.parseInt(request.getParameter("city"));
        String password=request.getParameter("password");
        
        int user_user_type_id=1;
        int status=1;
        boolean status2=false;
        //Registration obj=new Registration();
       // boolean status1=obj.insertUserInfo(fname, lname, emailId, country_id, state_id, city_id, contactNumber, status, user_user_type_id);



             con=DBConnection.getConnection();
             Statement stmt= con.createStatement();

             //String sql="INSERT INTO USER_TBL (USER_FNAME,USER_LNAME,USER_EMAIL,USER_COUNTRY_ID,USER_STATE_ID,USER_CITY_ID,USER_CONTACT_NUMBER,USER_STATUS,USER_USER_TYPE_ID,USER_CREATED_ON,USER_PREFERENCES) VALUES('"+fname+"','"+lname+"','"+emailId+"',"+country_id+","+state_id+","+city_id+","+contactNumber+","+user_status+","+user_user_type_id+",'"+sqlDate+"','"+repass+"';";
             PreparedStatement smt=con.prepareStatement("INSERT INTO USER_TBL (USER_FNAME,USER_LNAME,USER_EMAIL,USER_COUNTRY_ID,USER_STATE_ID,USER_CITY_ID,USER_CONTACT_NUMBER,USER_STATUS,USER_USER_TYPE_ID,USER_CREATED_ON,USER_PREFERENCES) VALUES(?,?,?,?,?,?,?,?,?,?,?)");
             smt.setString(1, fname);
             smt.setString(2, lname);
             smt.setString(3, emailId);
             smt.setInt(4, country_id);
             smt.setInt(5, state_id);
             smt.setInt(6, city_id);
             smt.setLong(7, contactNumber);
             smt.setInt(8, status);
             smt.setInt(9, user_user_type_id);
             smt.setTimestamp(10, null);
             smt.setString(11, repass);
             int i = smt.executeUpdate();
             if(i!=0){
                  status1=true;
                  
            ResultSet rss= stmt.executeQuery("SELECT MAX(user_id) FROM user_tbl");
            while(rss.next()){
            userId=rss.getInt(1);
            }
                  session.setAttribute("uid", userId);
                  session.setAttribute("name", fname);
              }
              System.out.println("After User Info "+status);
                
          if(status1==true){
            try {
                int uid= (Integer)session.getAttribute("uid");

                model.Registration obj = new Registration();
                status2 = obj.insertCredentialsDetails(userName,password,uid);
                System.out.println("User Id check "+uid);
                if(status2==true){
                    
                     status4 = obj.insertLabelDetails(uid);
                }
            } catch (NoSuchAlgorithmException ex) {
                ex.printStackTrace();
            } catch (NoSuchPaddingException ex) {
                ex.printStackTrace();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
         }
         if(status4==true){
            System.out.println("sucessful insertion");
          session.setAttribute("user_pref",repass);
            response.sendRedirect("index.jsp?smsg=Registration Successful!!");
         }else{
                session.removeAttribute("name");
                session.removeAttribute("uid");
                
             response.sendRedirect("signup.jsp?msg=Sign Up Failed");
          System.out.println("error in insertion ");
         }
        }else{
                session.removeAttribute("name");
                session.removeAttribute("uid");
                session.removeAttribute("user_pref");
            response.sendRedirect("signup.jsp?msg=Characters not matched");
          System.out.println("error in insertion ");
        }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

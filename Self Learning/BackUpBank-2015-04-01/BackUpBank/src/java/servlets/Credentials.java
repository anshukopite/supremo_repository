/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package servlets;

import connection.DBConnection;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.SetCredentialsInfo;

/**
 *
 * @author employee
 */
public class Credentials extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Credentials</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Credentials at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
            */
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
       // processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        //processRequest(request, response);
        int userId=1;
        HttpSession session=request.getSession(true);
      userId=(Integer)(session.getAttribute("uid"));

        int lcker_label_id=0;

        PrintWriter out=response.getWriter();
        String emailId=request.getParameter("emailId");
        String password=request.getParameter("password");
        String lables=request.getParameter("country");



        try{
       Connection con=DBConnection.getConnection();
       String query="select * from locker_label_tbl where LOCKER_LABEL_USER_ID="+userId+" AND LOCKER_LABEL_NAME='"+lables+"'";
       Statement smt=con.createStatement();
       ResultSet rs=smt.executeQuery(query);
       while(rs.next()){
       lcker_label_id=rs.getInt("LOCKER_LABEL_ID");
       }

        }catch(SQLException e){
       e.printStackTrace();
       }catch(ClassNotFoundException e){
       e.printStackTrace();
       }


        String desc=request.getParameter("desc");
         if(desc==null){
        desc="N/A";
        }
         if(desc.equals("")){
        desc="N/A";
        }
        String extension=request.getParameter("domain");
        out.println("emailid"+emailId+"\n password="+password+"\n desc="+desc);

        model.SetCredentialsInfo obj=new model.SetCredentialsInfo();
         boolean status=obj.insertCredentialsInfo(emailId, password, desc, userId,extension, lcker_label_id);
         out.print(status);
          if(status==true){
        response.sendRedirect("usercredential.jsp?addmsg=add sucessfully&label_id=0&nextpage=1");
        }




    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package servlets;



import connection.DBConnection;
import java.io.*;
import java.sql.*;
import java.util.*;
import java.text.*;
import java.util.regex.*;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.*;

import javax.servlet.*;
import javax.servlet.http.*;
import model.GetFileSize;
import model.GetFileDetails;
import org.apache.catalina.Session;


/**
 *
 * @author gopal
 */
public class FileUpload extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet UploadImage</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet UploadImage at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
            */
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
       // processRequest(request, response);

       try{
           HttpSession session=request.getSession(true);
           String relativePath="";
           String getFileName="";
        String getFileDesc="";
        String domainName="";
        PrintWriter out = response.getWriter();
        boolean isMultipart = ServletFileUpload.isMultipartContent(request);
String name[]= new String[5];
String value[]=new String[5];

if (!isMultipart) {
System.out.println("File Not Uploaded");
} else {

FileItemFactory factory = new DiskFileItemFactory();
System.out.println("File inside1");
ServletFileUpload upload = new ServletFileUpload(factory);
System.out.println("File inside2");
List items = null;

try {
    System.out.println("items: 1111"+upload);
items = (List)upload.parseRequest(request);
System.out.println("items: ");


} catch (Exception e) {
e.printStackTrace();
}
Iterator itr = items.iterator();
int i=0;

while (itr.hasNext()) {
FileItem item = (FileItem) itr.next();
if (item.isFormField()){
name[i]= item.getFieldName();
System.out.println("inside loop"+item.getFieldName());
value[i]= item.getString();
System.out.println("inside loop value"+value[i]);

i++;
}
else {
try {
String itemName = item.getName();
    System.out.println("itemName="+itemName);
Random generator = new Random();
int r = Math.abs(generator.nextInt());

String reg = "[.*]";
String replacingtext = "";
System.out.println("Text before replacing is:-" + itemName);
Pattern pattern = Pattern.compile(reg);
Matcher matcher = pattern.matcher(itemName);
StringBuffer buffer = new StringBuffer();

while (matcher.find()) {
matcher.appendReplacement(buffer, replacingtext);
}

int IndexOf = itemName.indexOf(".");
 domainName = itemName.substring(IndexOf);
System.out.println("domainName: "+domainName);

String fileName = buffer.toString()+"_"+r+domainName;
System.out.println("Final Image==="+fileName);
String path=request.getSession().getServletContext().getRealPath("/")+"uploads\\"+fileName;
relativePath="uploads\\"+fileName;
System.out.println(path);
/* Checking the file content type*/

/*GetFileDetails file=new GetFileDetails();
String fileType=file.getFileType(path);
    System.out.println("fileType= "+fileType);*/


/* CREATING THE FILE IN UPLOADS FOLDER */
                            File savedFile = new File(path);
                            item.write(savedFile);


/*out.println("<html>");
out.println("<body>");
out.println("<table><tr><td>");
out.println("<img src=images/"+fileName+">");
out.println("</td></tr></table>");
out.println("</body>");
out.println("</html>");*/





/*Connection conn = null;
String url = "jdbc:mysql://localhost:3306/";;
String dbName = "test";
String driver = "com.mysql.jdbc.Driver";
String username = "root";
String userPassword = "root";
String strQuery = null;
String strQuery1 = null;
String imgLen="";

try {
System.out.println("itemName::::: "+itemName);
Class.forName(driver).newInstance();
conn = DriverManager.getConnection(url+dbName,username,userPassword);
Statement st = conn.createStatement();
strQuery = "insert into testimage set image='"+fileName+"'";
int rs = st.executeUpdate(strQuery);
System.out.println("Query Executed Successfully++++++++++++++");
out.println("image inserted successfully");
out.println("</body>");
out.println("</html>");
} catch (Exception e) {
System.out.println(e.getMessage());
}*finally {
conn.close();
}*/
} catch (Exception e) {
e.printStackTrace();}
}

}
}
session.setAttribute("filePath", relativePath);
session.setAttribute("fileLabelId", value[0]);
session.setAttribute("fileName", value[1]);
session.setAttribute("fileDesc", value[2]);
session.setAttribute("fileLockerId", value[3]);
session.setAttribute("fileExt", domainName);
System.out.println(name[0]+" and "+value[0]);
    System.out.println(name[1]+" and "+value[1]);
       System.out.println(name[2]+" and "+value[2]);
          System.out.println(name[3]+" and "+value[3]);
response.sendRedirect("SetDocRecord");

       }catch(Exception e){
       e.printStackTrace();
       }
}


    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

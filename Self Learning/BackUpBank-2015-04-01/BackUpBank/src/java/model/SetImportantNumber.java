/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import connection.DBConnection;
import java.sql.*;
import java.util.Date;

/**
 *
 * @author employee
 */
public class SetImportantNumber {

    boolean status = false;
    Connection con = null;

    public boolean insertImportantNumberInfo(String PAN, String password, String desc, int userId, int lcker_lable_id) {

        try {
            
            Date date = new Date();

            Timestamp sqlDate = new Timestamp(date.getTime());

            
String query = "insert into  locker_content_tbl(LOCKER_CONTENT_PATH,LOCKER_CONTENT_DESC ,LOCKER_CONTENT_USER_ID ,LOCKER_CONTENT_LOCKER_ID ,LOCKER_CONTENT_STATUS,RECORD_CREATED_ON ,LOCKER_CONTENT_NAME,LOCKER_CONTENT_LOCKER_LABEL_ID) values(?,?,?,?,?,?,?,?)";
           con = DBConnection.getConnection();
           
            PreparedStatement smt = con.prepareStatement(query);
            smt.setString(1, PAN);
            smt.setString(2, desc);
            smt.setInt(3, userId);
            smt.setInt(4, 6);
            smt.setString(5,"active");
            smt.setTimestamp(6, sqlDate);
            smt.setString(7, password);
            smt.setInt(8, lcker_lable_id);
            
            
            int content_id = 0;
            ResultSet rss = smt.executeQuery("SELECT MAX(LOCKER_CONTENT_ID) FROM locker_content_tbl");
            while (rss.next()) {
                content_id = rss.getInt(1);
            }
            int i = smt.executeUpdate();
            if (i == 0) {
                status = false;
            } else {
                status = true;
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return status;
    }

    public boolean updateImportantNumber(String password, String desc, String PAN, String content_id) {

        try {
            System.out.print("here");
            con = DBConnection.getConnection();
            Statement smt = con.createStatement();
            String query = "update locker_content_tbl set LOCKER_CONTENT_PATH='" + password + "',LOCKER_CONTENT_DESC='" + desc + "',LOCKER_CONTENT_NAME='" + PAN + "' where locker_content_id=" + content_id;
            System.out.println("" + query);
            int i = smt.executeUpdate(query);

            if (i == 0) {
                status = false;
            } else {
                status = true;
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return status;
    }
}

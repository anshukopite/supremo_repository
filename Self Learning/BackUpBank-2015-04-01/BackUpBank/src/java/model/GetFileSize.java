/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package model;
import java.io.File;
import java.sql.*;
import java.text.DecimalFormat;
import connection.DBConnection;


/**
 *
 * @author employee
 */
public class GetFileSize {

    double fileSizeByte1;
    double lockerSize;
    int fileCount;
    ResultSet rs=null;
    Connection con=null;
    public double GetFileSize(String path){


  try{

  File f= new File(path);
  if(f.exists()){
        long fileSizeByte= f.length();
        fileSizeByte1=((double) fileSizeByte)/1024.00;
        DecimalFormat df = new DecimalFormat("#.##");
        fileSizeByte1=Double.parseDouble(df.format(fileSizeByte1));
        System.out.println("File Size: "+fileSizeByte1+" KB("+fileSizeByte+ " Bytes) "+"and File ="+path );
  }
  else{
        fileSizeByte1=0;
        System.out.println("In Else");
  }


  }catch (Exception e)
  {
  e.printStackTrace();
  }
  return fileSizeByte1;
    }



    public int GetFileCount(String path){


  try{

  File f= new File(path);
  if(f.exists()){
        fileCount=fileCount+1;
  }
 
  

  }catch(Exception e){
      e.printStackTrace();
  }
    return fileCount;
    }

  /* Functions to get the locker sizes*/

    public ResultSet GetLockerFileSize(int userId, int lockerId){


  try{
  String sql="select locker_content_path from locker_content_tbl where locker_content_locker_id="+lockerId+" and locker_content_user_id="+userId;
  
  con=DBConnection.getConnection();
  Statement st=con.createStatement();
  rs=st.executeQuery(sql);
  

  }catch (Exception e)
  {
  e.printStackTrace();
  }
  return rs;
    }

  

    
}

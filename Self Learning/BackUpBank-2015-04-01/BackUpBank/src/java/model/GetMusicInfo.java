/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import connection.DBConnection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


/**
 *
 * @author employee
 */
public class GetMusicInfo{

    Connection con=null;
    ResultSet rs=null;
    ResultSet rs2=null;
    Statement st = null;
    Statement st2 = null;
    ArrayList list=null;
   
    public ArrayList GetMusicFiles(int userId){

                try{
                    
                    
                    list= new ArrayList();
                    con=DBConnection.getConnection();
                    st=con.createStatement();
                    rs=st.executeQuery("select * from locker_content_tbl where locker_content_user_id="+userId+" and locker_content_status=1 and locker_content_locker_id=2"); /* locker_content_user_id will be changed to session_id after the login setup*/
                   
                    while(rs.next()){
                       ArrayList list1= new ArrayList();
                        String txtdate = rs.getString("RECORD_CREATED_ON");
                        SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                        Date date = dt.parse(txtdate);
                        SimpleDateFormat dt1 = new SimpleDateFormat("dd-MMM-yyyy");
                       list1.add(rs.getInt("LOCKER_CONTENT_ID"));
                       list1.add(rs.getString("locker_content_name"));
                       list1.add(rs.getString("LOCKER_CONTENT_Extension"));
                       list1.add(rs.getInt("LOCKER_CONTENT_user_id"));
                       list1.add(rs.getInt("LOCKER_CONTENT_locker_id"));
                       list1.add(rs.getInt("LOCKER_CONTENT_status"));
                       list1.add(dt1.format(date));
                       list.add(list1);

                    }

                }catch(Exception e){
                        e.printStackTrace();
                } finally{
                    try{
                        con.close();
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                }
                return list;

    }
    
     public ResultSet PlayFilePath(String content_id){
            
                    try{
                    con=DBConnection.getConnection();
                    st2=con.createStatement();
                    rs2=st2.executeQuery("select LOCKER_CONTENT_PATH from locker_content_tbl where locker_content_id="+content_id);
                    }catch(Exception e){
                                e.printStackTrace();
                    }finally{
                        try{
                            con.close();
                        }catch(Exception e){
                            e.printStackTrace();
                        }
                    }
                    return rs2;

     }



}

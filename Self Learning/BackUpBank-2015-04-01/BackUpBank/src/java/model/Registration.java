/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package model;
import java.security.NoSuchAlgorithmException;
import java.sql.*;
import connection.DBConnection;
import javax.crypto.NoSuchPaddingException;
import javax.mail.Session;
import java.util.Date;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.eclipse.jdt.internal.compiler.ast.ConditionalExpression;
/**
 *
 * @author employee
 */
public class Registration extends HttpServlet{
    boolean status=false;
    int status1=0;
    Connection con=null;
    ResultSet rs=null;
    int user_id=0;
   HttpServletRequest request ;
    HttpSession session = null;
    
    public boolean insertUserInfo(String fname,String lname,String emailId,int country_id,int state_id,int city_id,long contactNumber,int user_status,int user_user_type_id ){
            
        try{
               
                session = request.getSession(true);
             con=DBConnection.getConnection();
             Statement stmt= con.createStatement();

            ResultSet rss= stmt.executeQuery("SELECT MAX(user_id) FROM user_tbl");
            while(rss.next()){
            user_id=rss.getInt(1);
            }
             //String sql="INSERT INTO USER_TBL (USER_ID,USER_FNAME,USER_LNAME,USER_EMAIL,USER_COUNTRY_ID,USER_STATE_ID,USER_CITY_ID,USER_CONTACT_NO,USER_STATUS,USER_USER_TYPE_ID,USER_CREATED_ON) VALUES("+(user_id+1)+",'"+fname+"','"+lname+"','"+emailId+"',"+country_id+","+state_id+","+city_id+","+contactNumber+","+user_status+","+user_user_type_id+","+sqlDate+")";
             PreparedStatement ps = con.prepareStatement("INSERT INTO USER_TBL (USER_FNAME,USER_LNAME,USER_EMAIL,USER_COUNTRY_ID,USER_STATE_ID,USER_CITY_ID,USER_CONTACT_NUMBER,USER_STATUS,USER_USER_TYPE_ID,USER_CREATED_ON) VALUES(?,?,?,?,?,?,?,?,?,?");
             ps.setString(1, fname);
             ps.setString(2, lname);
             ps.setString(3, emailId);
             ps.setInt(4, country_id);
             ps.setInt(5, state_id);
             ps.setInt(6, city_id);
             ps.setLong(7, contactNumber);
             ps.setInt(8, status1);
             ps.setInt(9, user_user_type_id);
             ps.setTimestamp(10, null);
             
              int i=ps.executeUpdate();
              if(i!=0){
                  status=true;
                  session.setAttribute("uid", user_id);                  
              }
              System.out.println("After User Info "+status);
          }catch(ClassNotFoundException e){
            System.out.println("class not found ex"+e);
          }catch(SQLException e){
           System.out.println("sql ex"+e);
          }
        return status;
    }
    public boolean  insertCredentialsDetails(String userName,String password,int uid) throws NoSuchAlgorithmException, NoSuchPaddingException, Exception{
        int user_cred_id=0;
           try{
                System.out.println("User Id Check #2 "+uid);
          con=DBConnection.getConnection();
      
             String user_cred_status="active";
             
                PasswordEncoder pw = PasswordEncoder.getInstance();
             String passw = pw.encode(password, userName);
                System.out.println("ENC Pass "+passw);
            // System.out.println("Encrypt "+encryptionBytes);

             java.util.Date date = new Date();
             Timestamp sqldate = new Timestamp(date.getTime());
           /* ResultSet rss= stmt.executeQuery("SELECT MAX(user_cred_id) FROM user_cred_tbl");
            while(rss.next()){
                user_cred_id=rss.getInt(1);
            }*/
                
                String sql="insert into user_cred_tbl (user_cred_uname,user_cred_password,user_cred_date,user_cred_user_id,user_cred_status,record_created_on) values (?,?,?,?,?,?)";
                PreparedStatement pss = con.prepareStatement("insert into user_cred_tbl (user_cred_uname,user_cred_password,user_cred_date,user_cred_user_id,user_cred_status,record_created_on) values (?,?,?,?,?,?)");
                pss.setString(1, userName);
                pss.setString(2, passw);
                pss.setTimestamp(3, null);
                pss.setInt(4, uid);
                pss.setString(5, user_cred_status);
                pss.setTimestamp(6, sqldate);
                
                int j=pss.executeUpdate();
                if(j>0){
                    status=true;
                   
                }
                System.out.println("After credentials"+status);
                //if(status){
                        //String dcr = decrypt(passw.getBytes());
                        //session.setAttribute("decrypt", dcr);
                        //System.out.println("Decrypt "+dcr);
               // }
           }catch(ClassNotFoundException e){
            
            e.printStackTrace();
          }catch(SQLException e){
           e.printStackTrace();
          }
        return status;

    }


public boolean  insertLabelDetails(int uid) throws NoSuchAlgorithmException, NoSuchPaddingException, Exception{
        
           try{
          con=DBConnection.getConnection();
              int labelid = 0;
             int docLabel ;
             int musLabel ;
             int photLabel  ;
             int vidLabel ;
             int emailLabel ;
             int credLabel ;
             int debLabel ;
             int othercred;
             int atmLabel ;
             int netBankLabel ;
             int panLabel;
             int dlLabel;
             int insPolLabel;
             int uidLabel;
             int phnoLabel;
             int idcardLabel;
             int otherimp;
             

             Statement st = con.createStatement();
             Statement stmt= con.createStatement();
Statement smt=con.createStatement();
            
                String sqlDoc="insert into locker_label_tbl (locker_label_name,locker_label_locker_id,locker_label_user_id,locker_label_status) values('Docs',1,"+uid+",'active')";
                docLabel=smt.executeUpdate(sqlDoc);
             ResultSet rss= stmt.executeQuery("SELECT MAX(locker_label_id)+1 FROM locker_label_tbl ");
            while(rss.next()){
                    labelid = Integer.parseInt(rss.getString(1));

            }   
                
                String sqlMus="insert into locker_label_tbl values("+(labelid)+",'Music',2,"+uid+",'active')";
                String sqlPho="insert into locker_label_tbl values("+(labelid+1)+",'Photos',3,"+uid+",'active')";
                String sqlVid="insert into locker_label_tbl values("+(labelid+2)+",'Videos',4,"+uid+",'active')";
                String sqlEmail="insert into locker_label_tbl values("+(labelid+3)+",'Email',5,"+uid+",'active')";
                String sqlNetBanking="insert into locker_label_tbl values("+(labelid+4)+",'Net Banking',5,"+uid+",'active')";
                String sqlAtm="insert into locker_label_tbl values("+(labelid+5)+",'ATM Card',5,"+uid+",'active')";
                String sqlCred="insert into locker_label_tbl values("+(labelid+6)+",'Credit Card',5,"+uid+",'active')";
                String sqlDeb="insert into locker_label_tbl values("+(labelid+7)+",'Debit Card',5,"+uid+",'active')";
                 String sqlCredOther="insert into locker_label_tbl values("+(labelid+8)+",'Other Credentials',5,"+uid+",'active')";

                String sqlCont="insert into locker_label_tbl values("+(labelid+10)+",'Contact Number',6,"+uid+",1)";
                String sqlPan="insert into locker_label_tbl values("+(labelid+11)+",'PAN Card',6,"+uid+",1)";
                String sqlDL="insert into locker_label_tbl values("+(labelid+12)+",'Driving License',6,"+uid+",1)";
                String sqlInsure="insert into locker_label_tbl values("+(labelid+13)+",'Insurance Policy',6,"+uid+",1)";
                String sqlUid="insert into locker_label_tbl values("+(labelid+14)+",'Unique Id',6,"+uid+",1)";
                String sqlIdCard="insert into locker_label_tbl values("+(labelid+15)+",'Identity Card',6,"+uid+",1)";
                String sqlImpOther="insert into locker_label_tbl values("+(labelid+16)+",'Other Important Numbers',6,"+uid+",1)";

                

                
                musLabel=smt.executeUpdate(sqlMus);
                photLabel=smt.executeUpdate(sqlPho);
                vidLabel=smt.executeUpdate(sqlVid);
                emailLabel=smt.executeUpdate(sqlEmail);
                netBankLabel=smt.executeUpdate(sqlNetBanking);
                atmLabel = smt.executeUpdate(sqlAtm);
                credLabel = smt.executeUpdate(sqlCred);
                debLabel = smt.executeUpdate(sqlDeb);
                othercred = smt.executeUpdate(sqlCredOther);
                phnoLabel = smt.executeUpdate(sqlCont);
                panLabel = smt.executeUpdate(sqlPan);
                dlLabel = smt.executeUpdate(sqlDL);
                insPolLabel = smt.executeUpdate(sqlInsure);
                uidLabel = smt.executeUpdate(sqlUid);
                idcardLabel = smt.executeUpdate(sqlIdCard);
                otherimp = smt.executeUpdate(sqlImpOther);

                if((docLabel>0)&&(musLabel>0)&&(photLabel>0)&&(vidLabel>0)&&(emailLabel>0)&&(atmLabel>0)&&(credLabel>0)&&(debLabel>0)&&(panLabel>0)&&(netBankLabel>0)&&(phnoLabel>0)&&(panLabel>0)&&(dlLabel>0)&&(insPolLabel>0)&&(uidLabel>0)&&(idcardLabel>0)&&(othercred>0)&&(otherimp>0))
                    status=true;
                System.out.println("After credentials"+status);
                //if(status){
                        //String dcr = decrypt(passw.getBytes());
                        //session.setAttribute("decrypt", dcr);
                        //System.out.println("Decrypt "+dcr);
               // }
           }catch(ClassNotFoundException e){
           e.printStackTrace();
          }catch(SQLException e){
           e.printStackTrace();
          }
        return status;

    }

}

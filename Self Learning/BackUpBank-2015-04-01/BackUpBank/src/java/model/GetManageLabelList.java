/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import connection.DBConnection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author employee
 */
public class GetManageLabelList {

    Connection con= null;
    ResultSet rs = null;
    ArrayList row = new ArrayList();

    public ResultSet getDocLabels(int userId){
        try{
            con=DBConnection.getConnection();
            Statement st = con.createStatement();
            rs = st.executeQuery("select * from locker_label_tbl  where locker_label_user_id ="+userId+" and locker_label_locker_id=1 and locker_label_status=1 and locker_label_name NOT LIKE 'Docs' order by locker_label_id");


        }catch(Exception e){
            e.printStackTrace();

        }
        return rs ;
    }
    public ResultSet getMusicLabels(int userId){
        try{
            con=DBConnection.getConnection();
            Statement st = con.createStatement();
            rs = st.executeQuery("select * from locker_label_tbl  where locker_label_user_id ="+userId+" and locker_label_status=1 and locker_label_locker_id=2  and locker_label_name NOT LIKE 'Music' order by locker_label_id");


        }catch(Exception e){
            e.printStackTrace();

        }
        return rs ;
    }
     public ResultSet getPhotoLabels(int userId){
        try{
            con=DBConnection.getConnection();
            Statement st = con.createStatement();
            rs = st.executeQuery("select locker_label_id,locker_label_name from locker_label_tbl  where locker_label_user_id ="+userId+" and locker_label_status=1 and locker_label_locker_id=3 and locker_label_name NOT LIKE 'Photos' order by locker_label_id");


        }catch(Exception e){
            e.printStackTrace();

        }
        return rs ;
    }
     public ResultSet getVideoLabels(int userId){
        try{
            con=DBConnection.getConnection();
            Statement st = con.createStatement();
            rs = st.executeQuery("select * from locker_label_tbl  where locker_label_user_id ="+userId+" and locker_label_status=1 and locker_label_locker_id=4  and locker_label_name NOT LIKE 'Videos' order by locker_label_id");


        }catch(Exception e){
            e.printStackTrace();

        }
        return rs ;
    }
     public ResultSet getCredentialsLabels(int userId){
        try{
            con=DBConnection.getConnection();
            Statement st = con.createStatement();
            rs = st.executeQuery("select * from locker_label_tbl  where locker_label_user_id ="+userId+" and locker_label_locker_id=5  and locker_label_status=1 and locker_label_name NOT LIKE 'Email' and locker_label_name NOT LIKE 'Net Banking' and locker_label_name NOT LIKE 'ATM Card' and locker_label_name NOT LIKE 'Credit Card' and locker_label_name NOT LIKE 'Debit Card'  and locker_label_name NOT LIKE 'Other Credentials' order by locker_label_id");


        }catch(Exception e){
            e.printStackTrace();

        }
        return rs ;
    }
      public ResultSet getNumberLabels(int userId){
        try{
            con=DBConnection.getConnection();
            Statement st = con.createStatement();
            rs = st.executeQuery("select locker_label_id,locker_label_name from locker_label_tbl  where locker_label_user_id ="+userId+" and locker_label_status=1 and locker_label_locker_id=6  and locker_label_name NOT LIKE 'Contact Number' and locker_label_name NOT LIKE 'PAN Card' and locker_label_name NOT LIKE 'Driving License' and locker_label_name NOT LIKE 'Insurance Policy' and locker_label_name NOT LIKE 'Unique Id' and locker_label_name NOT LIKE 'Identity Card' and locker_label_name NOT LIKE 'Other Important Numbers' order by locker_label_id");


        }catch(Exception e){
            e.printStackTrace();

        }
        return rs ;
    }

}

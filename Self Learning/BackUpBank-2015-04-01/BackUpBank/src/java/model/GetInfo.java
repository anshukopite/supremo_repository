/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package model;
import java.sql.*;
import connection.DBConnection ;

/**
 *
 * @author employee
 */
public class GetInfo {
    ResultSet rs=null;
    Connection con=null;
    public ResultSet getCountry(){
        try{
      con=DBConnection.getConnection();
      String sql="select * from  country_tbl";
      Statement smt=con.createStatement();
      rs=smt.executeQuery(sql);
        }catch(ClassNotFoundException e){
        e.printStackTrace();
        }catch(SQLException e){
       e.printStackTrace();
        } 
        System.out.println(rs);
        return rs;
    }

    public ResultSet getState(int id){

         try{
      con=DBConnection.getConnection();
      String sql="select * from  state_tbl where STATE_COUNTRY_ID ="+id;
      Statement smt=con.createStatement();
      rs=smt.executeQuery(sql);
        }catch(ClassNotFoundException e){
        e.printStackTrace();
        }catch(SQLException e){
        e.printStackTrace();
        }
        return rs;
    }
      public ResultSet getCity(int id){

         try{
      con=DBConnection.getConnection();
      String sql="select * from  city_tbl where city_state_id ="+id;
      Statement smt=con.createStatement();
      rs=smt.executeQuery(sql);
        }catch(ClassNotFoundException e){
        e.printStackTrace();
        }catch(SQLException e){
        e.printStackTrace();
        }
        return rs;
    }
      public ResultSet getCountryCode(int id){

         try{
      con=DBConnection.getConnection();
      String sql="select s.`state_country_id` from `city_tbl` c,`state_tbl` s where c.`city_state_id`=s.`state_id` and c.`city_id`="+id;
      Statement smt=con.createStatement();
      rs=smt.executeQuery(sql);
        }catch(ClassNotFoundException e){
        e.printStackTrace();
        }catch(SQLException e){
        e.printStackTrace();
        }
        return rs;
    }


}

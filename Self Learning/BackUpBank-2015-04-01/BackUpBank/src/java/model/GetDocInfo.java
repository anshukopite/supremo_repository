/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import connection.DBConnection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author employee
 */
public class GetDocInfo {

    Connection con=null;
    ResultSet rs=null;
    Statement st = null;
    ArrayList list=null;
    
    public ArrayList GetDocFiles(int user_id, String labelId ,int locker_id){

                try{

                     String query="";
         if(labelId==null){

         query="select * from locker_content_tbl where locker_content_user_id="+user_id+" and locker_content_status=1 and locker_content_locker_id="+locker_id;
          System.out.println("in null condition");
         }
             else if(labelId.equals("0")){
         query="select * from locker_content_tbl where locker_content_user_id="+user_id+" and locker_content_status=1 and locker_content_locker_id="+locker_id;
           System.out.println("in else if 0 condition");
        }
        else{
         query="select * from locker_content_tbl where locker_content_user_id="+user_id+" and locker_content_status=1 and locker_content_locker_id="+locker_id+" and locker_content_locker_label_id='"+labelId+"'";
         System.out.println("in else condition");
        }
                    list= new ArrayList();
                    con=DBConnection.getConnection();
                    st=con.createStatement();
                    rs=st.executeQuery(query);
                    while(rs.next()){
                       ArrayList list1= new ArrayList();
                        String txtdate = rs.getString("RECORD_CREATED_ON");
                        SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                        Date date = dt.parse(txtdate);
                        SimpleDateFormat dt1 = new SimpleDateFormat("dd-MMM-yyyy");
                       list1.add(rs.getInt("LOCKER_CONTENT_ID"));
                       list1.add(rs.getString("locker_content_name"));
                       list1.add(rs.getString("LOCKER_CONTENT_DESC"));
                       list1.add(rs.getInt("LOCKER_CONTENT_user_id"));
                       list1.add(rs.getInt("LOCKER_CONTENT_locker_id"));
                       list1.add(rs.getInt("LOCKER_CONTENT_status"));
                       list1.add(dt1.format(date));
                       list.add(list1);

                    }
                   

                }catch(Exception e){
                        e.printStackTrace();
                }
                return list;

    }


}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import java.io.File;
import javax.swing.JFileChooser;

/**
 *
 * @author employee
 */
public class GetFileDetails {

    public String getFileType(String path){

        JFileChooser chooser = new JFileChooser();
        File file = new File(path);
        String fileTypeName = chooser.getTypeDescription(file);
        System.out.println("File Type= " + fileTypeName);
        return fileTypeName;

    }
}

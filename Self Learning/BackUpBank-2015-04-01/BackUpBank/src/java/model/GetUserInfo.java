/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import connection.DBConnection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author employee
 */
public class GetUserInfo {

    Connection con=null;
    ResultSet rs=null;
    Statement st = null;
    ArrayList list=null;
    int countd = 0 ;
    int countn = 0 ;
    public ArrayList getUserDetail(int user_id){

                try{

                     String query="Select * from user_tbl u,country_tbl c,state_tbl s,city_tbl ct where user_id ="+user_id+" and u.user_country_id=c.country_id and u.user_state_id = s.state_id and u.user_city_id = ct.city_id";

                    list= new ArrayList();
                    con=DBConnection.getConnection();
                    st=con.createStatement();
                    rs=st.executeQuery(query);
                    while(rs.next()){
                       ArrayList list1= new ArrayList();
                        String txtdate = rs.getString(11);
                        SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                        Date date = dt.parse(txtdate);
                        SimpleDateFormat dt1 = new SimpleDateFormat("dd-MMM-yyyy");

                       list1.add(rs.getString(2));
                       list1.add(rs.getString(3));
                       list1.add(rs.getString(4));
                       list1.add(rs.getLong(8));
                       list1.add(rs.getString("CITY_NAME"));
                       list1.add(rs.getString("STATE_NAMe"));
                       list1.add(rs.getString("COUNTRY_NAME"));
                       list1.add(dt1.format(date));
                       list1.add(rs.getInt(5));
                       list1.add(rs.getInt(6));
                       list1.add(rs.getInt(7));

                       list.add(list1);

                    }


                }catch(Exception e){
                        e.printStackTrace();
                }
                return list;

    }
    public ArrayList getAdminDetail(int admin_id){

                try{

                     String query="Select * from admin_tbl where admin_id ="+admin_id;

                    list= new ArrayList();
                    con=DBConnection.getConnection();
                    st=con.createStatement();
                    rs=st.executeQuery(query);
                    while(rs.next()){
                       ArrayList list1= new ArrayList();


                       list1.add(rs.getString(1));
                       list1.add(rs.getString(2));
                       list1.add(rs.getString(3));
                       list1.add(rs.getString(4));
                       list1.add(rs.getString(5));


                       list.add(list1);

                    }


                }catch(Exception e){
                        e.printStackTrace();
                }
                return list;

    }
 public ArrayList getUserMessageByName(int admin_id){

                try{

                     String query;
                    // if((countn%2)==0){
                      query="select u.`user_fname`,a.`admin_inbox_ques`,a.`admin_inbox_subject`,a.`admin_inbox_rec_date`,a.`admin_inbox_user_id`,a.`admin_inbox_id` from user_tbl u,admin_inbox_tbl a,`user_cred_tbl` uct where a.`admin_inbox_user_id`=uct.`user_cred_user_id` and uct.`user_cred_user_id`=u.`user_id` order by u.`user_fname` DESC";
                     // countn++;
                    // }else{
                        //query="select user_fname,admin_inbox_sub,admin_inbox_sent_date,admin_inbox_msg,admin_inbox_user_id from user_tbl,admin_inbox_tbl where admin_inbox_user_id = user_id order by user_fname desc";
                    //  }
                    list= new ArrayList();
                    con=DBConnection.getConnection();
                    st=con.createStatement();
                    rs=st.executeQuery(query);
                    while(rs.next()){
                       ArrayList list1= new ArrayList();
                       Date sqlDate =  rs.getTimestamp("ADMIN_INBOX_REC_DATE");
                       SimpleDateFormat dt1 = new SimpleDateFormat("dd-MMM-yyyy");
                       System.out.println("In it");
                       list1.add(rs.getString("USER_FNAME"));
                       list1.add(rs.getString("ADMIN_INBOX_SUBJECT"));
                       list1.add(dt1.format(sqlDate));
                       list1.add(rs.getString("ADMIN_INBOX_QUES"));
                       list1.add(rs.getInt("ADMIN_INBOX_USER_ID"));
                       list1.add(rs.getInt("ADMIN_INBOX_ID"));
                       list.add(list1);

                    }


                }catch(Exception e){
                        e.printStackTrace();
                }
                return list;

    }
public ArrayList getUserMessageByDate(int admin_id){

                try{

                     String query;

                     query="select user_fname,admin_inbox_subject,admin_inbox_rec_date,admin_inbox_ques,admin_inbox_user_id,admin_inbox_id from user_tbl,admin_inbox_tbl where admin_inbox_user_id = user_id order by admin_inbox_rec_date desc";

                     //query="select user_fname,admin_inbox_sub,admin_inbox_sent_date,admin_inbox_msg,admin_inbox_user_id from user_tbl,admin_inbox_tbl where admin_inbox_user_id = user_id order by admin_inbox_sent_date desc";

                    list= new ArrayList();
                    con=DBConnection.getConnection();
                    st=con.createStatement();
                    rs=st.executeQuery(query);
                    while(rs.next()){
                       ArrayList list1= new ArrayList();
                       Date sqlDate = rs.getTimestamp("ADMIN_INBOX_REC_DATE");
                        SimpleDateFormat dt1 = new SimpleDateFormat("dd-MMM-yyyy");
                            
                       list1.add(rs.getString("USER_FNAME"));
                       list1.add(rs.getString("ADMIN_INBOX_SUBJECT"));
                       list1.add(dt1.format(sqlDate));
                       list1.add(rs.getString("ADMIN_INBOX_QUES"));
                       list1.add(rs.getString("ADMIN_INBOX_USER_ID"));
                       list1.add(rs.getString("admin_inbox_id"));
                       list.add(list1);

                    }


                }catch(Exception e){
                        e.printStackTrace();
                }
                return list;

    }

 public ArrayList getUserDetailsByName(int user_id){

                try{

                     String query="select user_id,user_fname,user_created_on,user_status from user_tbl order by user_fname";

                    list= new ArrayList();
                    con=DBConnection.getConnection();
                    st=con.createStatement();
                    rs=st.executeQuery(query);
                    while(rs.next()){
                       ArrayList list1= new ArrayList();
                        Date txtdate = rs.getTimestamp("USER_CREATED_ON");
                        SimpleDateFormat dt1 = new SimpleDateFormat("dd-MMM-yyyy");

                       list1.add(rs.getInt("USER_ID"));
                       list1.add(rs.getString("USER_FNAME"));
                       list1.add(dt1.format(txtdate));
                       list1.add(rs.getInt("USER_STATUS"));
                       list.add(list1);

                    }


                }catch(Exception e){
                        e.printStackTrace();
                }
                return list;

    }

 public ArrayList getUserDetailsByDate(int user_id){

                try{

                     String query="select user_id,user_fname,user_created_on,user_status from user_tbl order by user_created_on";

                    list= new ArrayList();
                    con=DBConnection.getConnection();
                    st=con.createStatement();
                    rs=st.executeQuery(query);
                    while(rs.next()){
                       ArrayList list1= new ArrayList();
                        Date txtdate = rs.getTimestamp("USER_CREATED_ON");
                        SimpleDateFormat dt1 = new SimpleDateFormat("dd-MMM-yyyy");

                       list1.add(rs.getInt("USER_ID"));
                       list1.add(rs.getString("USER_FNAME"));
                       list1.add(dt1.format(txtdate));
                       list1.add(rs.getInt("USER_STATUS"));
                       list.add(list1);
                    }


                }catch(Exception e){
                        e.printStackTrace();
                }
                return list;

    }

 public ArrayList getReplyByName(int admin_id){

                try{

                     String query;
                    // if((countn%2)==0){
                      query="select user_fname,admin_inbox_subject,user_inbox_answer,admin_inbox_rec_date,admin_inbox_ques,admin_inbox_id,user_inbox_id,user_id from user_tbl,admin_inbox_tbl,user_inbox_tbl where admin_inbox_user_id = user_id order by user_fname";
                     // countn++;
                    // }else{
                        //query="select user_fname,admin_inbox_sub,admin_inbox_sent_date,admin_inbox_msg,admin_inbox_user_id from user_tbl,admin_inbox_tbl where admin_inbox_user_id = user_id order by user_fname desc";
                    //  }
                    list= new ArrayList();
                    con=DBConnection.getConnection();
                    st=con.createStatement();
                    rs=st.executeQuery(query);
                    while(rs.next()){
                       ArrayList list1= new ArrayList();
                        Date txtdate = rs.getTimestamp("ADMIN_INBOX_REC_DATE");
                        SimpleDateFormat dt1 = new SimpleDateFormat("dd-MMM-yyyy");

                       list1.add(rs.getString("USER_FNAME"));
                       list1.add(rs.getString("ADMIN_INBOX_SUBJECT"));
                       list1.add(rs.getString("USER_INBOX_ANSWER"));
                       list1.add(dt1.format(txtdate));
                       list1.add(rs.getString("admin_inbox_ques"));
                       list1.add(rs.getInt("admin_inbox_id"));
                       list1.add(rs.getInt("user_inbox_id"));
                       list1.add(rs.getInt("user_id"));

                       list.add(list1);

                    }


                }catch(Exception e){
                        e.printStackTrace();
                }
                return list;

    }


 public ArrayList getReplyByDate(int admin_id){

                try{

                     String query;
                    // if((countn%2)==0){
                      query="select user_fname,admin_inbox_subject,user_inbox_answer,admin_inbox_rec_date,admin_inbox_ques,admin_inbox_id,user_inbox_id,user_id from user_tbl,admin_inbox_tbl,user_inbox_tbl where admin_inbox_user_id = user_id order by admin_inbox_recieve_date";
                     // countn++;
                    // }else{
                        //query="select user_fname,admin_inbox_sub,admin_inbox_sent_date,admin_inbox_msg,admin_inbox_user_id from user_tbl,admin_inbox_tbl where admin_inbox_user_id = user_id order by user_fname desc";
                    //  }
                    list= new ArrayList();
                    con=DBConnection.getConnection();
                    st=con.createStatement();
                    rs=st.executeQuery(query);
                    while(rs.next()){
                       ArrayList list1= new ArrayList();
                        Date txtdate = rs.getTimestamp("ADMIN_INBOX_rec_DATE");
                        SimpleDateFormat dt1 = new SimpleDateFormat("dd-MMM-yyyy");

                       list1.add(rs.getString("USER_FNAME"));
                       list1.add(rs.getString("ADMIN_INBOX_SUBJECT"));
                       list1.add(rs.getString("USER_INBOX_ANSWER"));
                       list1.add(dt1.format(txtdate));
                       list1.add(rs.getString("admin_inbox_ques"));
                       list1.add(rs.getInt("admin_inbox_id"));
                       list1.add(rs.getInt("user_inbox_id"));
                       list1.add(rs.getInt("user_id"));

                       list.add(list1);

                    }


                }catch(Exception e){
                        e.printStackTrace();
                }
                return list;

    }

 public ArrayList getLogByName(int admin_id){

                try{
                    String brow = "Browser closed without loging off";
                     String query;
                    // if((countn%2)==0){
                      query="select * from user_log_tbl,user_tbl,user_cred_tbl where user_id=user_cred_user_id and user_cred_user_id = user_log_user_id order by user_fname";
                     // countn++;
                    // }else{
                        //query="select user_fname,admin_inbox_sub,admin_inbox_sent_date,admin_inbox_msg,admin_inbox_user_id from user_tbl,admin_inbox_tbl where admin_inbox_user_id = user_id order by user_fname desc";
                    //  }
                    list= new ArrayList();
                    con=DBConnection.getConnection();
                    st=con.createStatement();
                    rs=st.executeQuery(query);
                    while(rs.next()){
                       ArrayList list1= new ArrayList();
                        Date date = rs.getTimestamp("user_log_in");
                        SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MMM-dd hh:mm:ss");
SimpleDateFormat dt2 = new SimpleDateFormat("yyyy-MMM-dd hh:mm:ss");
                         Date  date1 = null;
                         String txtdate1;
                         if(rs.getString("user_log_out")==null){
                              txtdate1 = "Browser Directly Closed";
                               list1.add(rs.getString("USER_Log_id"));
                       list1.add(dt1.format(date));
                       list1.add(txtdate1);
                       list1.add(rs.getString("user_fname"));
                       list1.add(rs.getString("user_ip_address"));

                         }
                         else{
                              txtdate1 = rs.getString("user_log_out");
                              date1 = dt2.parse(txtdate1);
                              SimpleDateFormat dt3 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                              list1.add(rs.getString("USER_Log_id"));
                                list1.add(dt1.format(date));
                                    list1.add(dt3.format(date1));
                                list1.add(rs.getString("user_fname"));
                                list1.add(rs.getString("user_ip_address"));
                         }

                       list.add(list1);

                    }


                }
                catch(Exception e){
                        e.printStackTrace();
                }
                return list;

    }
  public ArrayList getLogByDate(int admin_id){

                try{
                    String brow = "Browser closed without loging off";
                     String query;
                    // if((countn%2)==0){
                      query="select * from user_log_tbl,user_tbl,user_cred_tbl where user_id=user_cred_user_id and user_cred_user_id = user_log_user_id order by user_log_in DESC";
                     // countn++;
                    // }else{
                        //query="select user_fname,admin_inbox_sub,admin_inbox_sent_date,admin_inbox_msg,admin_inbox_user_id from user_tbl,admin_inbox_tbl where admin_inbox_user_id = user_id order by user_fname desc";
                    //  }
                    list= new ArrayList();
                    con=DBConnection.getConnection();
                    st=con.createStatement();
                    rs=st.executeQuery(query);
                    while(rs.next()){
                       ArrayList list1= new ArrayList();
                        Date date = rs.getTimestamp("user_log_in");
                        SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MMM-dd hh:mm:ss");
                            SimpleDateFormat dt2 = new SimpleDateFormat("yyyy-MMM-dd hh:mm:ss");
                         Date  date1 = null;
                         String txtdate1;
                         if(rs.getString("user_log_out")==null){
                              txtdate1 = "Browser Directly Closed";
                               list1.add(rs.getString("USER_Log_id"));
                       list1.add(dt1.format(date));
                       list1.add(txtdate1);
                       list1.add(rs.getString("user_fname"));
                       list1.add(rs.getString("user_ip_address"));

                         }
                         else{
                              txtdate1 = rs.getString("user_log_out");
                              date1 = dt2.parse(txtdate1);
                              SimpleDateFormat dt3 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                              list1.add(rs.getString("USER_Log_id"));
                                list1.add(dt1.format(date));
                                    list1.add(dt3.format(date1));
                                list1.add(rs.getString("user_fname"));
                                list1.add(rs.getString("user_ip_address"));
                         }





                       list.add(list1);

                    }


                }catch(Exception e){
                        e.printStackTrace();
                }
                return list;

    }

  public ArrayList getFeedbackByName(int admin_id){

                try{

                     String query;
                    // if((countn%2)==0){
                      query="select feedback_id,user_fname,feedback_msg,feedback_rcv_date,feedback_subject from feedback_tbl,user_tbl where feedback_user_id = user_id order by user_fname";
                     // countn++;
                    // }else{
                        //query="select user_fname,admin_inbox_sub,admin_inbox_sent_date,admin_inbox_msg,admin_inbox_user_id from user_tbl,admin_inbox_tbl where admin_inbox_user_id = user_id order by user_fname desc";
                    //  }
                    list= new ArrayList();
                    con=DBConnection.getConnection();
                    st=con.createStatement();
                    rs=st.executeQuery(query);
                    while(rs.next()){
                       ArrayList list1= new ArrayList();
                        Date date = rs.getTimestamp("feedback_rcv_date");
                        SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MMM-dd hh:mm:ss");

                       /* String txtdate1 = rs.getString("user_log_out");
                        SimpleDateFormat dt2 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                        Date date1 = dt2.parse(txtdate1);
                        SimpleDateFormat dt3 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");*/


                       list1.add(rs.getString("Feedback_id"));
                       list1.add(rs.getString("user_fname"));
                       list1.add(rs.getString("feedback_msg"));
                       list1.add(dt1.format(date));
                       list1.add(rs.getString("feedback_subject"));

                       list.add(list1);

                    }


                }catch(Exception e){
                        e.printStackTrace();
                }
                return list;

    }
public ArrayList getFeedbackByDate(int admin_id){

                try{

                     String query;
                    // if((countn%2)==0){
                      query="select feedback_id,user_fname,feedback_msg,feedback_rcv_date,feedback_subject from feedback_tbl,user_tbl where feedback_user_id = user_id order by feedback_rcv_date";
                     // countn++;
                    // }else{
                        //query="select user_fname,admin_inbox_sub,admin_inbox_sent_date,admin_inbox_msg,admin_inbox_user_id from user_tbl,admin_inbox_tbl where admin_inbox_user_id = user_id order by user_fname desc";
                    //  }
                    list= new ArrayList();
                    con=DBConnection.getConnection();
                    st=con.createStatement();
                    rs=st.executeQuery(query);
                    while(rs.next()){
                       ArrayList list1= new ArrayList();
                        Date date = rs.getTimestamp("feedback_rcv_date");
                        
                        SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

                       /* String txtdate1 = rs.getString("user_log_out");
                        SimpleDateFormat dt2 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                        Date date1 = dt2.parse(txtdate1);
                        SimpleDateFormat dt3 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");*/


                       list1.add(rs.getString("Feedback_id"));
                       list1.add(rs.getString("user_fname"));
                       list1.add(rs.getString("feedback_msg"));
                       list1.add(dt1.format(date));
                       list1.add(rs.getString("feedback_subject"));

                       list.add(list1);

                    }


                }catch(Exception e){
                        e.printStackTrace();
                }
                return list;

    }

public ArrayList getAdminMessageByName(int user_id){

                try{

                     String query;
                    // if((countn%2)==0){
                      query="select u.user_inbox_id,u.user_inbox_user_id,u.user_inbox_answer,u.user_inbox_rcv_date,a.admin_inbox_subject,a.admin_inbox_ques from user_inbox_tbl u,user_cred_tbl uu,admin_inbox_tbl a where u.user_inbox_user_id = uu.user_cred_user_id and uu.user_cred_user_id="+user_id+" and a.admin_inbox_id = u.user_inbox_admin_inbox_id" ;
                     // countn++;
                    // }else{
                        //query="select user_fname,admin_inbox_sub,admin_inbox_sent_date,admin_inbox_msg,admin_inbox_user_id from user_tbl,admin_inbox_tbl where admin_inbox_user_id = user_id order by user_fname desc";
                    //  }
                    list= new ArrayList();
                    con=DBConnection.getConnection();
                    st=con.createStatement();
                    rs=st.executeQuery(query);
                    while(rs.next()){
                       ArrayList list1= new ArrayList();
                       Date txtdate = rs.getTimestamp("USER_INBOX_RCV_DATE");
                       
                       SimpleDateFormat dt1 = new SimpleDateFormat("dd-MMM-yyyy");

                       list1.add(rs.getString("USER_INBOX_ID"));
                       list1.add(rs.getString("USER_INBOX_USER_ID"));
                       list1.add(rs.getString("USER_INBOX_ANSWER"));
                       list1.add(dt1.format(txtdate));
                       list1.add(rs.getString("ADMIN_INBOX_SUBJECT"));
                       list1.add(rs.getString("ADMIN_INBOX_QUES"));
                       list.add(list1);

                    }


                }catch(Exception e){
                        e.printStackTrace();
                }
                return list;

    }
public ArrayList getAdminMessageByDate(int user_id){

                try{

                     String query;

                     query="select u.user_inbox_id,u.user_inbox_user_id,u.user_inbox_answer,u.user_inbox_rcv_date,a.admin_inbox_subject,a.admin_inbox_ques from user_inbox_tbl u,user_cred_tbl uu,admin_inbox_tbl a where u.user_inbox_user_id = uu.user_cred_user_id and uu.user_cred_user_id="+user_id+" and a.admin_inbox_id = u.user_inbox_admin_inbox_id order by user_inbox_rcv_date desc";

                     //query="select user_fname,admin_inbox_sub,admin_inbox_sent_date,admin_inbox_msg,admin_inbox_user_id from user_tbl,admin_inbox_tbl where admin_inbox_user_id = user_id order by admin_inbox_sent_date desc";

                    list= new ArrayList();
                    con=DBConnection.getConnection();
                    st=con.createStatement();
                    rs=st.executeQuery(query);
                    while(rs.next()){
                       ArrayList list1= new ArrayList();
                       Date txtdate = rs.getTimestamp("USER_INBOX_RCV_DATE");
                       
                       SimpleDateFormat dt1 = new SimpleDateFormat("dd-MMM-yyyy");

                       list1.add(rs.getString("USER_INBOX_ID"));
                       list1.add(rs.getString("USER_INBOX_USER_ID"));
                       list1.add(rs.getString("USER_INBOX_ANSWER"));
                       list1.add(dt1.format(txtdate));
                       list1.add(rs.getString("ADMIN_INBOX_SUBJECT"));
                       list1.add(rs.getString("ADMIN_INBOX_QUES"));
                       list.add(list1);
                   }
                }catch(Exception e){
                        e.printStackTrace();
                }
                return list;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import connection.DBConnection;
import java.sql.*;

/**
 *
 * @author employee
 */
public class GetCredentialsInfo {

    ArrayList list = null;

    public ArrayList getAllCredInfo(int user_id) {
        try {
            Connection con = DBConnection.getConnection();
            Statement smt = con.createStatement();
            String query = "select * from locker_content_tbl where locker_content_user_id=" + user_id + " and locker_content_status=1 and locker_content_locker_id=5";
            ResultSet rs = smt.executeQuery(query);
            list = new ArrayList();
            while (rs.next()) {
                ArrayList li = new ArrayList();
                li.add(rs.getString("LOCKER_CONTENT_NAME"));
                li.add(rs.getString("LOCKER_CONTENT_PATH"));
                li.add(rs.getString("LOCKER_CONTENT_DESC"));
                li.add(rs.getInt("LOCKER_CONTENT_USER_ID"));
                li.add(rs.getInt("LOCKER_CONTENT_LOCKER_ID"));
                li.add(rs.getInt("LOCKER_CONTENT_LOCKER_LABEL_ID"));
                li.add(rs.getInt("LOCKER_CONTENT_ID"));
                list.add(li);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public ArrayList getAllCredInfoByLabelID(int user_id, String labelId, int locker_id) {
        try {
            Connection con = DBConnection.getConnection();
            Statement smt = con.createStatement();
            String query = "";
            if (labelId == null) {

                query = "select * from locker_content_tbl where locker_content_user_id=" + user_id + " and locker_content_status=1 and locker_content_locker_id=" + locker_id;
                System.out.println("in null condition");
            } else if (labelId.equals("0")) {
                query = "select * from locker_content_tbl where locker_content_user_id=" + user_id + " and locker_content_status=1 and locker_content_locker_id=" + locker_id;
                System.out.println("in else if 0 condition");
            } else {
                query = "select * from locker_content_tbl where locker_content_user_id=" + user_id + " and locker_content_status=1 and locker_content_locker_id=" + locker_id + " and locker_content_locker_label_id='" + labelId + "'";
                System.out.println("in else condition");
            }
            ResultSet rs = smt.executeQuery(query);
            list = new ArrayList();
            while (rs.next()) {
                ArrayList li = new ArrayList();
                li.add(rs.getString("LOCKER_CONTENT_NAME"));
                li.add(rs.getString("LOCKER_CONTENT_PATH"));
                li.add(rs.getString("LOCKER_CONTENT_DESC"));
                li.add(rs.getInt("LOCKER_CONTENT_USER_ID"));
                li.add(rs.getInt("LOCKER_CONTENT_LOCKER_ID"));
                li.add(rs.getInt("LOCKER_CONTENT_LOCKER_LABEL_ID"));
                li.add(rs.getInt("LOCKER_CONTENT_ID"));
                li.add(rs.getString("LOCKER_CONTENT_EXTENSION"));
                li.add(rs.getDate("record_created_on"));
                System.out.println("li size" + li.size());
                list.add(li);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            
            e.printStackTrace();
        }
        return list;
    }

    public ArrayList getEditCredInfoByContentId(String content_id) {
        try {
            Connection con = DBConnection.getConnection();
            Statement smt = con.createStatement();
            String query = "select * from locker_content_tbl where locker_content_id=" + content_id;
            ResultSet rs = smt.executeQuery(query);
            list = new ArrayList();
            while (rs.next()) {
                ArrayList li = new ArrayList();
                li.add(rs.getString("LOCKER_CONTENT_NAME"));
                li.add(rs.getString("LOCKER_CONTENT_PATH"));
                li.add(rs.getString("LOCKER_CONTENT_DESC"));
                li.add(rs.getInt("LOCKER_CONTENT_USER_ID"));
                li.add(rs.getInt("LOCKER_CONTENT_LOCKER_ID"));
                li.add(rs.getInt("LOCKER_CONTENT_LOCKER_LABEL_ID"));
                li.add(rs.getInt("LOCKER_CONTENT_ID"));
                li.add(rs.getString("LOCKER_CONTENT_EXTENSION"));
                list.add(li);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public String getLabelName(int user_id, int label_id) {
        String name = "All Records";
        try {
            System.out.println("uid"+user_id+"\n label_id="+label_id);
            Connection con = DBConnection.getConnection();
            String query = "select * from locker_label_tbl where LOCKER_LABEL_USER_ID=" + user_id + " AND LOCKER_LABEL_ID=" + label_id;
            Statement smt = con.createStatement();
            ResultSet rs = smt.executeQuery(query);
            while (rs.next()) {
                name = rs.getString("LOCKER_LABEL_NAME");
            }
        } catch (SQLException e) {
            System.out.println("sql=" + e);
        } catch (ClassNotFoundException e) {
            System.out.println("class" + e);
        }
        return name;

    }

    public ArrayList getSearchCredentialInfo(String searchvalue, int a, int user_id, int locker_id, int label_id) {
        String upp = searchvalue.toLowerCase();
        try {
            Connection con = DBConnection.getConnection();
            String query ="select * from locker_content_tbl where LOCKER_CONTENT_USER_ID= " + user_id + " and locker_content_status=1 and LOCKER_CONTENT_LOCKER_ID =" + locker_id + " and (LOCKER_CONTENT_NAME like  ('"+upp+"%') OR LOCKER_CONTENT_NAME like  ('" + upp.toUpperCase() + "%'))";
            if (label_id == 0) {
                if (a == 0) {
                    query = "select * from locker_content_tbl where LOCKER_CONTENT_USER_ID= " + user_id + " and locker_content_status=1 and LOCKER_CONTENT_LOCKER_ID =" + locker_id + " and (LOCKER_CONTENT_NAME like  ('"+upp+"%') OR LOCKER_CONTENT_NAME like  ('" + upp.toUpperCase() + "%'))";
                } else {
                    if (a == 1) {
                        query = "select * from locker_content_tbl where LOCKER_CONTENT_USER_ID= " + user_id + " and locker_content_status=1 and LOCKER_CONTENT_LOCKER_ID =" + locker_id + " and (LOCKER_CONTENT_NAME like  ('%" + upp + "%') OR LOCKER_CONTENT_NAME like  ('%" + upp.toUpperCase() + "%'))";
                    }
                }
            } else {

                if (a == 0) {
                    query = "select * from locker_content_tbl where LOCKER_CONTENT_USER_ID= " + user_id + " and locker_content_status=1 and LOCKER_CONTENT_LOCKER_ID =" + locker_id + " and locker_content_locker_label_id=" + label_id + " and (LOCKER_CONTENT_NAME like  ('" + upp + "%') OR LOCKER_CONTENT_NAME like  ('" + upp.toUpperCase() + "%'))";
                } else {
                    if (a == 1) {
                        query = "select * from locker_content_tbl where LOCKER_CONTENT_USER_ID= " + user_id + " and locker_content_status=1 and LOCKER_CONTENT_LOCKER_ID =" + locker_id + " and locker_content_locker_label_id=" + label_id + " and (LOCKER_CONTENT_NAME like  ('%" + upp + "%') OR LOCKER_CONTENT_NAME like  ('%" + upp.toUpperCase() + "%'))";
                    }
                }

            }
            Statement smt = con.createStatement();
            ResultSet rs = smt.executeQuery(query);
            list = new ArrayList();
            while (rs.next()) {
                ArrayList li = new ArrayList();
                li.add(rs.getString("LOCKER_CONTENT_NAME"));
                li.add(rs.getString("LOCKER_CONTENT_PATH"));
                li.add(rs.getString("LOCKER_CONTENT_DESC"));
                li.add(rs.getInt("LOCKER_CONTENT_USER_ID"));
                li.add(rs.getInt("LOCKER_CONTENT_LOCKER_ID"));
                li.add(rs.getInt("LOCKER_CONTENT_LOCKER_LABEL_ID"));
                li.add(rs.getInt("LOCKER_CONTENT_ID"));
                li.add(rs.getString("LOCKER_CONTENT_EXTENSION"));
                li.add(rs.getDate("record_created_on"));
                System.out.println("li size" + li.size());
                list.add(li);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return list;
    }


    public int checkusername(String uname){
    int a=0;
    try {
            Connection con = DBConnection.getConnection();
            Statement smt = con.createStatement();
            String query = "select * from user_cred_tbl where USER_CRED_UNAME='"+uname+"'";
            ResultSet rs = smt.executeQuery(query);

            while (rs.next()) {
                a++;
            }
        } catch (ClassNotFoundException e) {
            System.out.println("class not found excption in getAllInfo" + e);
        } catch (SQLException e) {
            System.out.println("sql excption in getAllcredinfo" + e);
        }

    return a;
    }
    public int checkLabelAvailability(String uname,int user_id){
    int a=0;
    try {
            Connection con = DBConnection.getConnection();
            Statement smt = con.createStatement();
            String query = "select * from locker_label_tbl where LOCKER_LABEL_NAME='"+uname+"' and locker_label_user_id="+user_id;
            ResultSet rs = smt.executeQuery(query);

            while (rs.next()) {
                a++;
            }
        } catch (ClassNotFoundException e) {
            System.out.println("class not found excption in getAllInfo" + e);
        } catch (SQLException e) {
            System.out.println("sql excption in getAllcredinfo" + e);
        }

    return a;
    }
}

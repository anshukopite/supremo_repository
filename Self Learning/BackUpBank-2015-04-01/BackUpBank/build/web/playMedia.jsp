<%-- 
    Document   : playMedia
    Created on : Sep 29, 2012, 11:05:45 AM
    Author     : employee
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.util.ArrayList,model.GetDocInfo,model.GetLabelInfo,model.GetMusicInfo,java.sql.*,connection.DBConnection;"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script src="js/jquery.min.js" type="text/javascript"></script>
        <script src="js/jquery.fancybox-1.3.4.pack.js" type="text/javascript"></script>
        <script src="js/video.js" type="text/javascript"></script>
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link rel="stylesheet" type="text/css" href="css/jquery.fancybox-1.3.4.css">
        <link rel="stylesheet" type="text/css" href="css/loginstyle.css">
            <script type="text/javascript">
    window.history.forward();
    function noBack() {
        window.history.forward(); }
</script>
    </head>
    <body onload="noBack();"
    onpageshow="if (event.persisted) noBack();" onunload="" >
        <%response.setHeader("Cache-Control", "no-cache"); //Forces caches to obtain a new copy of the page from the origin server
        response.setHeader("Cache-Control", "no-store"); //Directs caches not to store the page under any circumstance
        response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
        response.setHeader("Pragma", "no-cache"); //HTTP 1.0 backward compatibility
        if (session.getAttribute("name") == null) {
            response.sendRedirect("index.jsp?status=session expired");
        }

        %>
        <%
        String content_id = request.getParameter("id");
        // String locker_id=request.getParameter("lockerId");
        // GetMusicInfo playMusic=new GetMusicInfo();
        Connection con = DBConnection.getConnection();
        Statement st2 = con.createStatement();
        ResultSet rs = st2.executeQuery("select LOCKER_CONTENT_PATH from locker_content_tbl where locker_content_id=" + content_id);

        if (rs.next()) {

        %>

        <div>
            <embed class="video" src="<%=rs.getString(1)%>" type="application/x-mplayer2" height="395px" width="600px"></embed>

        </div>

        <%
        } else {
            response.sendRedirect("usermusic.jsp?msg=Something Went Wrong!!");
        }
        %>
    </body>
</html>

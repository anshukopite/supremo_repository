<%-- 
    Document   : findpagenumber
    Created on : Sep 28, 2012, 3:44:11 PM
    Author     : gopal
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%response.setHeader("Cache-Control", "no-cache"); //Forces caches to obtain a new copy of the page from the origin server
        response.setHeader("Cache-Control", "no-store"); //Directs caches not to store the page under any circumstance
        response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
        response.setHeader("Pragma", "no-cache"); //HTTP 1.0 backward compatibility
        if (session.getAttribute("name") == null) {
            response.sendRedirect("index.jsp?status=session expired");
        } else {

           String pageno=request.getParameter("pageno");
               String redirctpage=request.getParameter("redirct");
               if(redirctpage.equals("5")){

                  response.sendRedirect("usercredential.jsp?nextpage="+pageno);
               }
                   if(redirctpage.equals("6")){
                   response.sendRedirect("userimportantnumber.jsp?nextpage="+pageno);
                   }
                   if(redirctpage.equals("1")){
                 response.sendRedirect("userhome.jsp?nextpage="+pageno);
                     }
                if(redirctpage.equals("2")){
                 response.sendRedirect("usermusic.jsp?nextpage="+pageno);
                }
               if(redirctpage.equals("3")){
                 response.sendRedirect("userphoto.jsp?nextpage="+pageno);
                }
               if(redirctpage.equals("4")){
                 response.sendRedirect("uservideo.jsp?nextpage="+pageno);
                }
     


        }
        %>
    </body>
</html>

<%@ page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@page import="connection.DBConnection,model.GetFileSize,java.sql.ResultSet,java.sql.Connection,java.sql.Statement,java.text.DecimalFormat" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>User Home</title>
<link rel="stylesheet" type="text/css" href="css/loginstyle.css">
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" >
$(document).ready(function()
{
$(".account").click(function()
{
var X=$(this).attr('id');

if(X==1)
{
$(".submenu").hide();
$(this).attr('id', '0');	
}
else
{

$(".submenu").show();
$(this).attr('id', '1');
}
	
});

//Mouseup textarea false
$(".submenu").mouseup(function()
{
return false
});
$(".account").mouseup(function()
{
return false
});


//Textarea without editing.
$(document).mouseup(function()
{
$(".submenu").hide();
$(".account").attr('id', '');
});
	
});
	
	</script>
    <script type="text/javascript">
    window.history.forward();
    function noBack() {
        window.history.forward(); }
</script>
<script src="js/jquery-1.4.3.min.js" type="text/javascript"></script>
<script src="js/jquery-1.8.0.min.js" type="text/javascript"></script>
<script src="js/thickbox.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="css/userprefence.css"/>
</head>

<body  onload="noBack();"
    onpageshow="if (event.persisted) noBack();" onunload="" >
        <%
        response.setHeader("Cache-Control", "no-cache"); //Forces caches to obtain a new copy of the page from the origin server
        response.setHeader("Cache-Control", "no-store"); //Directs caches not to store the page under any circumstance
        response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
        response.setHeader("Pragma", "no-cache"); //HTTP 1.0 backward compatibility
        if (session.getAttribute("name") == null) {
            response.sendRedirect("index.jsp?status=session expired");
        }else{
           // System.out.println("Session Preference="+(String)session.getAttribute("user_pref"));
    %>
<!--Wrapper starts-->
<div class="wrapper">
	<!--Header starts-->
	<div class="header">
		<!--Logoarea starts-->
		<div class="logoarea">
			<!--Logo starts-->
			<div class="logo">
				<a href="home.jsp">
					<img src="images/logo.png" alt="Logo " />
				</a>
			</div>
			<!--Logo Ends-->
			<!--Loginarea Starts-->
			<div class="loginarea">
                <div class="inbox" style="width:60px; float:left;">
                    <a href="inbox.jsp" style="color:#fff; font-weight:bold;">Inbox</a>
                </div>
				<div class="loginname">
					<h1>
						Logged In As:
							<span><%= session.getAttribute("name")%></span>
					</h1>
				</div>
				<div class="logout">
					<a href="logout.jsp">LOGOUT</a>
				</div>	
			</div>
			<!--Loginarea Ends-->
		</div>
		<!--Logoarea Ends-->
	</div>
	<!--Header ends-->
	<!--Main Container Starts-->
	<div class="maincontainer">
		<!--Left Container Starts-->
		<div class="leftcontainer">
			<!--LeftMenu Starts-->
			<div class="leftmenu">
				<div class="myaccount">
					 <label>My Account</label>
				</div>
				<ul>
					<li>
						<a href="myprofile.jsp?stat=view">My Profile</a>
					</li>
					<li>
						<a href="managespace.jsp">Manage Space</a>
					</li>
                    <li>
						<a href="managelabel.jsp">Manage Label</a>
					</li>
                    <li>
						<a href="userpreferences.jsp?width=400&height=300" class="thickbox" title="User Preference">User Preferences</a>
					</li>
					<li>
						<a href="changepassword.jsp">Change Password</a>
					</li>
					<li>
						<a href="askquestion.jsp">Ask A Question</a>
					</li>
                    <li>
						<a href="feedback.jsp">Feedback</a>
					</li>
				</ul>
			</div>
			<!--LeftMenu Ends-->
		</div>
		<!--Left Container Ends-->
		<!--Right Container Starts-->
		<div class="rightcontainer">
			<!--treestructure starts-->
			<div class="treestruct">
				<div class="treelist">
					<label>User Home</label>
				</div>
				<div class="searchbox">
					<div style="float:right;">
						<div class="dropdown">
							<a class="account" >
								<span>View By</span>
							</a>
							<div class="submenu" style="display: none; ">
								<ul class="root">
									<li>
									  <a href="home.jsp" >Thumbnail</a>
									</li>
									 <li>
									 	 <a href="homedetail.jsp">Detail</a>
	    							</li>
	   							</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<!--treestructure ends-->
			<!--Menu Starts-->
                    <%if(session.getAttribute("user_pref").equals("C")){%>
			<div class="menu">
				<ul>
					<li><a href="home.jsp" class="current">Home</a></li>
					<li><a href="repassword.jsp?locker_id=1">Docs</a></li>
					<li><a href="repassword.jsp?locker_id=2">Music</a></li>
					<li><a href="repassword.jsp?locker_id=3">Photos</a></li>
					<li><a href="repassword.jsp?locker_id=4">Videos</a></li>
					<li><a href="repassword.jsp?locker_id=5">Credentials</a></li>
					<li><a href="repassword.jsp?locker_id=6">Important Numbers</a></li>
				</ul>
			</div>
            <%}else{%>
            <div class="menu">
            <ul>
					<li><a href="home.jsp" class="current">Home</a></li>
					<li><a href="userhome.jsp?nextpage=1&label_id=0">Docs</a></li>
					<li><a href="usermusic.jsp?nextpage=1&label_id=0">Music</a></li>
					<li><a href="userphoto.jsp?nextpage=1&label_id=0">Photos</a></li>
					<li><a href="uservideo.jsp?nextpage=1&label_id=0">Videos</a></li>
					<li><a href="usercredential.jsp?nextpage=1&label_id=0">Credentials</a></li>
					<li><a href="userimportantnumber.jsp?nextpage=1&label_id=0">Important Numbers</a></li>
            </ul>
            </div>
            <%}%>
			<!--Menu Ends-->
			
			<!--maincontent starts-->
			<div class="main">
                <%
                     try{
                Connection con=null;
                ResultSet rs=null;
                Statement st=null;
               
                double size=0;
                int fileCount=0;
                String sizeLabel="KB";
           
                       String sql="select locker_content_path from locker_content_tbl where locker_content_user_id="+(Integer)session.getAttribute("uid");
                       GetFileSize file=new GetFileSize();
                       DBConnection db=new DBConnection();
                       con=db.getConnection();
                       st=con.createStatement();
                       rs=st.executeQuery(sql);
                       while(rs.next()){
                                size=size+file.GetFileSize(getServletContext().getRealPath("/")+rs.getString(1));
                                fileCount=file.GetFileCount(getServletContext().getRealPath("/")+rs.getString(1));
                       }
                       

                

        %>
				<!--mainarea starts-->
				<div class="mainarea">
					<div class="icon">
						<div class="iconblock">
                            <%if(session.getAttribute("user_pref").equals("C")){%>
                            <a href="repassword.jsp?locker_id=1"><img src="images/img4.png" alt="Document image" title="Docs" /></a>
                            <%}else{%>
                            <a href="userhome.jsp?nextpage=1&label_id=0"><img src="images/img4.png" alt="Document image" title="Docs" /></a>
                            <%}%>
							<p align="CENTER">
                                <b>DOCS LOCKER</b>
							</p>
						</div>
						<div class="iconblock">
                            <%if(session.getAttribute("user_pref").equals("C")){%>
                             <a href="repassword.jsp?locker_id=2"><img src="images/img5.png" alt="Document image" title="Music" /></a>
                              <%}else{%>
                              <a href="usermusic.jsp?nextpage=1&label_id=0"><img src="images/img5.png" alt="Document image" title="Music" /></a>
                              <%}%>
							<p align="CENTER">
                                <b>MUSIC LOCKER</b>
							</p>
						</div>
						<div class="iconblock">
                            <%if(session.getAttribute("user_pref").equals("C")){%>
                             <a href="repassword.jsp?locker_id=3"><img src="images/img1.1.png"alt="Document image" title="Photos" /></a>
                             <%}else{%>
                             <a href="userphoto.jsp?nextpage=1&label_id=0"><img src="images/img1.1.png"alt="Document image" title="Photos" /></a>
                              <%}%>
							<p align="CENTER">
                                <b>PHOTOS LOCKER</b>
							</p>
						</div>
						
					</div>
					<div class="icon">
						<div class="iconblock">
                            <%if(session.getAttribute("user_pref").equals("C")){%>
                             <a href="repassword.jsp?locker_id=4"><img src="images/img6.png" alt="Document image" title="Video" /></a>
                              <%}else{%>
                              <a href="uservideo.jsp?nextpage=1&label_id=0"><img src="images/img6.png" alt="Document image" title="Video" /></a>
                               <%}%>
							<p align="CENTER">
                                <b>VIDEOS LOCKER</b>
							</p>
						</div>
						<div class="iconblock">
                            <%if(session.getAttribute("user_pref").equals("C")){%>
                             <a href="repassword.jsp?locker_id=5"><img src="images/img2.2.png" alt="Document image" title="Credential" /></a>
                             <%}else{%>
                             <a href="usercredential.jsp?nextpage=1&label_id=0"><img src="images/img2.2.png" alt="Document image" title="Credential" /></a>
                             <%}%>
							<p align="CENTER">
                                <b>CREDENTIALS LOCKER</b>
							</p>
						</div>
						<div class="iconblock">
                            <%if(session.getAttribute("user_pref").equals("C")){%>
                             <a href="repassword.jsp?locker_id=6"><img src="images/img3.3.png"alt="Document image" title="Important Number" /></a>
                             <%}else{%>
                             <a href="userimportantnumber.jsp?nextpage=1&label_id=0"><img src="images/img3.3.png"alt="Document image" title="Important Number" /></a>
                             <%}%>
							<p align="CENTER">
                                <b>IMPORTANT NUMBERS LOCKER</b>
							</p>
						</div>
						
					</div>

                        Total Size used: <%if(size>=1024.00 && size<1048576.00){

                                                        size=size/1024.00;

                                                        sizeLabel="MB";
                                                            }
                                                            if(size>=1048576.00){
                                                                    size=size/1024.00;
                                                                    sizeLabel="GB";
                                                             
                                                             }

                                                        DecimalFormat df = new DecimalFormat("#.##");
                                                        size=Double.parseDouble(df.format(size));
                                                        out.print(size+" "+sizeLabel);



                       %>
                       Total Files:<%=fileCount%>
				</div>

				<!--mainarea ends-->
                <%}catch(Exception e){
                        e.printStackTrace();
                }%>
			</div>
			<!--maincontent ends-->
		</div>
		<!--Right Container Ends-->	
	</div>
	<!--Main Container Ends-->
	<!--Footer starts-->
	<div class="footer">
		<div class="footerleft">
			<p>
				&copy; All Copyrights reserved. <a href="#">www.bub.com</a>
			</p>
		</div>
		<div class="footerright">
			<ul>
				<li><a href="home.jsp">Home</a> &nbsp; &nbsp;</li>
			<!--	<li><a href="about.jsp">About Us</a> &nbsp;| &nbsp;</li>
				<li><a href="privacy.jsp">Privacy</a> &nbsp;| &nbsp;</li>
				<li><a href="terms.jsp">Terms</a> &nbsp;| &nbsp;</li>
				<li><a href="contact.jsp">Contact Us</a></li>-->
				
			</ul>
		</div>
	</div>
	<!--Footer Ends-->
</div>
<!--Wrapper ends-->
</body>
</html>
<% } %>
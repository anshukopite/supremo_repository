<%-- 
    Document   : setRecordLabel
    Created on : Sep 29, 2012, 10:04:16 AM
    Author     : employee
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%response.setHeader("Cache-Control", "no-cache"); //Forces caches to obtain a new copy of the page from the origin server
        response.setHeader("Cache-Control", "no-store"); //Directs caches not to store the page under any circumstance
        response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
        response.setHeader("Pragma", "no-cache"); //HTTP 1.0 backward compatibility
        if (session.getAttribute("name") == null) {
            response.sendRedirect("index.jsp?status=session expired");
        }

        %>
        <%
        String label_id = request.getParameter("label_id");
        session.setAttribute("label", label_id);
        response.sendRedirect("userhome.jsp");
        %>
    </body>
</html>

<%@ page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@page  import="model.*,java.util.*;" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>Admin Inbox</title>
        <script type="text/javascript" src="js/jquery-1.6.4.min.js"></script>
        <script>
            $(document).ready(function() {
                $('a.login-window').click(function() {

                    //Getting the variable's value from a link
                    var loginBox = $(this).attr('href');

                    //Fade in the Popup
                    $(loginBox).fadeIn(300);

                    //Set the center alignment padding + border see css style
                    var popMargTop = ($(loginBox).height() + 24) / 2;
                    var popMargLeft = ($(loginBox).width() + 24) / 2;

                    $(loginBox).css({
                        'margin-top' : -popMargTop,
                        'margin-left' : -popMargLeft
                    });

                    // Add the mask to body
                    $('body').append('<div id="mask"></div>');
                    $('#mask').fadeIn(300);

                    return false;
                });

                // When clicking on the button close or the mask layer the popup closed
                $('a.close, #mask').live('click', function() {
                    $('#mask , .login-popup').fadeOut(300 , function() {
                        $('#mask').remove();
                    });
                    return false;
                });
            });
        </script>
        <script src="js/jquery.js" type="text/javascript"></script>
        <script src="js/jsDatePick.min.1.3.js" type="text/javascript"></script>
        <script type="text/javascript" src="js/ddaccordion.js"></script>
        <link rel="stylesheet" type="text/css" href="css/loginstyle.css">
        <link rel="stylesheet" type="text/css" href="css/messagebox.css">
        <link rel="stylesheet" href="css/jsDatePick_ltr.min.css" type="text/css" />

        <script type="text/javascript">
            window.onload = function(){


                g_globalObject = new JsDatePick({
                    useMode:1,
                    isStripped:true,
                    target:"div3_example"
                    /*selectedDate:{				This is an example of what the full configuration offers.
                day:5,						For full documentation about these settings please see the full version of the code.
                month:9,
                year:2006
            },
            yearsRange:[1978,2020],
            limitToToday:false,
            cellColorScheme:"beige",
            dateFormat:"%m-%d-%Y",
            imgPath:"img/",
            weekStartDay:1*/
                });

                g_globalObject.setOnSelectedDelegate(function(){
                    var obj = g_globalObject.getSelectedDay();
                    alert("a date was just selected and the date is : " + obj.day + "/" + obj.month + "/" + obj.year);
                    document.getElementById("div3_example_result").innerHTML = obj.day + "/" + obj.month + "/" + obj.year;
                });


                g_globalObject2 = new JsDatePick({
                    useMode:1,
                    isStripped:false,
                    target:"div4_example",
                    cellColorScheme:"beige"
                    /*selectedDate:{				This is an example of what the full configuration offers.
                day:5,						For full documentation about these settings please see the full version of the code.
                month:9,
                year:2006
            },
            yearsRange:[1978,2020],
            limitToToday:false,
            dateFormat:"%m-%d-%Y",
            imgPath:"img/",
            weekStartDay:1*/
                });

                g_globalObject2.setOnSelectedDelegate(function(){
                    var obj = g_globalObject2.getSelectedDay();
                    alert("a date was just selected and the date is : " + obj.day + "/" + obj.month + "/" + obj.year);
                    document.getElementById("div3_example_result").innerHTML = obj.day + "/" + obj.month + "/" + obj.year;
                });

            };
        </script>

        <script type="text/javascript">


            ddaccordion.init({
                headerclass: "expandable", //Shared CSS class name of headers group that are expandable
                contentclass: "categoryitems", //Shared CSS class name of contents group
                revealtype: "click", //Reveal content when user clicks or onmouseover the header? Valid value: "click", "clickgo", or "mouseover"
                mouseoverdelay: 200, //if revealtype="mouseover", set delay in milliseconds before header expands onMouseover
                collapseprev: true, //Collapse previous content (so only one open at any time)? true/false
                defaultexpanded: [0], //index of content(s) open by default [index1, index2, etc]. [] denotes no content
                onemustopen: false, //Specify whether at least one header should be open always (so never all headers closed)
                animatedefault: false, //Should contents open by default be animated into view?
                persiststate: true, //persist state of opened contents within browser session?
                toggleclass: ["", "openheader"], //Two CSS classes to be applied to the header when it's collapsed and expanded, respectively ["class1", "class2"]
                togglehtml: ["prefix", "", ""], //Additional HTML added to the header when it's collapsed and expanded, respectively  ["position", "html1", "html2"] (see docs)
                animatespeed: "fast", //speed of animation: integer in milliseconds (ie: 200), or keywords "fast", "normal", or "slow"
                oninit:function(headers, expandedindices){ //custom code to run when headers have initalized
                    //do nothing
                },
                onopenclose:function(header, index, state, isuseractivated){ //custom code to run whenever a header is opened or closed
                    //do nothing
                }
            })


        </script>

        <script type="text/javascript" >
            $(document).ready(function()
            {
                $(".account").click(function()
                {
                    var X=$(this).attr('id');

                    if(X==1)
                    {
                        $(".submenu").hide();
                        $(this).attr('id', '0');
                    }
                    else
                    {

                        $(".submenu").show();
                        $(this).attr('id', '1');
                    }

                });

                //Mouseup textarea false
                $(".submenu").mouseup(function()
                {
                    return false
                });
                $(".account").mouseup(function()
                {
                    return false
                });


                //Textarea without editing.
                $(document).mouseup(function()
                {
                    $(".submenu").hide();
                    $(".account").attr('id', '');
                });

            });

        </script>

    </head>

    <body>
        <!--Wrapper starts-->
        <div class="wrapper">
        <!--Header starts-->
        <div class="header">
            <!--Logoarea starts-->
            <div class="logoarea">
                <!--Logo starts-->
                <div class="logo">
                    <a href="adminuserlist.jsp">
                        <img src="images/logo.png" alt="Logo " />
                    </a>
                </div>
                <!--Logo Ends-->
            <!--Loginarea Starts-->
                <div class="loginarea">
                    <div class="loginname">
                        <h1>
                            Logged In As:
                            <span><%= session.getAttribute("aduname") %></span>
                        </h1>
                    </div>
                    <div class="logout">
                        <a href="adminlogout.jsp">LOGOUT</a>
                    </div>
                </div>
                <!--Loginarea Ends-->
            </div>
            <!--Logoarea Ends-->
        </div>
        <!--Header ends-->
    <!--Main Container Starts-->
        <div class="maincontainer">
        <!--Left Container Starts-->
        <div class="leftcontainer">
            <div class="arrowlistmenu">
                <h3 class="menuheader" style="cursor:pointer default"><a href="adminmyprofile.jsp">My Profile</a></h3>
                <h3 class="menuheader expandable">Task</h3>
                <ul class="categoryitems">
                    <li><a href="adminuserlist.jsp">User List</a></li>
                    <li><a href="admininbox.jsp" >Inbox</a></li>
                    <li><a href="adminuserlog.jsp">User Log</a></li>
                     <li><a href="replyLog.jsp" class="current">Reply Log</a></li>
                    <li><a href="adminfeedback.jsp">Feedback</a></li>
                </ul>
                <h3 class="menuheader expandable">Calendar</h3>
                <ul class="categoryitems">
                    <a href="" id="div3_example" style="margin:-1px;width:230px; height:230px;"></a>
                </ul>
            </div>
        </div>
        <!--Left Container Ends-->
        <!--Right Container Starts-->
        <div class="rightcontainer">
            <!--treestructure starts-->
            <div class="treestruct">
                <div class="treelist">
                    <div class="dropdown">
                        <a class="account" >
                            <span>View By</span>
                        </a>
                        <div class="submenu" style="display: none; ">
                            <ul class="root">
                                <li>
                                    <a href="admininbox.jsp?sort=name" >Name</a>
                                </li>
                                <li>
                                    <a href="admininbox.jsp?sort=date" >Date</a>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!--treestructure ends-->

            <!--maincontent starts-->

                 <%ArrayList list = null;
        try {
            String sort = "name";

            if (sort.equals("name")) {
                sort = request.getParameter("sort");
                GetUserInfo uinfo = new GetUserInfo();
                list = uinfo.getReplyByName(1);

            %>
            <div class="main">
                <!--mainarea starts-->
                <div class="mainarea">
                    <form action="AdminReply" method="post">
                        <table class="bordered">
                            <thead>
                                <tr>
                                    <th>User Name</th>
                                    
                                    <th>Subject</th>
                                    <th>Replied On</th>
                                </tr>
                            </thead>
                            <%
                            for (int i = 0; i < list.size(); i++) {
                                ArrayList li = (ArrayList) list.get(i);
                                int amsgid=(Integer.parseInt(li.get(5).toString()));
                                 System.out.println("adminmsgid"+amsgid);
                                 int usermsgid = (Integer.parseInt(li.get(6).toString()));
                                 System.out.println("userid "+usermsgid);
                                 int userid = (Integer.parseInt(li.get(7).toString()));
                                 System.out.println("userid "+userid);
                                 

                            %>
                            <tr>
                                <td>
                                    <label>

                                        <a href="#message-box<%=i%>" class="logininfo login-window"><%=li.get(0)%></a>
                                        <div id="message-box<%=i%>" class="login-popup"  style=" width: 285px;">
                                            <a href="#" class="close">
                                                <img src="images/close_pop.png" class="btn_close" title="Close Window" alt="Close" />
                                            </a>
                                            <fieldset class="textarea">
                                                <label>From: <%out.print(li.get(0));%></label><br/>
                                                <label>Subject: <%=li.get(1)%></label>
                                                <p>Ques: <%=li.get(4)%></p>
                                                <p>Answer: <%= li.get(2)%></p>
                                                <input type="hidden" value="<%= li.get(4)%>" name="userid" />

                                                 <a href="#" class="close"><input type="button" value="Close" class="clickaddbtn" /></a>
                                            </fieldset>
                                        </div>

                                    </label>
                                </td>
                               
                                <td>
                                    <label><%=li.get(1)%></label>
                                </td>
                                <td>
                                    <label><%=li.get(3)%></label>
                                </td>
                            </tr><%}%>


                        </table>
                    </form>
                </div>
                <%
                        } else if (sort.equals("date")) {
                            sort = request.getParameter("sort");
                            GetUserInfo uinfo = new GetUserInfo();
                            list = uinfo.getReplyByDate(1);


                %>

                <div class="main">
                    <!--mainarea starts-->
                    <div class="mainarea">
                        <form action="adminInboxDelete" method="post">
                            <table class="bordered">
                                <thead>
                                    <tr>
                                        <th>User Name</th>
                                        <th>Subject</th>
                                        <th>Created On</th>
                                    </tr>
                                </thead>
                               <%
                            for (int i = 0; i < list.size(); i++) {
                                ArrayList li = (ArrayList) list.get(i);
                                int amsgid=(Integer.parseInt(li.get(5).toString()));
                                 System.out.println("adminmsgid"+amsgid);
                                 int usermsgid = (Integer.parseInt(li.get(6).toString()));
                                 System.out.println("userid "+usermsgid);
                                 int userid = (Integer.parseInt(li.get(7).toString()));
                                 System.out.println("userid "+userid);


                            %>
                                <tr>
                                <td>
                                    <label>

                                        <a href="#message-box<%=i%>" class="logininfo login-window"><%=li.get(0)%></a>
                                        <div id="message-box<%=i%>" class="login-popup"  style=" width: 285px;">
                                            <a href="#" class="close">
                                                <img src="images/close_pop.png" class="btn_close" title="Close Window" alt="Close" />
                                            </a>
                                            <fieldset class="textarea">
                                                <label>From: <%out.print(li.get(0));%></label><br/>
                                                <label>Subject: <%=li.get(1)%></label>
                                                <p>Ques: <%=li.get(4)%></p>
                                                <p>Answer: <%= li.get(2)%></p>
                                                <input type="hidden" value="<%= li.get(4)%>" name="userid" />

                                                 <a href="#" class="close"><input type="button" value="Close" class="clickaddbtn" /></a>
                                            </fieldset>
                                        </div>

                                    </label>
                                </td>

                                <td>
                                    <label><%=li.get(1)%></label>
                                </td>
                                <td>
                                    <label><%=li.get(3)%></label>
                                </td>
                            </tr><%}%>


                        </table>
                    </form>
                </div>

                    <%
                        } else  {
                            GetUserInfo uinfo = new GetUserInfo();
                            list = uinfo.getReplyByName(1);
                    %>
                    <div class="main">
                <!--mainarea starts-->
                <div class="mainarea">
                    <form action="AdminReply" method="post">
                        <table class="bordered">
                            <thead>
                                <tr>
                                    <th>User Name</th>

                                    <th>Subject</th>
                                    <th>Replied On</th>
                                </tr>
                            </thead>
                            <%
                            for (int i = 0; i < list.size(); i++) {
                                ArrayList li = (ArrayList) list.get(i);
                                int amsgid=(Integer.parseInt(li.get(5).toString()));
                                 System.out.println("adminmsgid"+amsgid);
                                 int usermsgid = (Integer.parseInt(li.get(6).toString()));
                                 System.out.println("userid "+usermsgid);
                                 int userid = (Integer.parseInt(li.get(7).toString()));
                                 System.out.println("userid "+userid);


                            %>
                            <tr>
                                <td>
                                    <label>

                                        <a href="#message-box<%=i%>" class="logininfo login-window"><%=li.get(0)%></a>
                                        <div id="message-box<%=i%>" class="login-popup"  style=" width: 285px;">
                                            <a href="#" class="close">
                                                <img src="images/close_pop.png" class="btn_close" title="Close Window" alt="Close" />
                                            </a>
                                            <fieldset class="textarea">
                                                <label>From: <%out.print(li.get(0));%></label><br/>
                                                <label>Subject: <%=li.get(1)%></label>
                                                <p>Ques: <%=li.get(4)%></p>
                                                <p>Answer: <%= li.get(2)%></p>
                                                <input type="hidden" value="<%= li.get(4)%>" name="userid" />

                                                 <a href="#" class="close"><input type="button" value="Close" class="clickaddbtn" /></a>
                                            </fieldset>
                                        </div>

                                    </label>
                                </td>

                                <td>
                                    <label><%=li.get(1)%></label>
                                </td>
                                <td>
                                    <label><%=li.get(3)%></label>
                                </td>
                            </tr><%}%>


                        </table>
                    </form>
                </div>
                        <% }

        } catch (Exception c) {
            c.printStackTrace();
        }

                        %>
                        <!--mainarea ends-->
                        <!--Pagination starts-->
                        <!--<div class="pagenation">
                            <span>Page:</span>
                            <ul>
                                <li><a href="admininbox.jsp" class="current">1</a></li>
                                <li><a href="">2</a></li>
                                <li><a href="">&raquo;</a></li>
                            </ul>
                        </div>-->
                        <!--Pagination ends-->

                    </div>
                    <!--maincontent ends-->
                </div>
                <!--Right Container Ends-->
            </div>
            <!--Main Container Ends-->
            <!--Footer starts-->
            <div class="footer">
                <div class="footerleft">
                    <p>
                        &copy; All Copyrights reserved. <a href="#">www.bub.com</a>
                    </p>
                </div>
                <div class="footerright">
			<ul>
				<li><a href="adminuserlist.jsp">Home</a> &nbsp; &nbsp;</li>
			<!--	<li><a href="about.jsp">About Us</a> &nbsp;| &nbsp;</li>
				<li><a href="privacy.jsp">Privacy</a> &nbsp;| &nbsp;</li>
				<li><a href="terms.jsp">Terms</a> &nbsp;| &nbsp;</li>
				<li><a href="contact.jsp">Contact Us</a></li>-->

			</ul>
		</div>
            </div>
            <!--Footer Ends-->
        </div>
        <!--Wrapper ends-->
    </body>
</html>

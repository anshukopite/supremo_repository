<%@ page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@page import="model.GetUserInfo,java.util.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>Admin User List</title>
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/ddaccordion.js"></script>
        <link rel="stylesheet" href="css/jsDatePick_ltr.min.css" type="text/css" />
        <link rel="stylesheet" type="text/css" href="css/loginstyle.css">
        <script type="text/javascript">


            ddaccordion.init({
                headerclass: "expandable", //Shared CSS class name of headers group that are expandable
                contentclass: "categoryitems", //Shared CSS class name of contents group
                revealtype: "click", //Reveal content when user clicks or onmouseover the header? Valid value: "click", "clickgo", or "mouseover"
                mouseoverdelay: 200, //if revealtype="mouseover", set delay in milliseconds before header expands onMouseover
                collapseprev: true, //Collapse previous content (so only one open at any time)? true/false
                defaultexpanded: [0], //index of content(s) open by default [index1, index2, etc]. [] denotes no content
                onemustopen: false, //Specify whether at least one header should be open always (so never all headers closed)
                animatedefault: false, //Should contents open by default be animated into view?
                persiststate: true, //persist state of opened contents within browser session?
                toggleclass: ["", "openheader"], //Two CSS classes to be applied to the header when it's collapsed and expanded, respectively ["class1", "class2"]
                togglehtml: ["prefix", "", ""], //Additional HTML added to the header when it's collapsed and expanded, respectively  ["position", "html1", "html2"] (see docs)
                animatespeed: "fast", //speed of animation: integer in milliseconds (ie: 200), or keywords "fast", "normal", or "slow"
                oninit:function(headers, expandedindices){ //custom code to run when headers have initalized
                    //do nothing
                },
                onopenclose:function(header, index, state, isuseractivated){ //custom code to run whenever a header is opened or closed
                    //do nothing
                }
            })


        </script>

        <script type="text/javascript">
            window.onload = function(){


                g_globalObject = new JsDatePick({
                    useMode:1,
                    isStripped:true,
                    target:"div3_example"
                    /*selectedDate:{				This is an example of what the full configuration offers.
                day:5,						For full documentation about these settings please see the full version of the code.
                month:9,
                year:2006
            },
            yearsRange:[1978,2020],
            limitToToday:false,
            cellColorScheme:"beige",
            dateFormat:"%m-%d-%Y",
            imgPath:"img/",
            weekStartDay:1*/
                });

                g_globalObject.setOnSelectedDelegate(function(){
                    var obj = g_globalObject.getSelectedDay();
                    alert("a date was just selected and the date is : " + obj.day + "/" + obj.month + "/" + obj.year);
                    document.getElementById("div3_example_result").innerHTML = obj.day + "/" + obj.month + "/" + obj.year;
                });



                g_globalObject2 = new JsDatePick({
                    useMode:1,
                    isStripped:false,
                    target:"div4_example",
                    cellColorScheme:"beige"
                    /*selectedDate:{				This is an example of what the full configuration offers.
                day:5,						For full documentation about these settings please see the full version of the code.
                month:9,
                year:2006
            },
            yearsRange:[1978,2020],
            limitToToday:false,
            dateFormat:"%m-%d-%Y",
            imgPath:"img/",
            weekStartDay:1*/
                });

                g_globalObject2.setOnSelectedDelegate(function(){
                    var obj = g_globalObject2.getSelectedDay();
                    alert("a date was just selected and the date is : " + obj.day + "/" + obj.month + "/" + obj.year);
                    document.getElementById("div3_example_result").innerHTML = obj.day + "/" + obj.month + "/" + obj.year;
                });

            };
        </script>
        <script src="js/jsDatePick.min.1.3.js" type="text/javascript"></script>
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" >
            $(document).ready(function()
            {
                $(".account").click(function()
                {
                    var X=$(this).attr('id');

                    if(X==1)
                    {
                        $(".submenu").hide();
                        $(this).attr('id', '0');
                    }
                    else
                    {

                        $(".submenu").show();
                        $(this).attr('id', '1');
                    }

                });

                //Mouseup textarea false
                $(".submenu").mouseup(function()
                {
                    return false
                });
                $(".account").mouseup(function()
                {
                    return false
                });


                //Textarea without editing.
                $(document).mouseup(function()
                {
                    $(".submenu").hide();
                    $(".account").attr('id', '');
                });

            });

        </script>
    </head>
<%
          if (session.getAttribute("aduname") == null) {
            response.sendRedirect("index.jsp?status=session expired");

        }

    %>
    <body>
        <!--Wrapper starts-->
        <div class="wrapper">
            <!--Header starts-->
            <div class="header">
                <!--Logoarea starts-->
                <div class="logoarea">
                    <!--Logo starts-->
                    <div class="logo">
                        <a href="adminuserlist.jsp">
                            <img src="images/logo.png" alt="Logo " />
                        </a>
                    </div>
                    <!--Logo Ends-->
            <!--Loginarea Starts-->
                    <div class="loginarea">
                        <div class="loginname">
                            <h1>
                                Logged In As:
                                <span><%= session.getAttribute("aduname")%></span>
                            </h1>
                        </div>
                        <div class="logout">
                            <a href="adminlogout.jsp">LOGOUT</a>
                        </div>
                    </div>
                    <!--Loginarea Ends-->
                </div>
                <!--Logoarea Ends-->
            </div>
            <!--Header ends-->
    <!--Main Container Starts-->
      <%
            String dfmsg=request.getParameter("dfmsg");
             String dsmsg=request.getParameter("dsmsg");
              String asmsg=request.getParameter("asmsg");
              String afmsg=request.getParameter("afmsg");
            if(dfmsg!=null){
                %>
             <script>
                            alert("Deactivation failed!");
                                    </script>
            <%}%>

             <%

            if(dsmsg!=null){
                %>
                        <script>
                             alert("Deactivation successful!");
                                    </script>
            <%}%>
             <%

            if(asmsg!=null){
                %>
                        <script>
                             alert("Activation successful!");
                                    </script>
            <%}%>
             <%

            if(afmsg!=null){
                %>
                        <script>
                             alert("Activation failed!");
                                    </script>
            <%}%>
            <div class="maincontainer">
                <!--Left Container Starts-->
                <div class="leftcontainer">
                    <div class="arrowlistmenu">
                        <h3 class="menuheader" style="cursor:pointer default"><a href="adminmyprofile.jsp">My Profile</a></h3>
                        <h3 class="menuheader expandable">Task</h3>
                        <ul class="categoryitems">
                            <li><a href="adminuserlist.jsp" class="current">User List</a></li>
                            <li><a href="admininbox.jsp">Inbox</a></li>
                            <li><a href="adminuserlog.jsp">User Log</a></li>
                             <li><a href="replyLog.jsp">Reply Log</a></li>
                             <li><a href="adminfeedback.jsp">Feedback</a></li>
                        </ul>
                        <h3 class="menuheader expandable">Calendar</h3>
                        <ul class="categoryitems">
                            <a href="" id="div3_example" style="margin:-1px;width:230px; height:230px;"></a>
                        </ul>
                    </div>
                </div>
                <!--Left Container Ends-->
            <!--Right Container Starts-->
                <div class="rightcontainer">
                    <!--treestructure starts-->
                    <div class="treestruct">
                        <div class="treelist">
                            <div class="dropdown">
                                <a class="account" >
                                    <span>View By</span>
                                </a>
                                <div class="submenu" style="display: none; ">
                                    <ul class="root">
                                        <li>
                                            <a href="adminuserlist.jsp?sort=name">Name</a>
                                        </li>
                                        <li>
                                            <a href="adminuserlist.jsp?sort=date" >Date</a>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!--treestructure ends-->

                <!--maincontent starts-->

                 <%ArrayList list = null;
        try {

            String sort = request.getParameter("sort");

            if(sort==null){
                sort="name";
                             GetUserInfo uinfo = new GetUserInfo();
                list = uinfo.getUserDetailsByName(1);
               
    
            }
            if (sort.equals("name")) {

                GetUserInfo uinfo = new GetUserInfo();
                list = uinfo.getUserDetailsByName(1);
            }
            if(sort.equals("date")){
                GetUserInfo uinfo = new GetUserInfo();
                list = uinfo.getUserDetailsByDate(1);

            }
                    %>
                    <div class="main">
                        <!--mainarea starts-->

                        <div class="mainarea">
                            <table class="bordered">
                                <thead>
                                    <tr>
                                        <th>User Name</th>
                                        <th>Date Of Creation</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <%
            for (int i = 0; i < list.size(); i++) {
                ArrayList li = (ArrayList) list.get(i);

                                %>
                                <tr>
                                    <td>

                                        <label><%=li.get(1)%></label>

                                    </td>
                                    <td>
                                        <label><%=li.get(2)%></label>
                                    </td>
                                    <td>
                                        <label><%
                                    if (Integer.parseInt(li.get(3).toString()) == 1) {

                                            %> <a href="Status?status=<%=li.get(3)%>&userid=<%=li.get(0)%>&by=admin">Active</a>
                                            <%} else {
                                            %><a href="Status?status=<%=li.get(3)%>&userid=<%=li.get(0)%>&by=admin">Deactive</a>
                                            <%}


                                        %></label>
                                    </td>
                                </tr><%}%>



                            </table>
                        </div>

                        <%


        } catch (Exception c) {
            c.printStackTrace();
        }

                        %>

                        <!--mainarea ends-->
                        <!--Pagination starts-->
                        <!--<div class="pagenation">
                            <span>Page:</span>
                            <ul>
                                <li><a href="admininbox.jsp" class="current">1</a></li>
                                <li><a href="">2</a></li>
                                <li><a href="">&raquo;</a></li>
                            </ul>
                        </div>-->
                        <!--Pagination ends-->

                    </div>
                    <!--maincontent ends-->
                </div>
                <!--Right Container Ends-->
            </div>
            <!--Main Container Ends-->
            <!--Footer starts-->
            <div class="footer">
                <div class="footerleft">
                    <p>
                        &copy; All Copyrights reserved. <a href="#">www.bub.com</a>
                    </p>
                </div>
                <div class="footerright">
			<ul>
				<li><a href="adminuserlist.jsp">Home</a> &nbsp; &nbsp;</li>
			<!--	<li><a href="about.jsp">About Us</a> &nbsp;| &nbsp;</li>
				<li><a href="privacy.jsp">Privacy</a> &nbsp;| &nbsp;</li>
				<li><a href="terms.jsp">Terms</a> &nbsp;| &nbsp;</li>
				<li><a href="contact.jsp">Contact Us</a></li>-->

			</ul>
		</div>
            </div>
            <!--Footer Ends-->
        </div>
        <!--Wrapper ends-->
    </body>
</html>

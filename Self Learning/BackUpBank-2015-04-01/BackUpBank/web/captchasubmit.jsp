<%@ page import="nl.captcha.Captcha"%>
<%
response.setHeader("Cache-Control", "no-cache"); //Forces caches to obtain a new copy of the page from the origin server
            response.setHeader("Cache-Control", "no-store"); //Directs caches not to store the page under any circumstance
            response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
            response.setHeader("Pragma", "no-cache"); //HTTP 1.0 backward compatibility 
if(session.getAttribute("name")==null){
            response.sendRedirect("index.jsp?status=session expired");
        }

    %>
<%

        Captcha captcha = (Captcha) session.getAttribute(Captcha.NAME);

        request.setCharacterEncoding("UTF-8");

        String answer = request.getParameter("answer");

        if (captcha.isCorrect(answer)) {

            response.sendRedirect("SignUp");
        } else {
        }

%>


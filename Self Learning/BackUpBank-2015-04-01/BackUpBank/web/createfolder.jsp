<%response.setHeader("Cache-Control", "no-cache"); //Forces caches to obtain a new copy of the page from the origin server
        response.setHeader("Cache-Control", "no-store"); //Directs caches not to store the page under any circumstance
        response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
        response.setHeader("Pragma", "no-cache"); //HTTP 1.0 backward compatibility
        if (session.getAttribute("name") == null) {
            response.sendRedirect("index.jsp?status=session expired");
        }

%>
<html>
    <head>
        <script>
            function checkvalidate(){
                var name=document.forms["createfolder"]["newlabel"].value;
                var validate1=document.getElementById("username").value;
                var validate=document.getElementsByTagName("username").value;
                 
                if(name==null||name==""){

                    alert("Please enter label name");
                    return false;
                }
                if(validate=="label name already exist"){
                     alert("label name already exist");
                     return false;
                }


            }
            function checkLabelAvailability(str)
            {


                var xmlhttp;
                if (window.XMLHttpRequest)
                {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp=new XMLHttpRequest();
                }
                else
                {// code for IE6, IE5
                    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange=function()
                {
                    if (xmlhttp.readyState==4 && xmlhttp.status==200)
                    {

                        document.getElementById("username").innerHTML=xmlhttp.responseText;


                    }
                }

                xmlhttp.open("GET","checklabelavailability.jsp?labelname="+str,true);
                xmlhttp.send();


            }
        </script>
    </head>
<div>

    <form action="SetLabel" name="createfolder"onsubmit="return checkvalidate();">
        <label>Label Name</label>
        <label><input type="text" name="newlabel" placeholder="New Label" onchange="checkLabelAvailability(this.value)" style="width:250px;padding:3px; margin-top:10px;"/></label><label name="username" id="username" value=""></label><br/><br/>
        <label><input type="hidden" value="<%= request.getParameter("locker_id")%>" name="lock_id"><input type="submit" value="+Add Label" class="clickaddbtn" style="margin-left:75px;"></label>
    </form>
</div>
</html>


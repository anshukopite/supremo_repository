<%@ page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@page import="java.sql.*,model.GetFileSize, java.io.File,java.text.DecimalFormat" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>User Home</title>
<link rel="stylesheet" type="text/css" href="css/loginstyle.css">
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" >
$(document).ready(function()
{
$(".account").click(function()
{
var X=$(this).attr('id');

if(X==1)
{
$(".submenu").hide();
$(this).attr('id', '0');	
}
else
{

$(".submenu").show();
$(this).attr('id', '1');
}
	
});

//Mouseup textarea false
$(".submenu").mouseup(function()
{
return false
});
$(".account").mouseup(function()
{
return false
});


//Textarea without editing.
$(document).mouseup(function()
{
$(".submenu").hide();
$(".account").attr('id', '');
});
	
});
	
	</script>
    <script type="text/javascript">
    window.history.forward();
    function noBack() { window.history.forward(); }
</script>
<script src="js/jquery-1.4.3.min.js" type="text/javascript"></script>
<script src="js/jquery-1.8.0.min.js" type="text/javascript"></script>
<script src="js/thickbox.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="css/userprefence.css"/>
</head>
<%
        response.setHeader("Cache-Control", "no-cache"); //Forces caches to obtain a new copy of the page from the origin server
        response.setHeader("Cache-Control", "no-store"); //Directs caches not to store the page under any circumstance
        response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
        response.setHeader("Pragma", "no-cache"); //HTTP 1.0 backward compatibility
        if (session.getAttribute("name") == null) {
            response.sendRedirect("index.jsp?status=session expired");

        }else{

    %>

<body onload="noBack();"
    onpageshow="if (event.persisted) noBack();" onunload="">
<!--Wrapper starts-->
<div class="wrapper">
	<!--Header starts-->
	<div class="header">
		<!--Logoarea starts-->
		<div class="logoarea">
			<!--Logo starts-->
			<div class="logo">
				<a href="home.jsp">
					<img src="images/logo.png" alt="Logo " />
				</a>
			</div>
			<!--Logo Ends-->
			<!--Loginarea Starts-->
			<div class="loginarea">
                 <div class="inbox" style="width:60px; float:left;">
                    <a href="inbox.jsp" style="color:#fff; font-weight:bold;">Inbox</a>
                </div>
				<div class="loginname">
					<h1>
						Logged In As:
							<span><%= session.getAttribute("name")%></span>
					</h1>
				</div>
				<div class="logout">
					<a href="logout.jsp">LOGOUT</a>
				</div>	
			</div>
			<!--Loginarea Ends-->
		</div>
		<!--Logoarea Ends-->
	</div>
	<!--Header ends-->
	<!--Main Container Starts-->
	<div class="maincontainer">
		<!--Left Container Starts-->
		<div class="leftcontainer">
			<!--LeftMenu Starts-->
			<div class="leftmenu">
				<div class="myaccount">
					<label>My Account</label>
				</div>
				<ul>
					<li>
						<a href="myprofile.jsp?stat=view">My Profile</a>
					</li>
					<li>
						<a href="managespace.jsp">Manage Space</a>
					</li>
                    <li>
						<a href="managelabel.jsp">Manage Label</a>
					</li>
                    <li>
						<a href="userpreferences.jsp?width=400&height=300" class="thickbox" title="User Preference">User Preferences</a>
					</li>
					<li>
						<a href="changepassword.jsp">Change Password</a>
					</li>
					<li>
						<a href="askquestion.jsp">Ask A Question</a>
					</li>
                    <li>
						<a href="feedback.jsp">Feedback</a>
					</li>
				</ul>
			</div>
			<!--LeftMenu Ends-->
		</div>
		<!--Left Container Ends-->
		<!--Right Container Starts-->
		<div class="rightcontainer">
			<!--treestructure starts-->
			<div class="treestruct">
				<div class="treelist">
					<label>User Home</label>
				</div>
				<div class="searchbox">
					<div style="float:right;">
						<div class="dropdown">
							<a class="account" >
								<span>View By</span>
							</a>
							<div class="submenu" style="display: none; ">
								<ul class="root">
									<li>
									  <a href="home.jsp" >Thumbnail</a>
									</li>
									 <li>
									 	 <a href="homedetail.jsp">Detail</a>
	    							</li>
	   							</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<!--treestructure ends-->
			<!--Menu Starts-->
             <%if(session.getAttribute("user_pref").equals("C")){%>
			<div class="menu">		
				<ul>
					<li><a href="home.jsp" class="current">Home</a></li>
					<li><a href="repassword.jsp?locker_id=1">Docs</a></li>
					<li><a href="repassword.jsp?locker_id=2">Music</a></li>
					<li><a href="repassword.jsp?locker_id=3">Photos</a></li>
					<li><a href="repassword.jsp?locker_id=4">Videos</a></li>
					<li><a href="repassword.jsp?locker_id=5">Credentials</a></li>
					<li><a href="repassword.jsp?locker_id=6">Important Numbers</a></li>
				</ul>
			</div>
             <%}else{%>
            <div class="menu">
            <ul>
					<li><a href="home.jsp" class="current">Home</a></li>
					<li><a href="userhome.jsp?nextpage=1&label_id=0">Docs</a></li>
					<li><a href="usermusic.jsp?nextpage=1&label_id=0">Music</a></li>
					<li><a href="userphoto.jsp?nextpage=1&label_id=0">Photos</a></li>
					<li><a href="uservideo.jsp?nextpage=1&label_id=0">Videos</a></li>
					<li><a href="usercredential.jsp?nextpage=1&label_id=0">Credentials</a></li>
					<li><a href="userimportantnumber.jsp?nextpage=1&label_id=0">Important Numbers</a></li>
            </ul>
            </div>
            <%}%>
			<!--Menu Ends-->
			
			<!--maincontent starts-->
			<div class="main">
				<!--mainarea starts-->
				<div class="mainarea">
					<form action="" method="get">
						<table class="unborder">
							<tr>
								<td>
                                    <%if(session.getAttribute("user_pref").equals("C")){%>
									<img src="images/img4.2.png"title="Docs" alt="Doc image"/>
                                    <label><a href="repassword.jsp?locker_id=1">Docs</a></label>
                                     <%}else{%>
                                     <img src="images/img4.2.png"title="Docs" alt="Doc image"/>
                                    <label><a href="userhome.jsp?nextpage=1&label_id=0">Docs</a></label>
                                    <%}%>
								</td>
								
								<td>
									<label>
										Total Size: <span><%
                                                             try{
                                                            double DocSize=0.00;
                                                            int fileCountDoc=0;
                                                            String sizeLabel="KB";
                                                            long DocSizeTemp;
                                                            GetFileSize size=new GetFileSize();
                                                            ResultSet rs=size.GetLockerFileSize((Integer)session.getAttribute("uid"),1);
                                                            while(rs.next()){
                                                                    File file= new File(getServletContext().getRealPath("/")+rs.getString(1));
                                                                    if(file.exists()){
                                                                        DocSizeTemp=file.length();
                                                                         DocSize=DocSize+((double) DocSizeTemp)/1024.00;
                                                                         fileCountDoc=size.GetFileCount(getServletContext().getRealPath("/")+rs.getString(1));
                                                                         System.out.println("MyCountForDoc="+fileCountDoc);

                                                                    }
                                                                    }
                                                                    if(DocSize>=1024.00 && DocSize<1048576.00){

                                                        DocSize=DocSize/1024.00;

                                                        sizeLabel="MB";
                                                            }
                                                            if(DocSize>=1048576.00){
                                                                    DocSize=DocSize/1024.00;
                                                                    sizeLabel="GB";

                                                             }

                                                        DecimalFormat df = new DecimalFormat("#.##");
                                                        DocSize=Double.parseDouble(df.format(DocSize));
                                                        out.print(DocSize+" "+sizeLabel);


                                                            




                                        %></span>
									</label>
								</td>
                                <td>
									<label>
										Total Files:<span><%=fileCountDoc%></span>
                                        <%
                                         }catch(Exception e){
                                                            e.printStackTrace();
                                                         }
                                        %>
									</label>
								</td>
							</tr>
							<tr>
								<td>
                                    <%if(session.getAttribute("user_pref").equals("C")){%>
									<img src="images/img5.2.png" title="Music" alt="Music image"/>
                                    <label><a href="repassword.jsp?locker_id=2">Music</a></label>
                                     <%}else{%>
                                     <img src="images/img5.2.png" title="Music" alt="Music image"/>
                                    <label><a href="usermusic.jsp?nextpage=1&label_id=0">Music</a></label>
                                    <%}%>
								</td>
								
								<td>
									<label>
										Total Size: <span><%
                                                          try{
                                                               double MusicSize=0.00;
                                                            String sizeLabel1="KB";
                                                            long MusicSizeTemp;
                                                            int fileCount1=0;
                                                            GetFileSize size1=new GetFileSize();
                                                            ResultSet rs1=size1.GetLockerFileSize((Integer)session.getAttribute("uid"),2);
                                                            while(rs1.next()){
                                                                    File file= new File(getServletContext().getRealPath("/")+rs1.getString(1));
                                                                    
                                                                    if(file.exists()){
                                                                        MusicSizeTemp=file.length();
                                                                         MusicSize=MusicSize+((double) MusicSizeTemp)/1024.00;
                                                                         fileCount1=size1.GetFileCount(getServletContext().getRealPath("/")+rs1.getString(1));
                                                                         System.out.println("MyCountForMusic="+fileCount1);
                                                                    }
                                                                    }
                                                                    if(MusicSize>=1024.00 && MusicSize<1048576.00){

                                                        MusicSize=MusicSize/1024.00;

                                                        sizeLabel1="MB";
                                                            }
                                                            if(MusicSize>=1048576.00){
                                                                    MusicSize=MusicSize/1024.00;
                                                                    sizeLabel1="GB";

                                                             }

                                                        DecimalFormat df1 = new DecimalFormat("#.##");
                                                        MusicSize=Double.parseDouble(df1.format(MusicSize));
                                                        out.print(MusicSize+" "+sizeLabel1);

                                        %></span>
									</label>
								</td>
                                <td>
									<label>
										Total Files:<span><%=fileCount1%></span>
                                        <%
                                         }catch(Exception e){
                                                            e.printStackTrace();
                                                         }
                                        %>
									</label>
								</td>
							</tr>
							<tr>
								<td>
                                    <%if(session.getAttribute("user_pref").equals("C")){%>
									<img src="images/img1.2.png" title="Photos" alt="Photos image"/>
                                    <label><a href="repassword.jsp?locker_id=3">Photos</a></label>
                                    <%}else{%>
                                    <img src="images/img1.2.png" title="Photos" alt="Photos image"/>
                                    <label><a href="userphoto.jsp?nextpage=1&label_id=0">Photos</a></label>
                                    <%}%>
								</td>
								<td>
									<label>
										Total Size: <span><%
                                                          try{
                                                               double PhotoSize=0.00;
                                                            String PhotosizeLabel="KB";
                                                            long PhotoSizeTemp;
                                                            int PhotoFileCount=0;
                                                            GetFileSize size2=new GetFileSize();
                                                            ResultSet rs2=size2.GetLockerFileSize((Integer)session.getAttribute("uid"),3);
                                                            while(rs2.next()){
                                                                    File file= new File(getServletContext().getRealPath("/")+rs2.getString(1));

                                                                    if(file.exists()){
                                                                        PhotoSizeTemp=file.length();
                                                                         PhotoSize=PhotoSize+((double) PhotoSizeTemp)/1024.00;
                                                                         PhotoFileCount=size2.GetFileCount(getServletContext().getRealPath("/")+rs2.getString(1));
                                                                         System.out.println("MyCountForMusic="+PhotoFileCount);
                                                                    }
                                                                    }
                                                                    if(PhotoSize>=1024.00 && PhotoSize<1048576.00){

                                                        PhotoSize=PhotoSize/1024.00;

                                                        PhotosizeLabel="MB";
                                                            }
                                                            if(PhotoSize>=1048576.00){
                                                                    PhotoSize=PhotoSize/1024.00;
                                                                    PhotosizeLabel="GB";

                                                             }

                                                        DecimalFormat df2 = new DecimalFormat("#.##");
                                                        PhotoSize=Double.parseDouble(df2.format(PhotoSize));
                                                        out.print(PhotoSize+" "+PhotosizeLabel);

                                        %></span>
									</label>
								</td>
                                <td>
									<label>
										Total Files:<span><%=PhotoFileCount%></span>
                                        <%
                                         }catch(Exception e){
                                                            e.printStackTrace();
                                                         }
                                        %>
									</label>
								</td>
							</tr>
							<tr>
								<td>
                                    <%if(session.getAttribute("user_pref").equals("C")){%>
									<img src="images/img6.2.png" title="Videos" alt="Video image"/>
                                    <label><a href="repassword.jsp?locker_id=4">Videos</a></label>
                                    <%}else{%>
                                    <img src="images/img6.2.png" title="Videos" alt="Video image"/>
                                    <label><a href="uservideo.jsp?nextpage=1&label_id=0">Videos</a></label>
                                    <%}%>
								</td>
								<td>
									<label>
										Total Size: <span><%
                                                          try{
                                                               double VideoSize=0.00;
                                                            String VideoLabel="KB";
                                                            long VideoSizeTemp;
                                                            int VideoFileCount=0;
                                                            GetFileSize size3=new GetFileSize();
                                                            ResultSet rs3=size3.GetLockerFileSize((Integer)session.getAttribute("uid"),4);
                                                            while(rs3.next()){
                                                                    File file= new File(getServletContext().getRealPath("/")+rs3.getString(1));

                                                                    if(file.exists()){
                                                                        VideoSizeTemp=file.length();
                                                                         VideoSize=VideoSize+((double) VideoSizeTemp)/1024.00;
                                                                         VideoFileCount=size3.GetFileCount(getServletContext().getRealPath("/")+rs3.getString(1));

                                                                    }
                                                                    }
                                                                    if(VideoSize>=1024.00 && VideoSize<1048576.00){

                                                        VideoSize=VideoSize/1024.00;

                                                        VideoLabel="MB";
                                                            }
                                                            if(VideoSize>=1048576.00){
                                                                    VideoSize=VideoSize/1024.00;
                                                                    VideoLabel="GB";

                                                             }

                                                        DecimalFormat df3 = new DecimalFormat("#.##");
                                                        VideoSize=Double.parseDouble(df3.format(VideoSize));
                                                        out.print(VideoSize+" "+VideoLabel);

                                        %></span>
									</label>
								</td>
                                <td>
									<label>
										Total Files:<span><%=VideoFileCount%></span>
                                        <%
                                         }catch(Exception e){
                                                            e.printStackTrace();
                                                         }
                                        %>
									</label>
								</td>
							</tr>
							<tr>
								<td>
                                    <%if(session.getAttribute("user_pref").equals("C")){%>
									<img src="images/img2.1.png" title="Credentials" alt="Credentials image"/>
									<label><a href="repassword.jsp?locker_id=5">Credentials</a></label>
                                    <%}else{%>
                                    <img src="images/img2.1.png" title="Credentials" alt="Credentials image"/>
									<label><a href="usercredential.jsp?nextpage=1&label_id=0">Credentials</a></label>
                                    <%}%>
								</td>
                                <td>
									<label>
										<span>Not Available</span>
									</label>
								</td>
								<td>
									<label>
										<span>Not Available</span>
									</label>
								</td>
								
							</tr>
							<tr>
								<td>
                                    <%if(session.getAttribute("user_pref").equals("C")){%>
									<img src="images/img3.1.png" title="Important Numbers" alt="Important Numbers image"/>
                                    <label><a href="repassword.jsp?locker_id=6">Important Numbers</a></label>
                                    <%}else{%>
                                    <img src="images/img3.1.png" title="Important Numbers" alt="Important Numbers image"/>
                                    <label><a href="userimportantnumber.jsp?nextpage=1&label_id=0">Important Numbers</a></label>
                                    <%}%>
								</td>
								<td>
									<label>
										<span>Not Available</span>
									</label>
								</td>
								<td>
									<label>
										<span>Not Available</span>
									</label>
								</td>
							</tr>
						</table>
					</form>
				</div>
				<!--mainarea ends-->
			</div>
			<!--maincontent ends-->
		</div>
		<!--Right Container Ends-->	
	</div>
	<!--Main Container Ends-->
	<!--Footer starts-->
	<div class="footer">
		<div class="footerleft">
			<p>
				&copy; All Copyrights reserved. <a href="#">www.bub.com</a>
			</p>
		</div>
		<div class="footerright">
			<ul>
				<li><a href="home.jsp">Home</a> &nbsp; &nbsp;</li>
			<!--	<li><a href="about.jsp">About Us</a> &nbsp;| &nbsp;</li>
				<li><a href="privacy.jsp">Privacy</a> &nbsp;| &nbsp;</li>
				<li><a href="terms.jsp">Terms</a> &nbsp;| &nbsp;</li>
				<li><a href="contact.jsp">Contact Us</a></li>-->

			</ul>
		</div>
	</div>
	<!--Footer Ends-->
</div>
<!--Wrapper ends-->
</body>
</html>
<%}%>
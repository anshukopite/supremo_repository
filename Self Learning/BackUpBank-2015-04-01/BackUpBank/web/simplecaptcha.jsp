<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>

    <head>

        <META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

        <title>Simple CAPTCHA Example</title>

        <link href="captchacss.css" type="text/css" rel="stylesheet" />

    </head>
    <%response.setHeader("Cache-Control", "no-cache"); //Forces caches to obtain a new copy of the page from the origin server
        response.setHeader("Cache-Control", "no-store"); //Directs caches not to store the page under any circumstance
        response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
        response.setHeader("Pragma", "no-cache"); //HTTP 1.0 backward compatibility
        if (session.getAttribute("name") == null) {
            response.sendRedirect("index.jsp?status=session expired");
        }

    %>
    <body>

        <center>

            <h3>Simple CAPTCHA Example</h3>

            <img id="captcha" src="<c:url value="simpleCaptcha.jpg" />" width="150">

            <form action="captchasubmit.jsp" method="post">
                <input type="text" name="answer" /><br>

            <input type="submit" value="Submit"></form>

        </center>

    </body>

</html>
<%@ page  import="java.io.FileInputStream" %>
<%@ page  import="java.io.BufferedInputStream"  %>
<%@ page  import="java.io.File"  %>
<%@ page import="java.io.IOException,connection.DBConnection,java.sql.ResultSet,java.sql.Statement,java.sql.Connection" %>
<%response.setHeader("Cache-Control", "no-cache"); //Forces caches to obtain a new copy of the page from the origin server
        response.setHeader("Cache-Control", "no-store"); //Directs caches not to store the page under any circumstance
        response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
        response.setHeader("Pragma", "no-cache"); //HTTP 1.0 backward compatibility
        if (session.getAttribute("name") == null) {
            response.sendRedirect("index.jsp?status=session expired");
        } else {
%>

<%

            // you  can get your base and parent from the database
            String base = "e1";
            String parent = "e2";
            // String filename=parent+"_codemiles.zip";
// you can  write http://localhost
            // String filepath="http://www.codemiles.com/example/"+base+"/";
            Connection con = null;
            try {
                String dbPath = "";
                String fileName = "";
                String pathssss = request.getSession().getServletContext().getRealPath("/");
                String fileId = request.getParameter("id");
                con = DBConnection.getConnection();
                Statement st = con.createStatement();
                ResultSet rs = st.executeQuery("select locker_content_path,locker_content_name,locker_content_extension from locker_content_tbl where locker_content_id=" + fileId);
                if (rs.next()) {

                    dbPath = rs.getString(1);
                    fileName = rs.getString(2) + rs.getString(3);
                    String ourPath = pathssss + dbPath;
                    System.out.println(ourPath);
                    BufferedInputStream buf = null;
                    ServletOutputStream myOut = null;

                    try {

                        myOut = response.getOutputStream();
                        //File myfile = new File(filepath+filename);

                        File myfile = new File(ourPath);
                        //set response headers
                        response.setContentType("multipart/mixed");

                        response.addHeader(
                                "Content-Disposition", "attachment; fileName=" + fileName);

                        response.setContentLength((int) myfile.length());

                        FileInputStream input = new FileInputStream(myfile);
                        buf = new BufferedInputStream(input);
                        int readBytes = 0;

                        //read from the file; write to the ServletOutputStream
                        while ((readBytes = buf.read()) != -1) {
                            myOut.write(readBytes);
                        }

                    } catch (IOException ioe) {

                        throw new ServletException(ioe.getMessage());

                    } finally {

                        //close the input/output streams
                        if (myOut != null) {
                            myOut.close();
                        }
                        if (buf != null) {
                            buf.close();
                        }

                    }
                } else {
                    response.sendRedirect("userhomepage.jsp?msg=Download Failed");
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
%>
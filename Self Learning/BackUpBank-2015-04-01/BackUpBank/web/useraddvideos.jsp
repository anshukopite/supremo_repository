<%-- 
    Document   : useraddvideos
    Created on : Oct 1, 2012, 7:48:13 PM
    Author     : gopal
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.util.ArrayList,model.GetDocInfo,model.GetLabelInfo,java.sql.ResultSet;"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>Add Video</title>        
        <link rel="stylesheet" type="text/css" href="css/loginstyle.css">
        <link rel="stylesheet" type="text/css" href="css/thickbox.css">
        <script>
            function CheckFileName(){

                var file=document.forms.ValidateUpload.docfile.value;
                var fileName=document.forms.ValidateUpload.docFileName.value;
                var fileDesc=document.forms.ValidateUpload.docFileDesc.value;
                var fileLength=file.length;
                var lastDotPos=file.lastIndexOf(".");
                var fileExt=file.substr(lastDotPos+1,fileLength);


                if(file==""){

                    alert("Please select a video file to upload");
                    return false;
                }
                if(fileName==""){

                    alert("Please specify the file name");
                    return false;
                }
                if(fileExt.toUpperCase()== "FLV" || fileExt.toUpperCase()== "WMV" || fileExt.toUpperCase()== "MPG" || fileExt.toUpperCase()== "DAT"){

                  //  return true;
                }

                else{
                    alert("."+fileExt+" is not a valid video file\nPlease select a video type file to upload");
                    return false;
                }
                  if(fileDesc==""||fileDesc==null||fileDesc.trim()==" "){
                    var tbLen = fileDesc.trim().length;
                            if (tbLen == 0)
                           {
                    return confirm ('Are you sure you want this Description to be saved as empty?');
                         }

                }
                return true;


            }
        </script>
        <script type="text/javascript">
    window.history.forward();
    function noBack() {
        window.history.forward(); }
</script>
<script src="js/jquery-1.8.0.min.js" type="text/javascript"></script>
<script src="js/thickbox.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="css/userprefence.css"/>
    </head>
    <body onload="noBack();"
    onpageshow="if (event.persisted) noBack();" onunload="" >
        <%response.setHeader("Cache-Control", "no-cache"); //Forces caches to obtain a new copy of the page from the origin server
        response.setHeader("Cache-Control", "no-store"); //Directs caches not to store the page under any circumstance
        response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
        response.setHeader("Pragma", "no-cache"); //HTTP 1.0 backward compatibility
        if (session.getAttribute("name") == null) {
            response.sendRedirect("index.jsp?status=session expired");
        }

        %>
        <!--Wrapper starts-->
        <div class="wrapper">
            <!--Header starts-->
            <div class="header">
                <!--Logoarea starts-->
                <div class="logoarea">
                    <!--Logo starts-->
                    <div class="logo">
                        <a href="home.jsp">
                            <img src="images/logo.png" />
                        </a>
                    </div>
                    <!--Logo Ends-->
            <!--Loginarea Starts-->
                    <div class="loginarea">
                         <div class="inbox" style="width:60px; float:left;">
                    <a href="inbox.jsp" style="color:#fff; font-weight:bold;">Inbox</a>
                </div>
                        <div class="loginname">
                            <h1>
                                Logged In As:
                                <span><%= session.getAttribute("name")%></span>
                            </h1>
                        </div>
                        <div class="logout">
                            <a href="logout.jsp">LOGOUT</a>
                        </div>
                    </div>
                    <!--Loginarea Ends-->
                </div>
                <!--Logoarea Ends-->
            </div>
            <!--Header ends-->
    <!--Main Container Starts-->
            <div class="maincontainer">
                <!--Left Container Starts-->
                <div class="leftcontainer">
                    <!--LeftMenu Starts-->
                    <div class="leftmenu">
                        <div class="myaccount">
                             <label>My Account</label>
                        </div>
                        <ul>
					<li>
						<a href="myprofile.jsp?stat=view">My Profile</a>
					</li>
					<li>
						<a href="managespace.jsp">Manage Space</a>
					</li>
                    <li>
						<a href="managelabel.jsp">Manage Label</a>
					</li>
                    <li>
						<a href="userpreferences.jsp?width=400&height=300" class="thickbox" title="User Preference">User Preferences</a>
					</li>
					<li>
						<a href="changepassword.jsp">Change Password</a>
					</li>
					<li>
						<a href="askquestion.jsp">Ask A Question</a>
					</li>
                    <li>
						<a href="feedback.jsp">Feedback</a>
					</li>
				</ul>
                    </div>
                    <!--LeftMenu Ends-->
                </div>
                <!--Left Container Ends-->
        <!--Right Container Starts-->
                <div class="rightcontainer">
                    <!--Menu Starts-->
                    <%if(session.getAttribute("user_pref").equals("C")){%>
                     <div class="menu">
                        <ul>
                            <li><a href="home.jsp">Home</a></li>
                            <li><a href="repassword.jsp?locker_id=1">Docs</a></li>
                            <li><a href="repassword.jsp?locker_id=2" >Music</a></li>
                            <li><a href="repassword.jsp?locker_id=3" >Photos</a></li>
                            <li><a href="uservideo.jsp" class="current">Videos</a></li>
                            <li><a href="repassword.jsp?locker_id=5">Credentials</a></li>
                            <li><a href="repassword.jsp?locker_id=6">Important Numbers</a></li>
                        </ul>
                    </div>
                     <%}else{%>
            <div class="menu">
            <ul>
					<li><a href="home.jsp" >Home</a></li>
					<li><a href="userhome.jsp?nextpage=1&label_id=0">Docs</a></li>
					<li><a href="usermusic.jsp?nextpage=1&label_id=0">Music</a></li>
					<li><a href="userphoto.jsp?nextpage=1&label_id=0">Photos</a></li>
					<li><a href="uservideo.jsp?nextpage=1&label_id=0" class="current">Videos</a></li>
					<li><a href="usercredential.jsp?nextpage=1&label_id=0">Credentials</a></li>
					<li><a href="userimportantnumber.jsp?nextpage=1&label_id=0">Important Numbers</a></li>
            </ul>
            </div>
            <%}%>
                    <!--Menu Ends-->
            <!--maincontent starts-->
                    <div class="main">
                        <!--mainarea starts-->
                        <div class="mainarea">
                            <div class="heading">
                                <h2>Add Documents</h2>
                            </div>
                            <div class="documententry">

                                <form action="FileUpload" method="post" onsubmit="return CheckFileName()" name="ValidateUpload" enctype="multipart/form-data">
                                    <table class="bordered border">
                                        <%
        ResultSet rs = null;
         String emsg=emsg=request.getParameter("emsg");
        String fmsg = request.getParameter("fmsg");
        String msg=request.getParameter("msg");
      

        try {
            int userId = (Integer)session.getAttribute("uid");
            int lockerId = 4;
            GetLabelInfo lb = new GetLabelInfo();
            rs = lb.getLabels(lockerId, userId);
                                        %>
                                        <tr>
                                               <%if(emsg!=null){%>
                                               <td>

                                               </td>

                                                <td>
                                                   <span style="color:red"><%=emsg%></span>
                                               </td>
                                               <%}%>
                                               <%if(fmsg!=null){%>
                                               <td>

                                               </td>

                                                <td>
                                                   <span style="color:red"><%=fmsg%></span>
                                               </td>
                                               <%}%>
                                                <%if(msg!=null){%>
                                               <td>

                                               </td>

                                                <td>
                                                   <span style="color:green"><%=msg%></span>
                                               </td>
                                               <%}%>

                                           </tr>
                                        <tr>
                                            <td>
                                                <label>Select File:</label>
                                            </td>
                                            <td>
                                                <input type="file" name="docfile"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>

                                                <label>Select Label:</label>
                                            </td>
                                            <td><select name="docLabelId">
                                                    <%
                                            while (rs.next()) {
                                                    %>
                                                    <option value="<%= rs.getString(1)%>"><%= rs.getString(2)%></option>
                                                    <%}%>
                                                </select>
                                                <%

        } catch (Exception e) {
            e.printStackTrace();
        }

                                                %>
                                            <form action=""> <label id="txtHint1"><a onclick="" href="createfolder.jsp?locker_id=4" title="Create Label" class="thickbox">Create label </a></label></form> </td>

                                        </tr>
                                        <tr>
                                            <td>
                                                <label>File Name:</label>
                                            </td>
                                            <td>
                                                <input type="text" name="docFileName" accept="video/*" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label>File Description:</label>
                                            </td>
                                            <td>
                                                <textarea name="docFileDesc"></textarea>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><input type="hidden" name="DocLockerId" value="4"></td>
                                            <td>
                                                <input type="submit" value="Submit" class="clickaddbtn" />
                                            </td>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                        </div>
                        <!--mainarea ends-->
                        
                    </div>
                    <!--maincontent ends-->
                </div>
                <!--Right Container Ends-->
            </div>
            <!--Main Container Ends-->
            <!--Footer starts-->
           <div class="footer">
                <div class="footerleft">
                    <p>
                        &copy; All Copyrights reserved. <a href="#">www.bub.com</a>
                    </p>
                </div>
                <div class="footerright">
			<ul>
				<li><a href="home.jsp">Home</a> &nbsp; &nbsp;</li>
			<!--	<li><a href="about.jsp">About Us</a> &nbsp;| &nbsp;</li>
				<li><a href="privacy.jsp">Privacy</a> &nbsp;| &nbsp;</li>
				<li><a href="terms.jsp">Terms</a> &nbsp;| &nbsp;</li>
				<li><a href="contact.jsp">Contact Us</a></li>-->

			</ul>
		</div>
            </div>
            <!--Footer Ends-->
        </div>
        <!--Wrapper ends-->
    </body>
</html>

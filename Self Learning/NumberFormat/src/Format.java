import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;


public class Format {

	public static void main (String[] args){
		
		//String str = "17.0230";
		NumberFormat formate = new DecimalFormat("##.00");
	    //BigDecimal big = new BigDecimal(str);
	    //System.out.println("BigDecimal "+big.round(new MathContext(2, RoundingMode.CEILING)));
	    //big = big.round(new MathContext(2, RoundingMode.UP));
	    //System.out.println(formate.format(big));
	    
	    Double num = 13.5678945;
	    Double num2 = new Double(135.365487);
	    Double num3 = new Double("132.56487");
	    
	    System.out.println(formate.format(num));
	    System.out.println(formate.format(num2));
	    System.out.println(formate.format(num3));
	    
	}
	
	
	
	
}

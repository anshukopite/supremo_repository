
public class Polymorphism {

	public static void main (String[] args)
	{

		ExtendingStaticCheck b = new ExtendingStaticCheck();
		StaticCheck a = new ExtendingStaticCheck();
		
		b.print();
		a.print();
		
	}
	
	
}

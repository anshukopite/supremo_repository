package com.anshu.polymorphism;

import com.anshu.customexception.CustomExceptionA;

public class B extends A {

	public void print() throws CustomExceptionA{
		System.out.println("In Class B");
	}
}

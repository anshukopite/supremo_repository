package com.anshu.polymorphism;

import com.anshu.customexception.CustomExceptionB;

public class A {

	public void print() throws CustomExceptionB{
		
		System.out.println("In Class A");
	}			
}

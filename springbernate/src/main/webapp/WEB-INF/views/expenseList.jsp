<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<div >
		<table>
			<tr>
				<th>Id</th>
				<th>Expenses</th>
				<th>Price</th>
			</tr>
			<c:forEach var="expense"  items="${expenses}">
				<tr>
					<td><label>${expense.id}</label></td>
					<td><label>${expense.expenseName}</label></td>
					<td><label>${expense.price}</label></td>
				</tr>
			</c:forEach>		
		</table>
		
		<a  href="<%=request.getContextPath()%>/addExpenses"  >Add More Expenses</a>
	</div>
</body>
</html>
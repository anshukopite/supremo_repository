<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Home</title>
</head>
<body>
	
	<form action="<%=request.getContextPath()%>/addExpenses" method="post">
		<table>
			<tr>
				<th>Id</th>
				<th>Expenses</th>
				<th>Price</th>
			</tr>		
			<tr>
				<td>Label</td>
				<td><input type="text" name="expense"/></td>
				<td><input type="text" name="price"/></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td><input type="submit" value="Add Expense"/></td>
			</tr>
		</table>	
	</form>
	
	
	<a  href="<%=request.getContextPath()%>/getExpenseList">Get Expenses</a>
	
</body>
</html>
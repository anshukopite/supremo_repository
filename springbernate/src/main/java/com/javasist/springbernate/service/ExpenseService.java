package com.javasist.springbernate.service;

import java.util.List;

import com.javasist.springbernate.entity.Expense;

public interface ExpenseService {

	void saveExpense(Expense exp);
	
	List<Expense> getExpenses();
	
	
}

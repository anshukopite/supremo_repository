package com.javasist.springbernate.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.javasist.springbernate.dao.ExpenseDao;
import com.javasist.springbernate.entity.Expense;

@Service
@Transactional
public class ExpenseServiceImpl implements ExpenseService {

	@Autowired
	private ExpenseDao expDao;
	
	public void saveExpense(Expense exp) {
		expDao.saveExpense(exp);
	}

	public List<Expense> getExpenses() {
		return expDao.getExpenses();
	}

}

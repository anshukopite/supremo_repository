package com.javasist.springbernate.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.javasist.springbernate.entity.Expense;
import com.javasist.springbernate.service.ExpenseService;

@Controller
@RequestMapping("/")
public class RequestController {

	@Autowired
	private ExpenseService expService;
	
	@RequestMapping("/")
	public String index() {
		return "index";
	}
	
	@RequestMapping("/landing")
	public String landingPage(ModelMap model) {
		System.out.println("Inside Landing");
		model.addAttribute("test","This is index page!!");		
		return "home";
	}
	
	@RequestMapping(value = "/addExpenses")
	public String addCustomers(
			HttpServletResponse response) {
				
		return "home";
	}
	
	@RequestMapping(value = "/addExpenses",method = RequestMethod.POST)
	public String addCustomers(HttpServletRequest request,HttpServletResponse response) {
		
		ModelAndView model = new ModelAndView();
		model.setViewName("expenses");
		String expense = request.getParameter("expense");
		String price = request.getParameter("price");
		Expense exp = new Expense(); 
		exp.setExpenseName(expense);
		exp.setPrice(Integer.parseInt(price));
		System.out.println("Inside AddExpenses!!");
		expService.saveExpense(exp);
		
		return "home";
	}
	
	@RequestMapping(value="/getExpenseList",method= RequestMethod.GET)
	public ModelAndView expenseList() {
		
		ModelAndView model = new ModelAndView("expenseList");
		List<Expense> expenses = expService.getExpenses();
		model.addObject("expenses",expenses);
		return model;
	}
	
	
	
}

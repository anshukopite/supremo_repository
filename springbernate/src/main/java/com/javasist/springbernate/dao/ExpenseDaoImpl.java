package com.javasist.springbernate.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.springframework.stereotype.Repository;

import com.javasist.springbernate.entity.Expense;

@Repository
public class ExpenseDaoImpl extends AbstractDAO implements ExpenseDao {

	

	public void saveExpense(Expense exp) {
		persist(exp);
	}

	@SuppressWarnings("unchecked")
	public List<Expense> getExpenses() {
		Criteria criteria = getSession().createCriteria(Expense.class);
		((List<Expense>)criteria.list()).forEach(p -> System.out.println("Id :"+p.getId()));
		return (List<Expense>)criteria.list();
	}

}

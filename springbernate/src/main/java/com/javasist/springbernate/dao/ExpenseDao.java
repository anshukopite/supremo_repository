package com.javasist.springbernate.dao;

import java.util.List;

import com.javasist.springbernate.entity.Expense;

public interface ExpenseDao {

	void saveExpense(Expense exp);
	List<Expense> getExpenses();
	
}

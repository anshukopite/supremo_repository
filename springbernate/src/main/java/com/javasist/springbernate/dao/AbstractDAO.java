package com.javasist.springbernate.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class AbstractDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	public Session getSession() {
		System.out.println("Session ");
		return sessionFactory.getCurrentSession();
	}
	
	public void persist(Object obj) {
		System.out.println("Saving...");
		getSession().persist(obj);
	}
	
	public void delete(Object obj) {
		getSession().delete(obj);
	}
	
}

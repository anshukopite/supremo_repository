package com.pretech;

import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate3.HibernateTemplate;

public class StudentDao {
	HibernateTemplate template;

	public void setSessionFactory(SessionFactory factory) {
		template = new HibernateTemplate(factory);
	}

	public void saveStudent(Student e) {
		template.save(e);
	}

}

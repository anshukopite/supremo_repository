package com.pretech;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

public class StudentMain {
	public static void main(String[] args) {

		Resource resource = new ClassPathResource("applicationContext.xml");
		BeanFactory factory = new XmlBeanFactory(resource);

		StudentDao dao = (StudentDao) factory.getBean("d");
		Student student = new Student();
		student.setId(101);
		student.setName("Vinod");
		dao.saveStudent(student);

	}
}

package com.test.springapp.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.test.springapp.pojo.user_tbl;

public class RedirectController extends SimpleFormController {

	ModelAndView model;
	@Override
	protected ModelAndView onSubmit(HttpServletRequest arg0,
			HttpServletResponse arg1,Object command,BindException error) throws Exception {
		
			System.out.println("In switch case");
			model = new ModelAndView(new RedirectView(getSuccessView()));
			model.addObject("msg","Redirect Successful!!");
			return model;
			
	}
	protected Object formBackingObject(HttpServletRequest request){
		
		System.out.println("In form Backing");
		return new user_tbl();
		
		
	}
}

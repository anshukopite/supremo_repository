package com.test.springapp.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

public class LogoutController extends AbstractController {

	@Override
	protected ModelAndView handleRequestInternal(HttpServletRequest req,
			HttpServletResponse arg1) throws Exception {
		DoLogin check = new DoLogin();
		ModelAndView model = new ModelAndView("logout");
		System.out.println(check.toString());
		
		model.addObject("check",check);
		return model;
		
		}
		
	}


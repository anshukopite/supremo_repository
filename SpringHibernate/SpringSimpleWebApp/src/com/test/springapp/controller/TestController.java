package com.test.springapp.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

@Controller
public class TestController extends AbstractController {
		
	@Override
	protected ModelAndView handleRequestInternal(HttpServletRequest req,
			HttpServletResponse arg1) throws Exception {
		// TODO Auto-generated method stub
		
		System.out.println("In TestController");
		ModelAndView model = new ModelAndView("/login.do");
		System.out.println("In TestController");
		model.addObject("message", "Welcome Guest!!");
		
		return model;
	}
	
}
package com.test.springapp.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.test.springapp.dao.EmployeeDao;
import com.test.springapp.dao.updateDAO;
import com.test.springapp.pojo.Employee;

public class UpdateController extends SimpleFormController{

	protected  ModelAndView onSubmit(HttpServletRequest request,
			HttpServletResponse response, Object command,
			BindException bindException){
		
		ModelAndView model = new ModelAndView(new RedirectView(getSuccessView()));
		Employee emp = (Employee) command;		
		System.out.println("Employee Name "+emp.getFirstName());
		System.out.println("Employee LastName "+emp.getLastName());
		System.out.println("Employee ID "+emp.getId());
		boolean redirect = new updateDAO().updateEmployee(emp.getId(),emp.getFirstName(),emp.getLastName(),emp.getEmailId(),emp.getGender());
		
		if(redirect){
			model.addObject("msg","Update Successful!!");			
			return model;
		}else{
			model = new ModelAndView(new RedirectView("update.do?id="+emp.getId()));
			model.addObject("emsg", "Update Failed");
		}		
		return model;
	}
	
	protected Map referenceData(HttpServletRequest request, Object command, Errors errors) {
		
		List<Object> empList = new EmployeeDao().getListById(Integer.parseInt((request.getParameter("id"))));
		Iterator<Object> itr = empList.iterator();
		Map<String,Object> empMap = new HashMap<String,Object>();
		empMap.put("empMap", empList);
		
		return empMap;
	}
	protected Object formBackingObject(HttpServletRequest request){
		
		return new Employee();
		
	}
	
	
}

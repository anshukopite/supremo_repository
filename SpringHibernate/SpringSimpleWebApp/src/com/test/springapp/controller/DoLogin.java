package com.test.springapp.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.test.springapp.dao.user_tblDAO;
import com.test.springapp.pojo.user_tbl;



public class DoLogin extends SimpleFormController{
	
	protected ModelAndView onSubmit(HttpServletRequest request,
			HttpServletResponse response, Object command,
			BindException bindException){
		ModelAndView model = new ModelAndView(new RedirectView("login.do"));
		  user_tbl user  = (user_tbl) command;
		
		  System.out.println("username:"+ user.getUsername());
		  System.out.println("password:"+user.getPwd());
		  
		System.out.println("In OnSubmit");		
		boolean redirect = new user_tblDAO().findUser(user.getUsername(), user.getPwd());	
		if(redirect){
			System.out.println("Redirect "+redirect);
			model  = new ModelAndView(new RedirectView(getSuccessView()));
			model.addObject("msg","Login Successful!!");
			return model;
		}else{
			
			return model;
		}			
	
	}
	
	
	
	protected Object formBackingObject(HttpServletRequest request)
			throws Exception {
		System.out.println("Form backing Object");
		return new user_tbl();
	}
		
		
		
	
	

}

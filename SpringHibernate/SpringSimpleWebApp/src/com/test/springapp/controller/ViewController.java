package com.test.springapp.controller;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.map.HashedMap;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.test.springapp.dao.EmployeeDao;
import com.test.springapp.pojo.Employee;

public class ViewController extends SimpleFormController {

	
	

	@Override
	protected Map referenceData(HttpServletRequest request, Object command, Errors errors) throws Exception {
		
		System.out.println("iN REFERENCE ");
		Map<String,Object> employeeMap = new HashMap<String,Object>();
		List<Object> employeeList = (List<Object>) new EmployeeDao().getList();
		
		employeeMap.put("employeeMap", employeeList);
		return employeeMap;			
	}
	
	protected Object formBackingObject(HttpServletRequest request)
			throws Exception {
		
			System.out.println("Form backing Object");
			return new Employee();
	
	}
	
		

}

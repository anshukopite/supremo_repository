package com.test.springapp.dao;

import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.HibernateTemplate;

import sun.font.CreatedFontTracker;

import com.test.springapp.connection.HibernateConfiguration;
import com.test.springapp.pojo.Employee;


public class EmployeeDao {
	
	HibernateTemplate template;
	int id;
	public void setSessionFactory(SessionFactory factorty){
		
		template = new HibernateTemplate(factorty);
		
	}
	
	public List<Object> getList() {
		
		SessionFactory factory = HibernateConfiguration.hiberconfig();
		Session ses = factory.openSession();
		Transaction trans = ses.beginTransaction();
		String hql = "FROM Employee";
		Query query = ses.createQuery(hql);
		trans.commit();
		List<Object> result = query.list();
		return result;
		
	}
	
	public List<Object> getListById(int id){
		
		SessionFactory factory = HibernateConfiguration.hiberconfig();
		Session ses = factory.openSession();
		Transaction trans = ses.beginTransaction();

		Criteria query = ses.createCriteria(Employee.class);
		query.add(Restrictions.eq("id", id));
		List<Object> result = query.list();
		trans.commit();
		
		
		
		return result;
		
	}
	
	
	
	
	public Employee setSetters(int id,String firstName,String lastName,String emailId,char gender){
		
		Employee emp = new Employee();
		System.out.println("Employeee");
					
		emp.setId(id);
		emp.setFirstName(firstName);
		emp.setLastName(lastName);
		emp.setEmailId(emailId);
		emp.setGender(gender);
					
		return emp;
		
	}

	
	public int getMaxId(Session ses,SessionFactory fact,Transaction trans){
		
		try{
		String hql = "Select max(id) from Employee";
		Query getMax = ses.createQuery(hql);
		List<?> result  = getMax.list();
		
		Iterator itr  = result.iterator();
		while(itr.hasNext()){
			
			  id= (int) itr.next();
			System.out.println("ID "+id);
		}
		}catch(Exception e){
			
			id=0;
			
		}
		return id;
			
	}
	
	

}

package com.test.springapp.dao;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.test.springapp.connection.HibernateConfiguration;

public class deleteDAO {

	public void deleteEmployee(String[] id){
		boolean res = false;
		SessionFactory factory = HibernateConfiguration.hiberconfig();
		Session ses = factory.openSession();
		Transaction trans = ses.beginTransaction();
		for(int i=0;i<id.length;i++){
			String hql = "DELETE Employee WHERE id= :id";
			Query query = ses.createQuery(hql);
			query.setParameter("id", Integer.parseInt(id[i]));
			int result  = query.executeUpdate();
			System.out.println("Rows Affected: "+result);
		}
		trans.commit();
		ses.close();
	}
	
	public void deleteAllEmployee(String id){
		boolean res = false;
		SessionFactory factory = HibernateConfiguration.hiberconfig();
		Session ses = factory.openSession();
		Transaction trans = ses.beginTransaction();
			String hql = "DELETE Employee WHERE id= :id";
			Query query = ses.createQuery(hql);
			query.setParameter("id", Integer.parseInt(id));
			int result  = query.executeUpdate();
			System.out.println("Rows Affected: "+result);
		trans.commit();
		ses.close();
	}
	
}

package com.test.springapp.dao;

import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.test.springapp.connection.HibernateConfiguration;
import com.test.springapp.pojo.user_tbl;

public class user_tblDAO {

	public boolean findUser(String username, String pwd) {

		boolean success=false;
		List<?> userList;
		SessionFactory factory = HibernateConfiguration.hiberconfig();
		Session ses = factory.openSession();

		Transaction trans = ses.beginTransaction();
		
		//CreateCriteria 
		Criteria query = ses.createCriteria(user_tbl.class); 
		
		/*Query query = ses.createQuery("From user_tbl where username='"
				+ username + "' and pwd ='" + pwd + "'");*/
		userList = query.list();
		Iterator<?> credItr = userList.iterator();

		while (credItr.hasNext()) {
			user_tbl user = (user_tbl) credItr.next();

			System.out.println("UserName:" + user.getUsername());
			System.out.println("Password:" + user.getPwd());

			if (user.getUsername().equals(username)) {

				if (user.getPwd().equals(pwd)) {

					System.out.println("SuccessFul Login");
					success = true;
					
				} else {

					System.out.println("Invalid Password!!!");
					
				}

			} else {

				System.out.println("Invalid Username!!!");
				
			}

		}
		trans.commit();
		ses.close();
		return success;
	}

}

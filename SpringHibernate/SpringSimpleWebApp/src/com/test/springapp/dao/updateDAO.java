package com.test.springapp.dao;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.test.springapp.connection.HibernateConfiguration;

public class updateDAO {

public Boolean updateEmployee(int id,String firstName,String lastName,String emailId,char gender){
	
		boolean res = false;
		SessionFactory factory = HibernateConfiguration.hiberconfig();
		Session ses = factory.openSession();
		Transaction trans = ses.beginTransaction();
		String hql = "UPDATE Employee SET firstName= :firstName, lastName= :lastName, emailId=:emailId,gender= :gender WHERE id= :id";
		Query query = ses.createQuery(hql);
		query.setParameter("firstName", firstName);
		query.setParameter("lastName", lastName);
		query.setParameter("emailId", emailId);
		query.setParameter("gender", gender);
		query.setParameter("id", id);
		int result  = query.executeUpdate();
		System.out.println("Rows Affected: "+result);
		if(result==1){
			
			res = true;
		}else{
			
			res = false;
		}
		trans.commit();
		ses.close();
		return res;
	}
	
	
	
	
	
}

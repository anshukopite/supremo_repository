package com.test.springapp.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.test.springapp.connection.HibernateConfiguration;
import com.test.springapp.pojo.Employee;

public class insertDAO {

	public boolean add(String fName,String lName, String email,char gender){
		
		boolean  redirect = false;
		try{
		SessionFactory factory = HibernateConfiguration.hiberconfig();		
		Session ses = factory.openSession();
		
		Transaction trans = ses.beginTransaction();
		System.out.println("In here");
		EmployeeDao dao = new EmployeeDao();
		int id = dao.getMaxId(ses,factory,trans);
		Employee emp = dao.setSetters((id+1), fName, lName, email,gender);		
		ses.persist(emp);		
		trans.commit();		
		redirect = true;
		ses.close();
		
		}catch(Exception e){
			redirect = false;
			e.printStackTrace();
		}				
		return redirect;
	}
	
	
	
}

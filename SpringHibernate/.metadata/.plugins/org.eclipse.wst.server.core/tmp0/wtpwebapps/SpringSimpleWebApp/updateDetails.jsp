<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Update employee</title>
<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
<script type="text/javascript">
	
$(document)
		.ready(
				function (){	
		
		
	
	
	var empGenderList =  $("#gender").map(function() {
		return $(this).val();
	}).get();
	
	for(var i=0;i<empFirstNameList.length;i++){
		
		//alert(empGenderList[i]);
		if(empGenderList[i]=='M'){
			$("#Male").attr('checked',true);			
		}else{
			$("#Female").attr('checked',true);
			
		}
	}
	
	$("#updateNow").click(function(){
		
		confirm("Are you sure, you want to update");
		
	});

	$("#back").click(function(){
		
		//alert("Back Home");
		window.location.href = "view.do"; 
		
	});
	
});	
	
	
	

</script>


</head>
<body>
	<c:if test="${empMap != NULL}" >
	<form action="update.do" name="updateDetails" method="post">
	
		<table>
			<tr>
					<th>Id</th>
					<th>First Name</th>
					<th>Last Name</th>
					<th>Email-Id</th>
					<th>Gender</th>
					<th>Update</th>
			</tr>	
			
				<tr>
		 <c:forEach items="${empMap}" var="empMap">
				<td>  <label id="id" >${empMap.getId()}</label><input type="hidden" name="id" id="hid" value="${empMap.getId()}">  </td>
				<td><input type="text" name="firstName" id="firstName" value="${empMap.getFirstName()}" ></input> </td>
				<td><input type="text" name="lastName" id="lastName" value="${empMap.getLastName()}"></input> </td>
				<td><input type="text" name="emailId" id="emailId" value="${empMap.getEmailId()}"></input> </td>
				<td><input type="radio" name="gender" id="Male" value="M"  />Male
					<input type="radio" name="gender" id="Female" value="F" />Female </td>
					<td><input type="hidden" id="emailid" value="${empMap.getEmailId()}" /></td>
				<td><input type="hidden" id="gender" value="${empMap.getGender()}" /></td>
			</c:forEach> 		
				<td><input type="submit" name="updateNow" value="Submit" id="updateNow" > </td>
			</tr>	
		</table>
	</form>
	
			<input type="button" name="BackHome" value="Home" id="back" />
	
</c:if>


</body>
</html>
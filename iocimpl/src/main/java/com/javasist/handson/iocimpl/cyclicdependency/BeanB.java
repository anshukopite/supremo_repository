package com.javasist.handson.iocimpl.cyclicdependency;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BeanB {

	private BeanA beanA;

	@Autowired
	public BeanB(BeanA beanA) {
		System.out.println("Initilaizing BeanB with Dependency of BeanA");
		this.beanA = beanA;
	}
}

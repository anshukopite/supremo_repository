package com.javasist.handson.iocimpl.cyclicdependency;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component
public class BeanA {

	private BeanB beanB;
	
	@Autowired
	public BeanA(@Lazy BeanB beanB) {
		System.out.println("Initilaizing BeanA with Dependency of BeanB");
		this.beanB = beanB;
	}
}

package com.javasist.handson.iocimpl.cyclicdependency;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.javasist.handson.iocimpl.cyclicdependency")
public class SpringConfig {

	
	
}

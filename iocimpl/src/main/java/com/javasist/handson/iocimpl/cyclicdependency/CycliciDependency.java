package com.javasist.handson.iocimpl.cyclicdependency;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class CycliciDependency {

	public static void main(String[] args) {

		ApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);
		
		System.out.println("Context Loading...");
		BeanA a = (BeanA)context.getBean("beanA");
		
		BeanB b = (BeanB) context.getBean("beanB");
		
		
		System.out.println("BeanA -> "+a.hashCode());
		System.out.println("BeanB	 -> "+b.hashCode());
	}
}

package com.javasist.handson.iocimpl;

import java.util.logging.Logger;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.javasist.handson.iocimpl.bean.Employee;

/**
 * Hello world!
 *
 */
public class App 
{
	static Logger log = Logger.getLogger(App.class.getName());
    public static void main( String[] args ){

    	
    	Resource res = new ClassPathResource("spring-context.xml");
    	BeanFactory factory = new XmlBeanFactory(res);
    	System.out.println("Factory Bean Loaded!!");
		/*
		 * Employee emp = (Employee)factory.getBean("employee");
		 * 
		 * System.out.println(emp);
		 */
    	ApplicationContext context = new ClassPathXmlApplicationContext("spring-context.xml");
    	System.out.println("Application Context Bean Loaded!!");
		
    	factory.getBean("employee");
		  
		 
    	
    }
}
